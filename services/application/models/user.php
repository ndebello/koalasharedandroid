<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model {
    function __construct() {
        parent::__construct();
		  $this->db = $this->load->database('default', TRUE); 
    }
    
	
	function getUsersAll() {
        $this->db = $this->load->database('default', TRUE);  
        $this->db->select();
        $query = $this->db->get('users');
        return $query->result();
    }
	function get_owner1($email)
	{
	
       $this->db->select();
       $this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('owner_table');
      // return $query->result();
		return $query->num_rows();
	}
	
	function caregiver_details($email)
	{
		/*$this->db->select();
		$this->db->where('email', $email); 
		$query = $this->db->get('caregiver');*/
		
		
		$query = $this->db->query("SELECT * FROM caregiver where email='$email' ");	
		
		return $query->result();
		//return $query->num_rows();
	}
	
	public function message_count()
	{
		return $this->db->count_all('owner_table');
	}
	function getowners($per_pg,$offset) {
        $this->db = $this->load->database('default', TRUE); 
		$this->db->order_by('own_id','desc'); 
        $this->db->select();
        $query = $this->db->get('owner_table',$per_pg,$offset);
		return $query->result();
    }
	
	
	function fetch_report1($pid)
	{
		$date = date("Y-m-d");
		$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' and note_date<='$date'  group by note_date order by note_id desc");	
     	
		
		
		$res = $query->num_rows();
			
			if($res<=0){
				return $res;
			} else {
			return $query->result();
			
			}
		
		
	}
	
	function fetch_report2($pid,$from_date,$to_date)
	{
		$date = date("Y-m-d");
		$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' and note_date<='$to_date'  and note_date>='$from_date' group by note_date order by note_id desc");	
     	
		
		
		$res = $query->num_rows();
			
			if($res<=0){
				return $res;
			} else {
			return $query->result();
			
			}
		
		
	}
	
	
	function fetch_report_details($pid,$date)
	{
	//SELECT * FROM notifications as noti,caregiver as care where noti.pat_id='$pid' and noti.note_date='$date' and care.id=noti.care_id ORDER BY noti.note_date ASC
	$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' and note_date='$date' ORDER BY `note_date` ASC");
		
     	$res = $query->num_rows();
			
			if($res<=0){
				return $res;
			} else {
			return $query->result();
			
			}
			
	}
	
	
	function fetch_report_details123($pid,$date,$item)
	{
	//SELECT * FROM notifications as noti,caregiver as care where noti.pat_id='$pid' and noti.note_date='$date' and care.id=noti.care_id ORDER BY noti.note_date ASC
	$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' and note_date='$date' and event_type_id='$item' ORDER BY `note_date` ASC");
		
     	$res = $query->num_rows();
			
			if($res<=0){
				return $res;
			} else {
			return $query->result();
			
			}
			
	}
	
	function fetch_newreport1($pid,$from,$to)
	{
		
$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' and note_date BETWEEN '$from' and '$to' group by note_date  ORDER BY `note_date` ASC");
return $query->result();
	
	}
	function fetch_newreport2($pid,$from,$to)
	{
		
$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' and note_date BETWEEN '$from' and '$to'   ORDER BY `note_date` ASC");
return $query->result();
	
	}
	
	function fetch_medecine($pid,$eid) 
      {
		//$query = $this->db->get('medicine');
		//Edited line ==>where pat_id='$pid' or pat_id=0
		$date = date("Y-m-d");
	 //$query = $this->db->query("SELECT * FROM notifications as noti, medicine as med where noti.event_type_id='$eid' and noti.pat_id='$pid' and med.med_id=noti.sub_event_id  and noti.note_date='$date'");
	 
	 $query = $this->db->query("SELECT * FROM medicine   where  ( pat_id='$pid' or pat_id='0') and is_delete!='1' ");
	 
	 
     	
     
		$res = $query->num_rows();
			
			if($res<=0){
				return $res;
			} else {
			return $query->result();
			
			}


	 }
	  
	  
	  
	
	function fetch_medecine1($pid,$eid) 
      {
		//$query = $this->db->get('medicine');
		//Edited line ==>where pat_id='$pid' or pat_id=0not.event_type_id='$eid' and not.pat_id='$pid' and
		
		$query = $this->db->query("SELECT * FROM notifications as nat, medicine as med  where  nat.sub_event_id=med.med_id and nat.pat_id='$pid' and nat.event_type_id='$eid'  group by nat.sub_event_id order by med.med_id desc ");	
     	return $query->result();
      }
	
	function fetch_diets1($pid,$eid) 
      {
		$query = $this->db->query("SELECT * FROM notifications as noti, diets as dts where noti.event_type_id='$eventid' and noti.pat_id='$pid' and dts.id=noti.sub_event_id");
			return $query->result();
      }
	  
	  function fetch_diets($pid,$eventid,$show_diets )
		{
			if($show_diets == "Intolerance" ){
			$query = $this->db->query("SELECT * FROM  foodlike as flike, diets as dit where flike.patid='$pid' and flike.foodlike='Intolerance' and dit.id=flike.foodid and dit.is_delete!='1'");
			} else if($show_diets == "Favourites" ){
			$query = $this->db->query("SELECT * FROM  foodlike as flike, diets as dit where flike.patid='$pid' and flike.foodlike='Favourites' and dit.id=flike.foodid and dit.is_delete!='1'");
			} else {
			//$query = $this->db->query("SELECT * FROM notifications as noti, diets as dts where noti.event_type_id='$eventid' and noti.pat_id='$pid' and dts.id=noti.sub_event_id");
			
			$query = $this->db->query("select * from diets where (pat_id='$pid' or pat_id='0') and is_delete!='1' ORDER BY id desc");
			}
			$res = $query->num_rows();
			//echo json_encode(array("result" =>"","message" =>$res), 204); exit;
			if($res<=0){
				return $res;
			} else {
				return $query->result();
			
			}
		}
	
	
	function fetch_activity1($pid,$eid) 
      {
				
		$query = $this->db->query("SELECT * FROM notifications as nat, activities as act  where  nat.sub_event_id=act.act_id and nat.pat_id='$pid' and nat.event_type_id='$eid'  group by nat.sub_event_id order by act.act_id desc ");	
     	return $query->result();
      }
	function fetch_parameter1($pid,$eid) 
      {
			
	
		//$query = $this->db->query("SELECT * FROM notifications as nat, parameter_name as pra  where  nat.sub_event_id=pra.id and nat.pat_id='$pid' and nat.event_type_id='$eid'   group by nat.sub_event_id  order by nat.note_id desc ");	
     	$query = $this->db->query("SELECT * from parameter_name   where   (pat_id='0' OR  pat_id='$pid') and is_delete!='1'");	    
			
			
			

		$res = $query->num_rows();
			
			if($res<=0){
				//return $res;
				echo json_encode(array("result" =>"","message" =>"No Result Found"), 204); exit;
			} else {
			$parameters =  $query->result();
			echo json_encode(array("result" =>"success","parameters_routines" => $parameters), 200);
			}


	}
	  
	 function num_notification($eid)
	{
	   $query = $this->db->query("select * from notifications  where note_id='$eid'");
       return $query->result();
		
	} 
	  
	  function parameteslist()
{
	$query = $this->db->query("SELECT * FROM parameter_name ");			
	return $query->result();
		
}
	
	function fetch_activity($pid,$eid) 
	{
     
		//$query = $this->db->query("SELECT * FROM notifications as noti,activities as act  where noti.event_type_id='$eid' and noti.pat_id='$pid' and act.act_id=noti.sub_event_id");
		
		$query = $this->db->query("SELECT * FROM activities where (pat_id='$pid'or pat_id='0') and is_delete!='1' order by act_id desc");
		
		$res = $query->num_rows();
		if($res<=0){
				return $res;
			} else {
			return $query->result();
			
			}
		
	}
	
	public function count_profetionals()
	{
		return $this->db->count_all('professionals');
	}
	
	function getprofetionals($per_pg,$offset) 
	{
	        $this->db->order_by('id','desc'); 
			$this->db->select();
	        $query = $this->db->get('professionals',$per_pg,$offset);
		    return $query->result();
	}
	
	function serach_profetionals()
	{      
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM professionals $searchkey");			
				
		return $query->result();

	    }

	function profetionalDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('professionals');		
	  return $query->result();
	
	}
	

/*


========================================>view the caregiver  =========================================================> */
function get_caregiver($id)
{
$query = $this->db->query("SELECT * FROM caregiver where id='$id'");			

return $query->result();		
}

function updatetransation($id,$trans)
{


$this->payamount= 50;
$this->tran_status= "Success";
$this->trno= $trans;
$this->pay_date=date("Y-m-d");
$this->db->update('owner_table', $this, array('own_id' => $id));





	
}

function addactivity()
{
				$file=time().$_FILES['attached']['name'];
				$dis="activity/";
				$dis=$dis.$file;
				move_uploaded_file($_FILES['attached']['tmp_name'],$dis);
	            $this->act_name   = $_POST['act_name']; 
				$this->act_details = $_POST['actdetails'];
				$this->act_attached = $file;
				$this->act_date = date("Y-m-d");
				$this->e_care_id=1;
				//$this->act_dayof_week=$_POST['dateofweek'];
				$this->db->insert('activities', $this);
}
	
	
	
	
function updateownerlicencestatus($oid)	
{
$data=array(
'own_licence_type'=>"Extended",
     );
$this->db->update('owner_table', $data, array('own_id' => $oid));
	
		
	}
function consultation_request($oid)
{
 $query = $this->db->query("select * from cons_request_messages where recipient='$oid' and subject!=''");
  return $query->result();
}

	function get_owner_licence($oid)
	{
      
	   $query = $this->db->query("select * from owner_licence where own_id='$oid'");
      //return $query->result();
	   return $query->num_rows();
	}	
	
function insert_anether_olicence($oid,$email,$num)	
	{
	$lic="koala".$oid."_".$num;
	$data=array(
	'own_id'=>$oid,
	'licence_no'=>$lic,
	'oemail'=>$email,
	
	);
	$this->db->insert('owner_licence', $data);
	return $lic;
	
	}
	
function get_caregiver_tologin($username,$password)
{
	
	  $this->db = $this->load->database('default', TRUE); 
		$this->db->select();
       $this->db->where('email', $username); 
	   $this->db->where('passwd', $password); 
       $query = $this->db->get('caregiver');
       //return $query->result();
	   return $query->num_rows();
}
	



function getcaregiversbyid($id)
	{
      
	   $query = $this->db->query("select * from caregiver where id='$id'");
      //return $query->result();
	   return $query->num_rows();
	}




	
function getcaregiversbyemail($email)
	{
      
	   $query = $this->db->query("select * from caregiver where email='$email'");
      //return $query->result();
	   return $query->num_rows();
	}

	
	function updatecaregiversbycid()
	{
	  // echo json_encode(array("result" =>$_POST), 200); exit;
	   
	   //$query = $this->db->query("update caregiver set passwd='$password' where email='$email'");
      //return $query->result();
	   //return $query->num_rows();
	   //$date=date("Y-m-d");
	   
	  
$data=array('lat'=>$_POST['lat'],
			'lon'=>$_POST['lng'],
			'name'=>$_POST['name'],
			'surname'=>$_POST['surname'],
			'age'=>$_POST['age'],
			'gender'=>$_POST['gender'],
			'zipcode'=>$_POST['zipcode'],
			'city'=>$_POST['city'],
			'state'=>$_POST['state'],
			'address'=>$_POST['address'],
			'phone'=>$_POST['phone'],
			'photo'=>$_POST['image_name']
			//'from'=>$date,
            );
return $this->db->update('caregiver', $data, array('id' => $_POST['user_id']));
	}

	
	
	
		function updateowncaregiversbycid()
	{
	  
	   
	  
$data=array('lat'=>$_POST['lat'],
			'lon'=>$_POST['lng'],
			'name'=>$_POST['name'],
			'surname'=>$_POST['surname'],
			'age'=>$_POST['age'],
			'gender'=>$_POST['gender'],
			'zipcode'=>$_POST['zipcode'],
			'city'=>$_POST['city'],
			'state'=>$_POST['state'],
			'address'=>$_POST['address'],
			'phone'=>$_POST['phone']
			//'photo'=>$photo,
			//'from'=>$date,
            );
return $this->db->update('caregiver', $data, array('id' => $_POST['caregiver_id']));
	}

	
	
	
	function get_profdetails($username,$password)
	{
		  $this->db = $this->load->database('default', TRUE); 
       $this->db->select();
       $this->db->where('email', $username); 
	   $this->db->where('password', $password); 
       $query = $this->db->get('professionals');
       //return $query->result();
	   return $query->num_rows();
	}
	
	function getallcaregivers($oid)
	{
       $this->db->select();
       $this->db->where('own_id', $oid); 
	   $query = $this->db->get('caregiver');
       //return $query->result();
	   return $query->num_rows();
	}
	function getmorecaregivers($oid)
	{
	   $query = $this->db->query("select * from caregiver where care_type_id!=1 and own_id='$oid'");
      //return $query->result();
	   return $query->num_rows();
	}
	
	function getcaregiversemail($email)
	{
      
	   $query = $this->db->query("select * from caregiver where email='$email'");
      //return $query->result();
	   return $query->num_rows();
	}
	
function insertintocaregiver($ar,$snname,$cemail,$ctype,$patient,$id)
	{
return $query = $this->db->query("insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id) values('$ar','$snname','$cemail','$ctype','$patient','0','$id')");
//return $query->result();
//return $query->num_rows();
	}
	
	function get_ownerdet($username)
    {
	    $query = $this->db->get_where('owner_table', array('owner_email' => $username),1);
	    //$this->db->where("owner_email",$username);
		//$this->db->where("own_passwd",$password);
		$query=$this->db->get("owner_table"); 
		  return $query->result();
		if( $query->num_rows() > 0 )
		{
			
		//return $query->row_array();
	    return $query->result();
		//return false;
		}
    }
	
   function getProfessionalsAll() {
        $this->db = $this->load->database('default', TRUE);  
        $this->db->select('us.*, pfl.*');
		$this->db->join('professionals pfl', 'pfl.professional_id  = us.id');
		$this->db->where('role', 'professional' );
        $query = $this->db->get('users us');
		//print_r($query->result());
		return $query->result();
  }
   function getCaregiverslist() {
        $this->db = $this->load->database('default', TRUE);  
       // $this->db->select();
		//$this->db->where('role', 'caregiver' );
        //$query = $this->db->get('users');
        //return $query->result();
		$query = $this->db->query("SELECT * FROM caregiver");			
		return $query->result();
		
		
		
    }
	
	
	
	
   function getCaregiversAll() {
        $this->db = $this->load->database('default', TRUE);  
         $this->db->select('us.*, pat.*');
		 $this->db->join('patients pat', 'pat.caregiver_id = us.id');
		$this->db->where('role', 'caregiver' );
        $query = $this->db->get('users us');
		//print_r($query->result() );
        return $query->result();
    }
	
	// get user by username
	function get_users_by_username( $username ) {
		$query = $this->db->get_where('users', array('username' => $username), 1);
		if( $query->num_rows() > 0 ) 
		return $query->row_array();
		return false;
	}
	
	// get user by id
	function get_users_by_id( $id=null ) {
		$query = $this->db->get_where('users', array('id' => $id), 1);
		if( $query->num_rows() > 0 ) 
		//return $query->row_array();
	    return $query->result();
		return false;
	}
	function get_users_name( $name ) { 
	$this->db = $this->load->database('default', TRUE);  
		$query = $this->db->get_where('owner_table', array('owner_email' => $name), 1);
		if( $query->num_rows() > 0 ) 
		return $row=$query->row_array();
	    
		
	}
	
	
	
	function get_ajax_owner($email,$type) 
	{
		
		//$this->db->select();
      // $this->db->where('l_type', $type); 
      // $this->db->where('owner_email', $email); 
       //$query = $this->db->get('owner_table');
	   
	   $query = $this->db->query("select * from owner_table where owner_email='$email' and l_type='$type'");
       return $query->num_rows();
	
	}
	
	
	function get_own_care($cemail,$id) 
	{
		
		$this->db->select();
       $this->db->where('email', $cemail); 
       $this->db->where('own_id', $id); 
       $query = $this->db->get('caregiver');
       return $query->num_rows();
	
	}
	
	
	
function addmulpat($oid,$pname,$sname,$ownid) 
{
	
	$data=array(
	'own_licence'=>$oid,
    'name'=>$pname,
	'surname'=>$sname,
	'own_id'=>$ownid,
	);
		
	return $this->db->insert('patients', $data);
}
	
function insert_own_care($name,$sname,$cemail,$ctype,$patient,$id)	
	{
	$data=array(
	'name'=>$name,
    'surname'=>$sname,
	'email'=>$cemail,
	'care_type_id'=>$ctype,
	'pat_id'=>$patient,
	'own_id'=>$id,
	);
	return $this->db->insert('caregiver', $data);
	}
	
	
	
	
	
	
	
function get_con_rsa_lice($email,$lice)
	{
	
       $this->db->select();
       $this->db->where('licence_no', $lice); 
       $this->db->where('oemail', $email); 
       $query = $this->db->get('owner_licence');
       //return $query->result();
		return $query->num_rows();
		
	}
	
 function getpatient_details($oid,$pn,$spn,$lice)
 {
	   $this->db->select();
       $this->db->where('own_id', $oid); 
      $this->db->where('own_licence', $lice);
       $query = $this->db->get('patients');
       return $query->num_rows();
		
 }
 function getowner($email)
	{
       $this->db->select();
       $this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('owner_table');
       //return $query->result();
	   //return $query->num_rows();
	   return $row=$query->row_array();
	}
	function getowner1($email)
	{
       //$this->db->select();
       //$this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       //$query = $this->db->get('owner_table');
       //return $query->result();
	   //return $query->num_rows();
	   $query = $this->db->query("select * from owner_licence where oemail='$email' order by id desc");
	   return $row=$query->row_array();
	}

	
	
	function get_ajax_owner_licence($email,$lice) 
	{
	   $this->db->select();
       $this->db->where('licence_no', $lice); 
       $this->db->where('oemail', $email); 
       $query = $this->db->get('owner_licence');
       return $query->num_rows();
	
	}
	
	
	
	
	
	
	function get_users_lice( $name ) {
		//$query = $this->db->get_where('owner_licence', array('own_id' => $name));
		  $query = $this->db->query("select licence_no from owner_licence where own_id='$name' order by id desc");
		return $row=$query->result();
	    
		
	}
	
	
	
	

	function ownlogin($username,$password)
	{
		$this->db->where("owner_email",$username);
		$this->db->where("own_passwd",$password);
		$query=$this->db->get("owner_table"); 
		return $query->num_rows();
	}



function login1($username,$password)
	{
		$this->db->where("kname",$username);
		$this->db->where("kpasswd",$password);
		 $query=$this->db->get("koala_admin"); 
		return $query->num_rows();
		
	}





	function login($username,$password)
	{
		$this->db->where("username",$username);
		$this->db->where("password",$password);
		$query=$this->db->get("users"); 
		
		
		
		if($query->num_rows()>0)
		{
			$data=$query->result();
			$user_id=$data[0]->id;
			if($data[0]->role=="caregiver")
			{
				$this->db->where("caregiver_id",$user_id);
				$result=$this->db->get("patients"); 
				$details= $result->result();
                $photo= $details[0]->photo;
				//print_r($result);
			}
			else if($data[0]->role=="professional")
			{
			    $this->db->where("professional_id ",$user_id);
				$result=$this->db->get("professionals"); 
				$details= $result->result();
                $photo= $details[0]->photo;
			}
			else
			{
			$photo= '';
			}
			foreach($query->result() as $rows)
			{
				//add all data to session
				$newdata = array(
				  'user_id'  => $rows->id,
				  'user_name'  => $rows->username,
				  'user_email'    => $rows->email,
				  'user_role'    => $rows->role,
				  'logged_in'  => TRUE,
				  'user_photo'  => $photo,
				);
			}
			 $this->session->set_userdata($newdata);
			
			 return true;
		}
			return false;
		}
	
	public function add_user()
	{   
		$data=array(
		'username'=>$this->input->post('username'),
		'email'=>$this->input->post('email'),
		'password'=>md5($this->input->post('password'))
		);
		$this->db->insert('users',$data);
	}
		
	public function updateowntype($id,$type)
	{
	$this->care_type= $type; 
	$this->db->update('owner_table', $this, array('own_id' => $id));	
		}
	//
	
	
	
	
	public function getpatient($owid)
	{
	  $this->db = $this->load->database('default', TRUE);  
	  $query = $this->db->query("select * from patients where own_id='$owid'");
    return $query->num_rows();
	}
	
	public function getpatient1($owid,$licence)
	{
		//echo "select * from patients where own_id='$owid' and own_licence='$licence' "; //exit;
		
	 //$this->db = $this->load->database('default', TRUE);  
	 $query = $this->db->query("select * from patients where own_id=$owid and own_licence='$licence' ");
	 
    return $query->num_rows(); 
	}
	
	
	
	public function get_owner($id)
	{
	  $this->db = $this->load->database('default', TRUE);  
      /*$this->db->where('own_id', $id);  
	  $query = $this->db->get('owner_table');	
	  return $query->result();
	  */
	  $query = $this->db->query("select * from owner_table where own_id='$id'");
/*   foreach ($query->result_array() as $row)
   {/*
    $row['own_licence'];
    $row['own_name'];
  
  $data=array(
	'own_id'=>$id,			 
	'email'=>$this->session->userdata('owner_email'),
	'passwd'=>$this->session->userdata('own_passwd'),
	'care_type_id'=>1,
	'name'=>$this->session->userdata('own_name'),
	'phone'=>$this->session->userdata('own_mobile'),
	); 
    }*/
	
	
	}
//============//////////// function about the patient insert and add owner,update owner,addpayment /////////////////=============================   
	function owner_insertpatient($id,$licence)
	{
	 $data=array(
	'own_id'=>$id,
	'own_licence'=>$licence,
	'name'=>$_POST['name'],
	'surname'=>$_POST['surname'],
	'created'=>date("Y-m-d"),
	 );
	  $this->db->insert('patients',$data);
	  return $id=$this->db->insert_id();
    }
	
	
	public function add_session_user123($lat,$lon,$username,$ltype,$password,$lice_type,$uname,$role)
	{   
	
	
	
	$data=array(
	'own_name'=>$uname,
    'owner_email'=>$username,
	'own_passwd'=>md5($password),
    'own_ipaddress'=>$_SERVER['REMOTE_ADDR'],
	'own_date'=>date("Y-m-d"),
	'lat'=>$lat,
	'lon'=>$lon,
	'owner_type'=>$lice_type,
	'l_type'=>$ltype,
	);
	
	$this->db->insert('owner_table',$data);
	return $id=$this->db->insert_id();
	
	}
	
	public function add_session_con_owner($lat,$lon,$username,$ltype,$password,$ltype,$uname)
	{ 
	
	$data=array(
	'own_name'=>$uname,
    'owner_email'=>$username,
	'own_passwd'=>md5($password),
    'own_ipaddress'=>$_SERVER['REMOTE_ADDR'],
	'own_date'=>date("Y-m-d"),
	'owner_type'=>"business",
	'l_type'=>"Consortium",
	'lat'=>$lat,
	'lon'=>$lon,
	);
	
	$this->db->insert('owner_table',$data);
	return $id=$this->db->insert_id();
	
	}
	
	
	
	
	
	
	function in_owner_lice($id,$username,$ltype)
	{
	/*echo "<script>alert($id)</script>";*/
	$lice="koala".$id;
	$mdata=array(
	'own_id'=>$id,
    'licence_no'=>$lice,
	'oemail'=>$username,
	'otype'=>$ltype,
	);
	$this->db->insert('owner_licence',$mdata);	
	}
	
	function in_owner_lice1($id,$licence,$username,$ltype)
	{
		
	/*echo "<script>alert($id)</script>";*/
	$lice=$licence;
	$mdata=array(
	'own_id'=>$id,
    'licence_no'=>$lice,
	'oemail'=>$username,
	'otype'=>$ltype,
	);
	$this->db->insert('owner_licence',$mdata);	
	}
	
	
	public function insert_owner_rsa_con()
	{
	$data=array(
	'own_name'=>$this->session->userdata('uname'),
    'owner_email'=>$this->session->userdata('username'),
	'own_mobile'=>$this->session->userdata('omobile'),
	'own_address'=>$this->session->userdata('oaddress'),
	'own_passwd'=>md5($this->session->userdata('passwordvalue')),
    'own_ipaddress'=>$_SERVER['REMOTE_ADDR'],
	'own_date'=>date("Y-m-d"),
	'owner_type'=>$this->session->userdata('licence'),
	'l_type'=>$this->session->userdata('ltype'),
	);
	
	$this->db->insert('owner_table',$data);
	return $id=$this->db->insert_id();

	}
	
	public function update_owner_rsa_con()
	{
	$data=array(
	'own_address'=>$this->session->userdata('oaddress'),
	'own_passwd'=>md5($this->session->userdata('passwordvalue')),
	);
	$email=$this->session->userdata('username');
	//$this->db->insert('owner_table',$data);
	$this->db->update('owner_table', $data, array('owner_email' => $email));
	//return $id=$this->db->insert_id();
	}
	public function update_owner($id,$name,$mobile,$address,$status,$trns,$pay)
	{
     //'own_date'=>$date,
	$data=array(
	'own_name'=>$name,
	'own_mobile'=>$mobile,
	'own_address'=>$address,
	'own_status'=>$status,
	'payamount'=>$pay,
	'trno'=>$trns,
	);
	//$this->db->insert('owner_table',$data);
	$this->db->update('owner_table', $data, array('own_id' => $id));
	//return $id=$this->db->insert_id();
	}

   public function updateowner($id)
 {	
	    $this->own_licence ="koala".$id;
        
        $this->db->update('owner_table', $this, array('own_id' => $id));
	}
	
	
	public function  add_payment($id)
	{
	
	$data=array(
	'payment_type'=>$this->session->userdata('ctype'),
    'transaction_id'=>$this->session->userdata('cnumber'),
	'payment_amount'=>3000,
	'payment_date'=>date("Y-m-d"),
	'payment_ip'=>$_SERVER['REMOTE_ADDR'],
	'user_id'=>$id,
	);
	return $this->db->insert('payments',$data);	
			
	}
	
	
	
	
	public function updateownersta($id)
    {	
	$data=array(
	'own_status'=>"inactive",
    );
	 //$this->own_status ="inactive";
     $this->db->update('owner_table', $data, array('own_id' => $id));
	}
	
	function ownerdetail($id) {
		$this->db->where('own_id', $id);  
		$query = $this->db->get('owner_table');		
		return $query->result();

	}
	
	function patient_details($id) 
	{
		
		 $query = $this->db->query("select * from patients where id ='$id' order by id desc");
		return $query->result();
		
		
	}
	
	function care_details($id) 
	{
		$this->db->where('id', $id);  
		$query = $this->db->get('caregiver');		
		return $query->result();

	}
	
	
	
	
	
	
    public function add_session_user()
	{   
		$data=array(
			'username'=>$this->session->userdata('username'),
			'email'=> $this->session->userdata('email'),
			'password'=>md5($this->session->userdata('passwordvalue')),
			'role'=>$this->session->userdata('role'),
		);
		$this->db->insert('users',$data);
		$last_id=$this->db->insert_id();
		
		if($this->session->userdata('role')=='caregiver'){
		
			$data=array(
			'city'=>$this->session->userdata('city'),
			'state'=>$this->session->userdata('state'),
			'address'=>$this->session->userdata('address'),
			'name'=>$this->session->userdata('name'),
			'surname'=>$this->session->userdata('surname'),
			'age'=>$this->session->userdata('age'),
			'gender'=>$this->session->userdata('gender'),
			'zipcode'=>$this->session->userdata('zipcode'),
			'photo'=>$this->session->userdata('photo'),
			'phone'=>$this->session->userdata('phone'),
			'phone2'=>$this->session->userdata('phone2'),
			'phone3'=>$this->session->userdata('phone3'),
			'contact1'=>$this->session->userdata('contact1'),
			'contact2'=>$this->session->userdata('contact2'),
			'contact3'=>$this->session->userdata('contact3'),
			'pathology'=>$this->session->userdata('pathology'),
			'allergies'=>$this->session->userdata('allergies'),
			'intolerances'=>$this->session->userdata('intolerances'),
			'note'=>$this->session->userdata('note'),
			'id_card'=>$this->session->userdata('qr_code'),
			'caregiver_id'=>$last_id,
			);
			$this->db->insert('patients',$data);
			}	
		elseif($this->session->userdata('role')=='professional'){
		   $data=array(
			'name'=>$this->session->userdata('name'),
			'surname'=>$this->session->userdata('surname'),
			'age'=>$this->session->userdata('age'),
			'gender'=>$this->session->userdata('gender'),
			'zipcode'=>$this->session->userdata('zipcode'),
			'photo'=>$this->session->userdata('photo'),
			'phone'=>$this->session->userdata('phone'),
			'address'=>$this->session->userdata('address'),
			'city'=>$this->session->userdata('city'),
			'state'=>$this->session->userdata('state'),
			'qualification'=>$this->session->userdata('qualification'),
			'doctor_board_admitted_to'=>$this->session->userdata('admitted_to'),
			'from'=>$this->session->userdata('from'),
			'cv'=>$this->session->userdata('cv'),
			//'note'=>$this->session->userdata('note'),
			'professional_id'=>$last_id,
			);
		    $this->db->insert('professionals',$data);
		
		}
		
	}
    
	function add_profetional($lat,$lng,$role,$ltype,$username,$passwordvalue,$uname,$surname,$age,$gender,$address,$city,$zipcode,$state,$qualification,$admitted_to,$from,$photo,$trans_no,$pay_amount,$pay_status,$pay_date)
	{       $data=array(
			'name'=>$uname,
			'surname'=>$surname,
			'age'=>$age,
			'gender'=>$gender,
			'zipcode'=>$zipcode,
			'photo'=>$photo,
		//	'phone'=>$this->session->userdata('phone'),
			'address'=>$address,
			'city'=>$city,
			'state'=>$state,
			'qualification'=>$qualification,
			'doctor_board_admitted_to'=>$admitted_to,
			'from'=>$from,
			//'cv'=>$this->session->userdata('cv'),
			'email'=>$username,
			'password'=>md5($passwordvalue),
			'lat'=>$lat,
			'lon'=>$lng,
			'trans_no' => $trans_no,
			'pay_amount' => $pay_amount,
			'pay_status' => $pay_status,
			'pay_date' => $pay_date
			
			//'note'=>$this->session->userdata('note'),
			//'professional_id'=>$last_id,
			);
		   $this->db->insert('professionals',$data);
		   return $id=$this->db->insert_id();
			}
			
			
	function update_prof_transation($pid,$item_transaction)
	{
		$this->trans_no = $item_transaction;
		$this->pay_status="Success";
        $this->pay_date = date("Y-m-d");
        $this->db->update('professionals', $this, array('id' => $pid));
	}
	
			
			
	
	function updateUser($id)
    {
         // please read the below note
        $this->email = $_POST['email'];
        $this->role = $_POST['role'];
        $this->db->update('users', $this, array('id' => $_POST['id']));
    }
	
	function professionalDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('professionals');		
	  return $query->result();
	
	}
	//--------------admin details-----//
	function adminDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('users');	
	return $query->result();
	
	}
	//--------------end--------------//
	
	function caregiverDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('patients');		
	  return $query->result();
	
	}
	
	
	function updateprofessional($id)
    {
         // please read the below note
        $this->name = $_POST['name'];
        $this->surname = $_POST['surname'];
        $this->age = $_POST['age'];
        $this->gender = $_POST['gender'];
        $this->city = $_POST['city'];
        $this->address = $_POST['address'];
        $this->qualification = $_POST['qualification'];
        $this->photo = $_POST['photo'];
        $this->from = $_POST['from'];
        $this->doctor_board_admitted_to = $_POST['doctor_board_admitted_to'];
        $this->db->update('professionals', $this, array('id' => $_POST['id']));
    }
	function updatepatient($id)
	{
	
	 $data=array( 
	'name'=>$_POST['name'],
	'photo'=>$_POST['image_name'],
    'surname'=>$_POST['surname'],
	'age'=>$_POST['age'],
	'gender'=>$_POST['gender'],
	'zipcode'=>$_POST['zip'],
	'city'=>$_POST['city'],
	'state'=>$_POST['state'],
	'address'=>$_POST['address'],
	'phone'=>$_POST['phone'],
	'phone2'=>$_POST['phone2'],
	'phone3'=>$_POST['phone3'],
	'contact1'=>$_POST['contact1'],
	'contact2'=>$_POST['contact2'],
	'contact3'=>$_POST['contact3'],
	'pathology'=>$_POST['pathology'],
	'allergies'=>$_POST['allergies'],
	'intolerances'=>$_POST['intolerances'],
	'note'=>$_POST['note'],
	'lat'=>$_POST['lat'],
	'lon'=>$_POST['lng']
	 );
	
	$this->db->update('patients', $data, array('id' => $id));
	
	
	
	
		
	}
	
	//-----------Admin update---------//
	function updateAdmin($id)
    {
		
		$this->username = $_POST['username'];
        $this->password = $_POST['password'];
		$this->db->update('users', $this, array('id' => $_POST['id']));
    }
	//----------end----------//
		function updatecaregiver($id)
    {
		$this->db = $this->load->database('default', TRUE);
        $this->db->select();
        $this->name = $_POST['name'];
        $this->surname = $_POST['surname'];
        $this->age = $_POST['age'];
        $this->gender = $_POST['gender'];
        $this->photo = $_POST['photo'];
        $this->city = $_POST['city'];
        $this->state = $_POST['state'];
        $this->address = $_POST['address'];
        $this->phone = $_POST['phone'];
        $this->pathology = $_POST['pathology'];
        $this->allergies = $_POST['allergies'];
        $this->intolerances = $_POST['intolerances'];
        $this->db->update('patients', $this, array('id' => $_POST['id']));
    }
	function deleteprofessional($id) {
		$this->db->delete('professionals', array('id' => $id));

	}
	function deletecaregivers($id) {
		$this->db->delete('patients', array('id' => $id));

	}
    // delete user by id
	function deleteUsers($id) {
	    $this->db->delete('users', array('id' => $id));;
	}
	//------------------------Periods---------//
	
	function periodDetail($id) {
			$this->db->where('id', $id);  
			$query = $this->db->get('periods');		
			return $query->result();

			}
	function updatePeriods($id) {

			//$this->patient_id   = $_POST['patient_id']; 
			$this->sleep_time = $_POST['sleep_time'];
			$this->wake_time = $_POST['wake_time'];
			$this->created = $_POST['created'];
			$this->db->update('periods', $this, array('id' => $_POST['id']));
			}
	function deleteperiods($id) {
			$this->db->delete('periods', array('id' => $id));

			}

	//---------end-----------------//

	//------------medicince module-------------//
	
	
	public function medicine_count()
	{
		return $this->db->count_all('medicine');
	}
	
	
	function getMedicines($per_pg,$offset) {
		$this->db->order_by('med_id','desc'); 
        $this->db->select();
	    $query = $this->db->get('medicine',$per_pg,$offset);
		return $query->result();
	}
	
	function diets_count()
	{
	return $this->db->count_all('diets');	
	}
	
	
	
		function getdiets($per_pg,$offset) 
		{
			/*$this->db->select('d.*, p.name'); //or whatever you're selecting
			$this->db->join('patients p', 'd.pat_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			$query = $this->db->get('diets d');
			return $query->result();
			*/
			//$query = $this->db->query("select * from diets order by id desc limit '$per_pg','$offset'");
			$this->db->order_by('id','desc'); 
			$this->db->select();
	        $query = $this->db->get('diets',$per_pg,$offset);
			return $query->result();
			
		}
		
		function count_parameter()
	{
	return $this->db->count_all('parameter_name');	
	}
		
		
		function getparameters1($per_pg,$offset) 
		{
			//$this->db->select('pp.*, p.name'); //or whatever you're selecting
			//$this->db->join('patients p', 'pp.patient_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			//$query = $this->db->get('parameters pp');
			//return $query->result();
		//$query = $this->db->query("SELECT * FROM parameter_name");			
		//return $query->result();
		    $this->db->order_by('id','desc'); 
			$this->db->select();
	        $query = $this->db->get('parameter_name',$per_pg,$offset);
		    return $query->result();
		}
	
	
	function count_activity()
		{
			return $this->db->count_all('activities');	
		}
	
	
	
	function activitylist($per_pg,$offset)
{
			  // $query = $this->db->query("select * from activities ");
			 //return $query->result();
			 //return $query->num_rows();
	        $this->db->order_by('act_id','desc'); 
			$this->db->select();
	        $query = $this->db->get('activities',$per_pg,$offset);
		    return $query->result();
	 
	 
}
	
		
		
		
	function getpatients($oid)
	{
	
       $this->db->select();
       $this->db->where('own_id', $oid); 
       $query = $this->db->get('patients');
	   return $query->result();
		
		
	}
	
	function getcaregiver($oid)
	{
		/*
	  $query = $this->db->get('patients');
	  $this->db->where('own_licence', 33);  
	  return $query->result();
	  */
	  // $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       //$this->db->where('own_licence', $lid); 
	   $this->db->where('own_id', $oid); 
       $query = $this->db->get('caregiver');
	   return $query->result();
		
		
	}	
	
	function medicinebyname($name) {
	  $this->db->where('med_name', $name);  
	  $query = $this->db->get('medicine');		
	 // return $query->result();
	  return $query->num_rows();
	}
	
	
	function addMedicines() {
		
	    $this->med_name   = $_POST['medicine_name']; 
		$this->medformat = $_POST['medicine_format'];
		$this->med_note = $_POST['note'];
		$this->med_quantity = $_POST['quantity'];
		$this->med_purchase_date = $_POST['purchase_date'];
		$this->med_expirartion_date = $_POST['expiration_date'];
		$this->med_dose = $_POST['dosage'];
		$this->med_meal_time = $_POST['meal_time'];
		$this->med_price = $_POST['price'];
		
		$this->db->insert('medicine', $this);
	}
	
	
	
	
   	function medicineDetail($id) {
	  $this->db->where('med_id', $id);  
	  $query = $this->db->get('medicine');		
	  return $query->result();
	
	}
	
	function updateMedicine($id) {
	  
	    $this->med_name   = $_POST['medicine_name']; 
		$this->medformat = $_POST['medicine_format'];
		$this->med_note = $_POST['note'];
		$this->med_quantity = $_POST['quantity'];
		$this->med_purchase_date = $_POST['purchase_date'];
		$this->med_expirartion_date = $_POST['expiration_date'];
		$this->med_dose = $_POST['dosage'];
		$this->med_meal_time = $_POST['meal_time'];
		$this->med_price = $_POST['price'];
		
        $this->db->update('medicine', $this, array('med_id' => $_POST['id']));
	}
	function deleteMedicines($id) {
	 $this->db->delete('medicine', array('med_id' => $id));
	
	}
	
   //---------profesional Details---------//
    function getProfessionalsAlls() {
        $this->db = $this->load->database('default', TRUE);  
        $this->db->select();
		 $this->db->where('professional_id', $id); 
        $query = $this->db->get('professionals');
        return $query->result();
    }
	
	
	//---------caregiver details-----------//
	function get_caregivers_by_id( $id=null ) {
	   $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       $this->db->where('caregiver_id', $id); 
       $query = $this->db->get('patients');
	   return $query->result();
	}
	//---------professional details-----------//
    function get_professionals_by_id( $id=null ) {
	
	   $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       $this->db->where('id', $id); 
       $query = $this->db->get('professionals');
	   return $query->result();
	}
	
	//------------Search module----------//
	function caregiverList() {
	  $this->db = $this->load->database('default', TRUE);  
	 $query = $this->db->query("select * from caregiver ");
	 return $query->result();
	
	}
	
	function getprof($email)
	{
		$query = $this->db->query("select * from professionals where email='$email'");
	   // return $row=$query->row_array();
	   
	   return $row=$query->result();
	   
	}
	
	
	function caregiverSearch() {
		$this->db->like('username', $_POST['search']);
		$this->db->where('role', 'caregiver');
		$query = $this->db->get('users');
		return $query->result();
	
	}
	function professionalList() {
	 $this->db->where('role', 'professional');
	 $query = $this->db->get('users');
	 return $query->result();
	
	}
	function professionalSearch() {
		$this->db->like('username', $_POST['search']);
		$this->db->where('role', 'professional');
		$query = $this->db->get('users');
		return $query->result();
	
	}
	
	
	function getSates() {
        $this->db = $this->load->database('default', TRUE);  
        $this->db->select();
        $query = $this->db->get('users');
        return $query->result();
		
    }
	
	//-------Edit profile--------//
	function editProfile($id,$nfile,$cv,$lat,$lng) {
		//$this->email   = mysql_real_escape_string($_POST['email']);
		$this->name   =mysql_real_escape_string($_POST['name']); 
		$this->surname = mysql_real_escape_string($_POST['surname']);
		$this->age = mysql_real_escape_string($_POST['age']);
		$this->gender = mysql_real_escape_string($_POST['gender']);
		$this->city = mysql_real_escape_string($_POST['city']);
		$this->zipcode =mysql_real_escape_string($_POST['zipcode']);
		$this->state = mysql_real_escape_string($_POST['state']);
		$this->cv = $cv;
		$this->lat =$lat;
		$this->lon=$lng;
		$this->address = mysql_real_escape_string($_POST['address']);
		//$this->phone = mysql_real_escape_string($_POST['phone']);
		$this->qualification = mysql_real_escape_string($_POST['qualification']);
		$this->doctor_board_admitted_to = mysql_real_escape_string($_POST['admitted_to']);
		$this->from = mysql_real_escape_string($_POST['from']);
		$this->atime=mysql_real_escape_string($_POST['atime']);
		@$this->photo = $nfile;
		
		//$this->session->set_userdata('user_photo',$this->session->userdata('photo'));
		//die('ok');
       return $this->db->update('professionals', $this, array('id' => $id));
		
	}
	
	//---------------edit caregivers profile---//
	//-------Edit profile--------//
	function editcaregiverProfile($id) {
	
		///$this->username   =$_POST['username']; 
		//$this->role   =$_POST['role'];
		//$this->db->update('users', $this, array('id' => $_POST['id']));		
		$this->name   =$_POST['name']; 
		$this->surname = $_POST['surname'];
		$this->age = $_POST['age'];
		$this->gender = $_POST['gender'];
		$this->zipcode = $_POST['zipcode'];
		//$this->city = $_POST['city'];
		$this->state = $_POST['state'];
		$this->address = $_POST['address'];
		$this->phone = $_POST['phone'];
		$this->phone2 = $_POST['phone2'];
		$this->phone3 = $_POST['phone3'];
		$this->contact1 = $_POST['contact1'];
		$this->contact2 = $_POST['contact2'];
		$this->contact3 = $_POST['contact3'];
		$this->pathology = $_POST['pathology'];
		$this->allergies = $_POST['allergies'];
		$this->intolerances = $_POST['intolerances'];
		$this->note = $_POST['note'];
		$this->photo = $this->session->userdata('photo');
		if($_POST['qr_code']){
		 $this->id_card=$_POST['qr_code'];
		}
		//print_r($_POST);
		//die();
		
		$this->db->update('patients', $this, array('caregiver_id' => $_POST['id']));
	}
	
	
	
	//------end------------//
	//------------------------Update foods---------------//
	
			
	function updateFood($id) {

				$this->food_name   = $_POST['food_name']; 
				$this->food_type = $_POST['food_type'];
				$this->created = $_POST['created'];
				$this->db->update('foods', $this, array('id' => $_POST['id']));
			}	
	function foodDetail($id) {
				$this->db->where('id', $id);  
				$query = $this->db->get('diets');		
				return $query->result();

			}
	function deletefoods($id) {
			$this->db->delete('foods', array('id' => $id));

			}
			
	//--------------------------end---------------------------//

	//------------------------Update Parameters---------------//
	
			
	function updateparameters($id,$pname) 
	{
				$data=array(
			'pname'=>$pname,
			      );
	$this->db->update('parameter_name', $data, array('id' => $id));
			}


function update_paracolumns($id,$name)
{
	$data=array(
			'para_id'=>$id,
			'paraname'=>$name,
      );
	  
	  $this->db->update('parameter_columns', $data, array('para_id' => $id));
	}
			
			
			
				
	function parametersDetail($id) {
		$this->db->where('id', $id);  
		$query = $this->db->get('parameter_name');		
		return $query->result();

	}
	function deleteparameters($id) {
		$this->db->delete('parameter_name', array('id' => $id));
		$this->db->delete('parameter_columns', array('para_id' => $id));

	} 
	function patientDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('patients');		
	  return $query->result();
	
	}
	function careDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('caregiver');		
	  return $query->result();
	
	}
	
	
	
		
	/* serach patients   */	
	function serach_patients($oid,$srch)
	{ 
	 
	    $oid=$_POST['oid'];
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  and ';
				//$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  and ';
				
			
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		
		$query = $this->db->query("SELECT * FROM patients where name  like '%$srch%' and own_id='$oid'");			
				
		return $query->result();

	
	
	 
	 
	}	
	
	
	function serach_caregiver($oid,$srch)
	{
	  //$oid=$_POST['oid1'];
		/*$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  and ';
				//$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				
			
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		*/
		
		$query = $this->db->query("SELECT * FROM caregiver where name like '%$srch%' and own_id='$oid'");			
				
		return $query->result();

	
	
	}
	
	
	
		
			
		
    //-- get professional all--//
    function getProfessionals() {  
		$query = $this->db->get('professionals');
		return $query->result();
	}
	function getProfessionals1($pid) {  
		$query = $this->db->query("SELECT prf.id as pfid,pnet.netstatus,prf.name,prf.age,prf.gender,prf.photo,pnet.rid,pnet.sid FROM professionals as prf left join professional_networks_prf as pnet on (prf.id=pnet.rid or prf.id=pnet.sid) and ( pnet.rid='$pid' or pnet.sid='$pid') where prf.id!='$pid' group by prf.id");
		return $query->result();
	}
	
	
	
//--  search Drug   --//	
	function serach_drug()
	{
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`med_name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM medicine $searchkey");			
				
		return $query->result();

	}
	
	
	function serach_deits()
	{
		
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`food_name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM diets $searchkey");			
				
		return $query->result();

	
	}
	
	
	
	function serach_activity()
	{
	
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`act_name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM activities $searchkey");			
				
		return $query->result();

	
	
	}
	

function serach_owner()
	{
	
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`own_name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM owner_table $searchkey");			
				
		return $query->result();

	
	
	}

function serach_owner_date($date1,$date2)
	{
	
		
		$query = $this->db->query("SELECT * FROM owner_table where own_date between '$date1' and '$date2' ");			
				
		return $query->result();

	
	
	}








	
	
	//-- search professional --//
    function searchProfessionals() { 
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !=''){
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				$searchkey .= '`zipcode` like "%'.mysql_real_escape_string($temp).'%"  OR ';	
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM professionals $searchkey");			
				
		return $query->result();

		/*	
        $this->db->like('name', $_POST['keyword']);		
        $this->db->like('surname', $_POST['keyword']);		
		$query = $this->db->get('professionals');
		return $query->result(); */
	}
	
    //-----Get States---//
    
	function getStates() {
        $this->db = $this->load->database('default', TRUE); 
        $this->db->select();
        $query = $this->db->get('countries');
        return $query->result();
    }
	
	///////////////////////////////////////////
    //---------caregiver details-----------//
	function getCaregivers_by_id( $id=null ) {
	   $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       $this->db->where('id', $id); 
       $query = $this->db->get('patients');
	   return $query->result();
	}
	//---------professional details-----------//
    function getProfessionals_by_id( $id=null ) {
	
	   $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       $this->db->where('id', $id); 
       $query = $this->db->get('professionals');
	   return $query->result();
	}
	
	function deleteactiviy($id)
{
	//$query = $this->db->query("delete  from activities where act_id='$id'");
	$this->db->delete('activities', array('act_id' => $id));
    // return $query->result();
}

function updateactivity($id) {
if(!empty($_FILES['attached']['name']))
{
$file=time().$_FILES['attached']['name'];
				$dis="activity/";
				$dis=$dis.$file;
				move_uploaded_file($_FILES['attached']['tmp_name'],$dis);
}
else
{
	$file=$_POST['attached'];
}
	            $this->act_name   = $_POST['act_name']; 
				$this->act_details = $_POST['actdetails'];
				$this->act_attached = $file;
				$this->upated_date = date("Y-m-d");
				//$this->act_dayof_week=$_POST['dateofweek'];
				//$this->db->insert('activities', $this);
				$this->db->update('activities', $this, array('act_id' => $_POST['id']));

			}	
	
	
	
	
   //deleting the the patient data 
	function deletepatient($id) 
	{
	$this->db->delete('patients', array('id' => $id));
	} 
	
	
	
	function caregiver_patients($cid){
		
	   $query = $this->db->query("select *,ps.id as pid from patients as ps , list_caregiver as cs where ps.id=cs.pat_id and cs.careid='$cid' ");
      
	     $res = $query->num_rows();
			
			if($res<=0){
				return $res;
			} else {
			return $query->result();
			
			}
	
	}
	
	
	///////////////
	
	
	//After professional login
	
	function getPatients2($pid) {		
	
	 $query = $this->db->query("SELECT * FROM patients as T1 , professional_networks as T2, list_caregiver as T3 where (T1.id = T3.pat_id) and (T2.sid=T3.careid)  and ( T2.rid='$pid') group by T1.id");
	

	return $query->result();
	 
		
	}
	
	
	function getprofetionals123($pid)
	{
        $query= $this->db->query("select *,care.id as cid from professional_networks as pnet, caregiver as care  where (pnet.rid='$pid') and pnet.netstatus=1 and care.id=pnet.sid");
		return $query->result();
		
	}
	
	
	function getProfessionals_all($cid) {  
	  	
		$query = $this->db->query("SELECT prf.id as pfid,pnet.netstatus,prf.name,prf.age,prf.gender,prf.photo FROM professionals as prf left join professional_networks as pnet on prf.id=pnet.rid and pnet.sid='$cid' group by prf.id");   
    
		 $res = $query->num_rows();
			
			if($res<=0){
				return $res;
			} else {
			return $query->result();
			
			}
		
		
	}
	
	
	function get_patient_details($oid)
	 {
		$query = $this->db->query("select * from patients where own_id='$oid'"); 
		return $query->result();
	 }
	
	
	function getnotification_new($pid,$date,$user_type)
	{
		
		//$query = $this->db->query("select * from notifications as noti ,event_type as ety where noti.event_type_id=ety.evn_id and noti.pat_id='$pid'");
		
		if($user_type=="caregiver"){
		
		$query = $this->db->query("select * from notifications as ns,event_type as evs where  ns.pat_id='$pid' and ns.note_date='$date' and evs.evn_id=ns.event_type_id order by note_id desc");	
		} else {
		$query = $this->db->query("select * from notifications as ns,event_type as evs where  ns.pat_id='$pid' and ns.note_date='$date'  and evs.evn_id=ns.event_type_id order by note_id desc");	
		
		}
		
       $res = $query->num_rows();
			
			if($res<=0){
				return $res;
			} else {
			return $query->result();
			
			}
	}
	
	function get_owner_caretype($pid,$oid)	
	{
		$query = $this->db->query("select owner_caretype from patients where id='$pid' and own_id='$oid'");
        return $query->result();
	}
	
	//Adding Caregiveer
	function get_patient_licence($pid)
	{
		$query = $this->db->query("SELECT * FROM patients where id='$pid'");			
		return $query->result();
	}
	
	function get_patient_type($lice)
	{
		$query = $this->db->query("SELECT * FROM owner_licence where licence_no='$lice'");			
		return $query->result();
	}
	
	
	////Caregiver Add rotines
	
	function get_caregiver_routines($pid,$eid){
	
		if($eid==1){
			$query = $this->db->query("SELECT * FROM parameter_name WHERE (pat_id='$pid' or pat_id='0') and is_delete!='1'");			
			return $query->result();
		}
		
		if($eid==2){
			$query = $this->db->query("SELECT * FROM medicine WHERE (pat_id='$pid' or pat_id='0') and is_delete!='1'");			
			return $query->result();
		}
		
		if($eid==3){
			$query = $this->db->query("SELECT * FROM diets WHERE (pat_id='$pid' or pat_id='0') and is_delete!='1'");			
			return $query->result();
		}
		
		if($eid==4){
			$query = $this->db->query("SELECT * FROM activities WHERE (pat_id='$pid' or pat_id='0') and is_delete!='1'");			
			return $query->result();
		}
		
		if($eid==5){
			$query = $this->db->query("SELECT * FROM sub_event_type where evn_id='$eid' ");			
			return $query->result();
		}
		
	}
	
	//Caregiver edit routine
	
	function view_notification($id)
	{
		$query = $this->db->query("SELECT * FROM notifications where note_id='$id'");			
		return $query->result();
	}
	
	
	function status_notification($id,$cid1){
		$query = $this->db->query("SELECT * FROM notifications where note_id='$id'  and status='-1'");			
		  return $res = $query->num_rows();
		
	}
	
	
function edit_notification($nid)
	{
		
		$status=$_POST['status'];
		$time=$_POST['time']; 
		$pid=$_POST['patient_id'];
		$cid1=$_POST['user_id'];
		$name = $_POST['user_disp_name'];
		$pname=$_POST['patient_name'];
		
		$ns="select * from notifications where note_id='$nid'";
		$ne=mysql_query($ns) or die(mysql_error());
		$nr=mysql_fetch_array($ne);
		$mstatus=$nr['status'];
		$evenid=$nr['event_type_id'];
		$ip=$_SERVER['REMOTE_ADDR'];
	    $date=date("Y-m-d");	
		if($status==1)
		{
			$mtxt="done";
		}
		else
		{
			$mtxt="not done";
		}
		
		
		$ms="select * from list_caregiver as lcare, caregiver as care where lcare.careid=care.id and lcare.pat_id='$pid' and lcare.careid!='$cid1'";
		$me=mysql_query($ms) or die(mysql_error());
		
		while($mr=mysql_fetch_array($me))
		{
		$careid=$mr['careid'];
		$online=$mr['is_online'];
		$cs="select * from set_notifications where car_id='$careid' and withp='no'";
		 $ce=mysql_query($cs) or die(mysql_error());
		 $mcr=mysql_fetch_array($ce);
		  if($evenid==1)
		  {
		   $mstatus=$mcr['parameter_status'];
		  }
		  if($evenid==2)
		  {
		   $mstatus=$mcr['medical_status'];
		  }
		  if($evenid==3)
		  {
		   $mstatus=$mcr['diets_status'];
		  }
		  if($evenid==4)
		  {
		   $mstatus=$mcr['activity_status'];
		  }
		
		   if($mcr['withp']=='no' && $mstatus==1)     
			{
		
		$cs="select * from caregiver where id='$careid'";
		$ce=mysql_query($cs) or die(mysql_error());
		$cr=mysql_fetch_array($ce);
		$rname=$cr['name'];		
		$stype="enotification";
		$to=$careid;
		$from=$cid1;
		$sname=$name;
		$res="caregiver";
		$loginusertye=$res;
		$logintype=$res;
		if($evenid==1)
		{
			$ename="Clinical parameters";
		}
		if($evenid==2)
		{
			$ename="Drugs";
		}
	   if($evenid==3)
		{
			$ename="Diet";
		}
		if($evenid==4)
		{
			$ename="Activities";
		}

		if($evenid==5)
		{
			$ename="Sleep / wake";
		}
  
		$ename=strtolower($ename);
		$message="$ename event is ".$mtxt." by ". $name. " for the patient ". $pname;
		$rid=$mr['careid'];
		$mps="insert into popup_notifications (note_message,n_sid,n_rid,nm_id,note_type,n_ip,n_date,ltype,care_online) values('$message','$cid1','$rid','$nid','$stype','$ip','$date','$res','$online')";
		$mpe=mysql_query($mps) or die(mysql_error());
	
		
		$ds="select * from device_tocken where userid='$careid' and user_type='$res'";
		$se=mysql_query($ds) or die(mysql_error());
		$sn=mysql_num_rows($se);
		if($sn>0)
		{
		 
			while($sr=mysql_fetch_array($se))
			{  
			
				$device=$sr['tockenid'];
				$device_platform = $sr['platform'];
		
				$msg = array(
								'message'       => $message,
								'title'         => $name.' has updated the event '.$ename,
								'subtitle'      => 'update patient event',
								'sid'           =>$from,
								'rid'           =>$to,
								'sname'         =>$name,
								'recivertype'   =>$loginusertye,
								'logintype'     =>$logintype,
								'chat'          =>"carenotification", 
								'call_type'     =>"eventupdated",
								'rname'         =>$rname,
								'ins'           =>$nid,
								'pid'         =>$pid,
								'pname'           =>$pname,
								
								'event_type'    =>$ename,
								'vibrate'        => 1,
								'sound'         => 1
							);	 
				 $call_type="eventupdated";
				 $ins=$nid;
				 $event_type=$ename;
		 
		 
		 
				$res = $this->sendcaregiver_notification($device,$msg,$device_platform,$name,$from,$to,$loginusertye,$logintype,$rname,$ename,$message,$call_type,$ins,$event_type,$pid,$pname);
				
	 
			}
	
	
		}
	
			}
		}
		
		$us="update notifications set status='$status',note_time='$time',edit_cid='$cid1' where note_id='$nid'";
		$ue=mysql_query($us) or die(mysql_error());
		return $ue;
		
	}
	
	function view_update_notification($id,$user_id){
		$us="update popup_notifications set status='1' where n_id='$id' and n_rid='$user_id'";
		$ue=mysql_query($us) or die(mysql_error());
	}
	
	
	//caregiver add activities
	
	function insert_activity($patient_id,$caregiver_id,$actdetails,$acttype,$actname)
	{
		$date=date("Y-m-d");
		$data=array(
		'pat_id'=>$patient_id,
		'care_id'=>$caregiver_id,
		'act_type'=>$acttype,
		'act_details'=>$actdetails,
		'act_name'=>$actname,
		//'act_attached'=>$file,
		//'act_dayof_week'=>$dateofweek,
		'act_date'=>$date,
		);
		
		
		
		$query = $this->db->query("SELECT * FROM activities where act_name='$actname'");			
		//return $query->result();
		
		$res = $query->num_rows();
		//echo json_encode(array("result" =>"success","info"=>$res), 200); exit;
		if($res>0){
			//return $res;
			echo json_encode(array("result" =>"","message" =>"Activity already exists."), 204); exit;
		} else {
		
			$insert=  $this->db->insert('activities', $data);
			return $insert;
			
		}
	}
	
	
	
	//Caregiver edit activities
	
	function view_activity($id)
	{
		$query = $this->db->query("SELECT * FROM activities where act_id='$id'");			
		return $query->result();
	}
	
	///edit
	//caregiver add activities
	
	function edit_activity($act_id,$patient_id,$caregiver_id,$actdetails,$acttype,$actname)
	{
		$date=date("Y-m-d");
		$data=array(
		'pat_id'=>$patient_id,
		'care_id'=>$caregiver_id,
		'act_type'=>$acttype,
		'act_details'=>$actdetails,
		'act_name'=>$actname,
		//'act_attached'=>$file,
		//'act_dayof_week'=>$dateofweek,
		'act_date'=>$date,
		);
		
		
		
		$query = $this->db->query("SELECT * FROM activities where act_name='$actname' and act_id<>'$act_id'");			
		//return $query->result();
		
		$res = $query->num_rows();
		//echo json_encode(array("result" =>"success","info"=>$res), 200); exit;
		if($res>0){
			//return $res;
			echo json_encode(array("result" =>"","message" =>"Activity already exists."), 204); exit;
		} else {
		
			$query = $this->db->query("UPDATE activities set act_name='$actname', act_details='$actdetails', upated_date='$date' where act_id='$act_id'");
			
		}
	}
	
	
	//Caregiver add medecin
	
	function get_medlist($name,$pid,$med_id)
	{
		   $this->db->select();
		   $this->db->where('med_name', $name); 
		   $this->db->where('pat_id', $pid); 
		   if($med_id) { $this->db->where('med_id !=', $med_id); }
		   
		   $query = $this->db->get('medicine');
		   return $query->num_rows();	
	}
	
	
	function insert_medicine($med_id,$patient_id,$caregiver_id,$medname,$med_type,$med_note,$med_qua,$pre_date,$exp_date,$med_price,$meal_type,$med_dose,$checkboxFourInput,$checkboxFourInput1,$checkboxFourInput2)
	{
		$date=date("Y-m-d");
		$data=array(
		'pat_id'=>$patient_id,
		'care_id'=>$caregiver_id,
		'med_name'=>$medname,
		'medformat'=>$med_type,
		'med_note'=>$med_note,
		'med_quantity'=>$med_qua,
		'med_purchase_date'=>$pre_date,
		'med_expirartion_date'=>$exp_date,
		'med_price'=>$med_price,
		'med_meal_time'=>$meal_type,
		'med_dose'=>$med_dose,
		'created'=>$date,
		'checkboxFourInput'=>$checkboxFourInput,
		'checkboxFourInput1'=>$checkboxFourInput1,
		'checkboxFourInput2'=>$checkboxFourInput2
		 );
		
		
		
		
		
		if($med_id) {
		
			$query = $this->db->query("SELECT * FROM medicine where med_name='$medname' and med_id<>'$med_id'");			
			$res = $query->num_rows();
			if($res>0){
				echo json_encode(array("result" =>"","message"=>"Drug Name already exists"), 204); exit;
			} else {
				return $this->db->update('medicine', $data, array('med_id' => $med_id));
			}
		} else {
			
			$query = $this->db->query("SELECT * FROM medicine where med_name='$medname'");			
			$res = $query->num_rows();
			if($res>0){
				echo json_encode(array("result" =>"","message"=>"Drug Name already exists"), 204); exit;
			} else {
				return $this->db->insert('medicine', $data);
			}
		
		}
		
	}
	
	//view medicine
	function get_medicine($id)
	{
		$query = $this->db->query("SELECT * FROM medicine where med_id='$id'");			
		
		return $query->result();		
	}
	
	
	//Caregiver insert diets
	
	function insert_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$file,$foodlike)
	{
		
		
		$date=date("Y-m-d");
		$data=array(
		'pat_id'=>$patient_id,
		'care_id'=>$caregiver_id,
		'food_name'=>$foodname,
		'food_type'=>$foodtype,
		'food_desc'=>$fooddel,
		'food_image'=>$file,
		'diet_day'=>$dateofweek,
		'created_date'=>$date,
		'food_like'=>$foodlike,
		);
		$this->db->insert('diets', $data);
		return $id=$this->db->insert_id();
		
		
		
	}
	
	//get diets
	function get_diets($id)
	{
	$query = $this->db->query("SELECT * FROM diets where id='$id'");			

	return $query->result();		
	}

	function update_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$file,$foodlike,$id)
	{
		$date=date("Y-m-d");
		$data=array(
		'pat_id'=>$patient_id,
		'upd_careid'=>$caregiver_id,
		'food_name'=>$foodname,
		'food_type'=>$foodtype,
		'food_desc'=>$fooddel,
		'food_image'=>$file,
		'diet_day'=>$dateofweek,
		'updated_date'=>$date,
		'food_like'=>$foodlike,
		);
		
		return $this->db->update('diets', $data, array('id' => $id));
		
	}
	
	
	//Add parameter_columns
	function addparameters($param_id,$pname,$numclu,$date,$patient_id) 
		{
			$data=array(
			'pname'=>$pname,
			'p_date'=>$date,
			'ncolumns'=>$numclu,
			'pat_id'=>$patient_id,
			);
			
			if($param_id){
					 $this->db->update('parameter_name', $data, array('id' => $param_id));
			} else {
				 $this->db->insert('parameter_name', $data);
				 return $id=$this->db->insert_id();
			}
			
	
		}
		
		function add_paracolumns($param_id,$id,$name)
		{ if($param_id) $k = $param_id; else $k = $id;
			$data=array(
			
					
					
					'para_id'=>$k,
					'paraname'=>$name,
			  );
			
			return $this->db->insert('parameter_columns', $data);
		}
		
		
		function view_perameter($id){
			$query = $this->db->query("select *  from parameter_name as pname,parameter_columns as pcol  where pname.id='$id' and pcol.para_id='$id'");
			return $query->result();	
		}
	
	
	function updateownerlicencestatus1($licence)	
	{
	$data=array(
	'licencetype'=>"Extended",
		 );
	$this->db->update('owner_licence', $data, array('licence_no' => $licence));
	}

	
	function get_owner_by_licence($licence,$oid)
	{
      
	   $query = $this->db->query("select * from owner_licence where licence_no='$licence' and licencetype<>'Extended' and own_id='$oid'");
      //return $query->result();
	   return $query->num_rows();
	}	
	
	///////////
	function check_token_id($userid,$tockenid,$platform,$user_type){
		
		$query = $this->db->query("select * from device_tocken where tockenid='$tockenid' and user_type='$user_type'");
        $token_count =  $query->num_rows();
		
		
		
		return $token_count;
		
	}
		
	function update_token_id($userid,$tockenid,$platform,$user_type){
		
			$query = $this->db->query("select * from device_tocken where userid<>'$userid' and tockenid='$tockenid' and user_type='$user_type'" );
			$token_count =  $query->num_rows();
			
			if($token_count>0){
				
				$data=array(
				'userid'=>$userid
				
				);
		
				 $this->db->update('device_tocken', $data, array('tockenid' => $tockenid));
				
				
			}
		
		
	}
	
	
	function insert_token_id($userid,$tockenid,$platform,$user_type){
		
			$data=array(
				'userid'=>$userid,
				'tockenid'=>$tockenid,
				'platform'=>$platform,
				'user_type' => $user_type
				
				);
		
				 return $this->db->insert('device_tocken', $data);
		
	}
	
	
	function delete_token_id($userid,$tockenid,$user_type){
		
		//echo "delete from device_tocken where userid='$userid' and tockenid='$tockenid' and user_type='$user_type'";
		//exit;
		
		$query = $this->db->query("delete from device_tocken where userid='$userid' and tockenid='$tockenid' and user_type='$user_type'");
		
		
		
		//echo $query->result();	exit;
		
		
		
	}
	
	
	//GET NOTIFICATIONS
	function get_notifications_count_unread($user_id,$user_type){
		
		$query = $this->db->query("select * from popup_notifications where n_rid='$user_id' and ltype='$user_type' and status=0");
        return  $query->num_rows();
		
	}
	
	
	function get_notifications_count($user_id,$user_type){
		
		$query = $this->db->query("select * from popup_notifications where n_rid='$user_id' and ltype='$user_type'");
        return  $query->num_rows();
		
	}
	
	
	function get_notifications($user_id,$user_type){
		$query = $this->db->query("select * from popup_notifications where n_rid='$user_id' and ltype='$user_type' order by n_id desc");
			return $query->result();
	}
	
	
	//Get waiting requests
	
	////Requesting waiting list professional from caregivers
	
	function get_wainting_requests_count($user_id){
		$query = $this->db->query("select * from professional_networks where rid='$user_id' and netstatus=0");
			        return  $query->num_rows();
			
	}	
	
	function get_wainting_requests($user_id){
		$query = $this->db->query("select * from professional_networks where rid='$user_id' and netstatus=0");
			return $query->result();
			
	}	
	
	
	
	function sendcaregiver_notification($deviceid,$msg1,$device_platform,$name,$from,$to,$loginusertye,$logintype,$rname,$ename,$mesg,$call_type,$ins,$event_type,$pid,$pname)
{
	$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

			//echo $device_platform; exit;
		if($device_platform=="Android"){
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
				/*if($status==1)
				{
					$txt="done";
				}
				else
				{
					$txt="not done";
				}*/
			//$message = $name.' has '.$txt.' the event '.strtolower($ename)." ";
			$message=$mesg;
			
			$path = base_url().'assets/KoalaProPushCert.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
		$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'sid'           =>$from,
				'rid'           =>$to,
				'sname'         =>$name,
				'recivertype'   =>$loginusertye,
				'logintype'     =>$logintype,
				'chat'          =>"carenotification", 
				//'call_type'     =>"eventupdated",
				//'session_id'    =>$var,
				'call_type'     =>$call_type,
				'ins'           =>$ins,
					'pid'         =>$pid,
								'pname'           =>$pname,
								
				'event_type'    =>$event_type,
				'rname'         =>$rname
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				 
		}
		
		 $result;
}
	function parametesinsert($caregiver_id,$patient_id,$praid,$opt1,$opt2,$opt3,$opt4,$opt5)
{
	
$data=array(
	'option1'=>$opt1,
	'option2'=>$opt2,
	'option3'=>$opt3,
	'option4'=>$opt4,
	'option5'=>$opt5,
	'care_id'=>$caregiver_id,
	'patient_id'=>$patient_id,
	'para_id'=>$praid,
	'created'=>date("Y-m-d"),
	);
	
$this->db->insert('parameters', $data);	
	
	
	

}

	///Notification Settings
	function get_notify($cid,$pid)
	 { //echo "select * from set_notifications where car_id='$cid' and pat_id='$pid' order by note_id desc"; exit;
	$query = $this->db->query("select * from set_notifications where car_id='$cid' and pat_id='$pid' order by note_id desc");
		   return $query->result(); 
	 }
	 
	 function update_set_notification($patient_id,$caregiver_id,$parameter,$medicine,$diet,$activity,$cdate,$nid,$withp)
	{ 

	 $data=array('pat_id'=>$patient_id,'car_id'=>$caregiver_id,'parameter_status'=>$parameter,'medical_status'=>$medicine,'diets_status'=>$diet,
	 'activity_status'=>$activity, 'withp'=>$withp, 'cdate'=>$cdate,'ip'=>$_SERVER['REMOTE_ADDR'],);
	 
	 return $this->db->update('set_notifications', $data, array('note_id' => $nid));
	}
	//fetching pending events  upto current time
       
          function getPendingEventsByPatient($patient_id){
              $milliseconds = round(microtime(true) * 1000);
           
              $query = $this->db->query("select count(*) as count from notifications where pat_id =".$patient_id." and status='-1' and gmt_time < ".$milliseconds);
          
               return $query->row_array();
          }
		  
		  
		  function getPendingEventsByPatient1($patient_id,$c_id){
				$ms1="select * from set_notifications where car_id='$c_id'";
				$me1=mysql_query($ms1);
				$mr1=mysql_fetch_array($me1);
				
				if($mr1['withp']=='yes'){
					$milliseconds = round(microtime(true) * 1000);
					$query = $this->db->query("select count(*) as count from notifications where pat_id =".$patient_id." and status='-1' and gmt_time < ".$milliseconds);
          
					return $query->row_array();
					
				} else return 0;
			  
			  
			  
          }
		  
	
	// Pending event details by patient id
        function getPendingEventdetails($patient_id){
              $milliseconds = round(microtime(true) * 1000);
           
              $query = $this->db->query("select * from notifications where pat_id =".$patient_id." and status='-1' and gmt_time < ".$milliseconds);
          
               return $query->result_array();
          }
	
	
	
	
	
	// Pending event details by patient id
	function getPendingmoredetails($patient_id,$eventid)
	{
		  $milliseconds = round(microtime(true) * 1000);
		 $date=date("Y-m-d");
		  $query = $this->db->query("select * from notifications where pat_id ='$patient_id' and event_type_id='$eventid' and note_date >= '$date' and status='-1'");
	  
		   return $query->result_array();
	}
	
	
	function insert_agenda($title,$details,$time,$date,$pid)
	{
		$data=array(
		'title'=>$title,
		'details'=>$details,
		'time'=>$time,
		'date'=>$date,
		'prof_id'=>$pid
		);
		return $this->db->insert('agenda', $data);	
		
	}
	
	function getagenda1($pid)
	{
		
	   $query = $this->db->query("select * from agenda where prof_id='$pid'");
       return $query->result();
		
	}
	
	function getagenda_info($pid,$agenda_id)
	{
		
	   $query = $this->db->query("select * from agenda where prof_id='$pid' AND id='$agenda_id'");
       return $query->result();
		
	}

	
	
	function get_caregiver1($email)
	{
			
		$query = $this->db->query("SELECT * FROM caregiver where email='$email'");			
		return $row=$query->row_array();
			
	}
	
	
	function getconsultaion_new($iRecipient)
	{
		//$query= $this->db->query("select * from cons_request_messages as cons   WHERE ((cons.`sender` = '{$iRecipient}' && cons.stype=1) && (cons.`recipient` = '{$iPid}' && cons.rtype=1) || (cons.`sender` = '{$iRecipient}' && cons.stype=0) && (cons.`recipient` = '{$iPid}' && cons.rtype=1) ) || ((cons.`recipient` = '{$iRecipient}' && cons.stype=1) && (cons.`sender` = '{$iPid}' && cons.rtype=1) || (cons.`recipient` = '{$iRecipient}' && cons.stype=1) && (cons.`sender` = '{$iPid}' && cons.rtype=0) )  order by cons.id asc");
		
		$query= $this->db->query("select * from cons_request_messages WHERE recipient=".$iRecipient." order by id desc");
		
		return $query->result();
		
	}
	
	function show_Consultation_Request($id)
	{
		//$query= $this->db->query("select * from cons_request_messages as cons   WHERE ((cons.`sender` = '{$iRecipient}' && cons.stype=1) && (cons.`recipient` = '{$iPid}' && cons.rtype=1) || (cons.`sender` = '{$iRecipient}' && cons.stype=0) && (cons.`recipient` = '{$iPid}' && cons.rtype=1) ) || ((cons.`recipient` = '{$iRecipient}' && cons.stype=1) && (cons.`sender` = '{$iPid}' && cons.rtype=1) || (cons.`recipient` = '{$iRecipient}' && cons.stype=1) && (cons.`sender` = '{$iPid}' && cons.rtype=0) )  order by cons.id asc");
		
		$query= $this->db->query("select * from cons_request_messages WHERE id=".$id."");
		return $query->result();
		
	}
	
	
	
	
	function group_edit_notification($status,$nid,$patid,$cid1,$name,$pname)
	{
		
		//$status=$_POST['status'];
		//$time=$_POST['time']; 
		$pid=$patid;//$_POST['patient_id'];
		//$cid1=$_POST['user_id'];
		//$name = $_POST['user_disp_name'];
		//$pname=$_POST['patient_name'];
		
		$ns="select * from notifications where note_id='$nid'";
		$ne=mysql_query($ns) or die(mysql_error());
		$nr=mysql_fetch_array($ne);
		$mstatus=$nr['status'];
		$evenid=$nr['event_type_id'];
		$ip=$_SERVER['REMOTE_ADDR'];
	    $date=date("Y-m-d");	
		if($status==1)
		{
			$mtxt="done";
		}
		else
		{
			$mtxt="not done";
		}
		
		
		$ms="select * from list_caregiver as lcare, caregiver as care where lcare.careid=care.id and lcare.pat_id='$pid' and lcare.careid!='$cid1'";
		$me=mysql_query($ms) or die(mysql_error());
		
		while($mr=mysql_fetch_array($me))
		{
		$careid=$mr['careid'];
		$online=$mr['is_online'];
		$cs="select * from set_notifications where car_id='$careid' and withp='no'";
		 $ce=mysql_query($cs) or die(mysql_error());
		 $mcr=mysql_fetch_array($ce);
		  if($evenid==1)
		  {
		   $mstatus=$mcr['parameter_status'];
		  }
		  if($evenid==2)
		  {
		   $mstatus=$mcr['medical_status'];
		  }
		  if($evenid==3)
		  {
		   $mstatus=$mcr['diets_status'];
		  }
		  if($evenid==4)
		  {
		   $mstatus=$mcr['activity_status'];
		  }
		
		   if($mcr['withp']=='no' && $mstatus==1)     
			{
		
		$cs="select * from caregiver where id='$careid'";
		$ce=mysql_query($cs) or die(mysql_error());
		$cr=mysql_fetch_array($ce);
		$rname=$cr['name'];		
		$stype="enotification";
		$to=$careid;
		$from=$cid1;
		$sname=$name;
		$res="caregiver";
		$loginusertye=$res;
		$logintype=$res;
		if($evenid==1)
		{
			$ename="Clinical parameters";
		}
		if($evenid==2)
		{
			$ename="Drugs";
		}
	   if($evenid==3)
		{
			$ename="Diet";
		}
		if($evenid==4)
		{
			$ename="Activities";
		}

		if($evenid==5)
		{
			$ename="Sleep / wake";
		}
  
		$ename=strtolower($ename);
		$message="$ename event is ".$mtxt." by ". $name. " for the patient ". $pname;
		
		
		$rid=$mr['careid'];
		$mps="insert into popup_notifications (note_message,n_sid,n_rid,nm_id,note_type,n_ip,n_date,ltype,care_online) values('$message','$cid1','$rid','$nid','$stype','$ip','$date','$res','$online')";
		$mpe=mysql_query($mps) or die(mysql_error());
	
		
		$ds="select * from device_tocken where userid='$careid' and user_type='$res'";
		$se=mysql_query($ds) or die(mysql_error());
		$sn=mysql_num_rows($se);
		if($sn>0)
		{
		 
			while($sr=mysql_fetch_array($se))
			{  
			
				$device=$sr['tockenid'];
				$device_platform = $sr['platform'];
		
				$msg = array(
								'message'       => $message,
								'title'         => $name.' has updated the event '.$ename,
								'subtitle'      => 'update patient event',
								'sid'           =>$from,
								'rid'           =>$to,
								'sname'         =>$name,
								'recivertype'   =>$loginusertye,
								'logintype'     =>$logintype,
								'chat'          =>"carenotification", 
								'call_type'     =>"group_event",
								'rname'         =>$rname,
								'ins'           =>$nid,
								'pid'         =>$pid,
								'pname'           =>$pname,
								
								'event_type'    =>$ename,
								'vibrate'        => 1,
								'sound'         => 1
							);	 
				 $call_type="group_event";
				 $ins=$nid;
				 $event_type=$ename;
		 
		 
		 
				$res = $this->sendcaregiver_notification($device,$msg,$device_platform,$name,$from,$to,$loginusertye,$logintype,$rname,$ename,$message,$call_type,$ins,$event_type,$pid,$pname);
				
	 
			}
	
	
		}
	
			}
		}
		
		$us="update notifications set status='$status',edit_cid='$cid1' where note_id='$nid'";
		$ue=mysql_query($us) or die(mysql_error());
		return $ue;
		
	}
	
	
	function getcaregivers_new($pid,$cid) {		
		//$query = $this->db->get('patients');
	  $query = $this->db->query("select * from caregiver where pat_id='$pid' and id!='$cid'");
       return $query->result();
		//return $query->num_rows();
		
	}
	
}

