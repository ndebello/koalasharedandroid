<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Controller_koala_local extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		
		header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
		header('Access-Control-Allow-Origin: *');  
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		
		 $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		$this->load->model('user');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('email');
		
		/*$ip = $_SERVER['REMOTE_ADDR'];  
 
		// the IP address to query
		@$query = unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
		//echo @$lat=$query['lat'];
		//@$lon=$query['lon'];
		$timezone=$query['timezone'];
		//$ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
		date_default_timezone_set("$timezone");*/
		
		
		
	}

	function index()
	{
		$this->load->helper('url');
		//$this->load->view('welcome_message');
		//print_r($_POST);
	}
	
	public function login_post()
	{
				
		if(isset($_POST['submit']))
		{
			$username=$this->input->post('username');
			$pass=$this->input->post('password');
			$password=md5($this->input->post('password'));
			$selectype=$this->input->post('selectype');
			@$_REQUEST['remember'];
			
			if(@$_REQUEST['remember'] == "on") {    // if user check the remember me checkbox        
				setcookie('remember_me', $username, time()+60*60*24*100, "/");
				setcookie('remember_pass', $pass, time()+60*60*24*100, "/");
			}
			else {   // if user not check the remember me checkbox
				setcookie('remember_me', 'gone', time()-60*60*24*100, "/"); 
				setcookie('remember_pass', 'gone', time()-60*60*24*100, "/");
			}
		}
		
		
		if(!empty($username) && !empty($password) && $selectype=="owner")
		{ 
			$use=$this->user->get_users_name($username);
			
			
			if(!empty($use)){
				$status=$use['own_status'];
				$type=$use['l_type'];
				$owner_id=$use['own_id'];
				$owner_licence=$this->user->get_users_lice($owner_id);
				
				if($status=="active")
				{
					$result=$this->user->ownlogin($username,$password);
					
				}
		 
				if($status=="active" and $result>0 and $type!="RSA" and $type!="Consortium")
				{
					//$this->session->set_userdata('owner_name', $username);
					//$this->data['flash']='';
					echo json_encode(array("result" =>"success", "user_info"=>$use), 200);
				  //echo json_encode(array('result' => 'success','status'=>1), 200);
				  //redirect('/front/after_owner_login/'.$owner_id, 'refresh');
				}
		   
				if($status=="active" and $result>0 and ($type=="RSA"))
				{
					$this->session->set_userdata('owner_name', $username);
					$this->data['flash']='';
					//redirect('/front/after_consortiam_login/'.$owner_id, 'refresh'); 
					//echo json_encode(array('result' => 'success','status'=>1), 200);
				echo json_encode(array("result" =>"success","user_info"=>$use), 200);
				}
  
				if($status=="active" and $result>0 and ($type=="Consortium"))
				{
					$this->session->set_userdata('owner_name', $username);
					$this->data['flash']='';
				  //redirect('/front/after_consortiam_login/'.$owner_id, 'refresh'); 
				  // echo json_encode(array('result' => 'success','status'=>1), 200);
			echo json_encode(array("result" =>"success","user_info"=>$use), 200);
				}
  
				if($status=="inactive")
				{
					echo json_encode(array('result' =>'' ,'status'=>0), 204);	
					//$this->data['flash']='User status is in active !'; 
				}
		  
				if(@$result<=0)
				{
					echo json_encode(array('result' =>'' ,'status'=>0), 204);	
					//$this->data['flash']='Username and password does not match !'; 
				}
		
			} else {
			
				echo json_encode(array('result' =>'' ,'status'=>0), 204);	
			}
		}
 
		if(!empty($username) and !empty($password) and $selectype=="caregiver")
		{
			$use=$this->user->get_caregiver_tologin($username,$password);
	  
			if($use<=0)
			{
				//$this->data['flash']='Please enter correct username or password of caregiver!';
				//echo json_encode(array('result' =>'' ,'status'=>0), 204);	
				echo json_encode(array('result' =>'' ,'status'=>0), 204);
			}
			else
			{
				$result= $this->session->set_userdata('owner_care', $username);
				 $email=$this->session->userdata('owner_care');
					$response_data= $this->user->caregiver_details($email);
					
				
				
					
					$ms="update caregiver set is_online='1' where email='$username'";
					$me=mysql_query($ms) or die(mysql_error());
				
					////////////////////////////
					
					$us="select * from caregiver where email='$username'";
					$ue=mysql_query($us) or die(mysql_error());
					$ur=mysql_fetch_array($ue);
					$careid=$ur['id'];
					$ls="select * from list_caregiver where careid='$careid'";
					$le=mysql_query($ls) or die(mysql_error());
					  while($lr=mysql_fetch_array($le))
					  {
					   $pid=$lr['pat_id'];
					   $date=date("Y-m-d");
					   $l="select * from set_notifications where car_id='$careid' and pat_id='$pid'";
					   $e=mysql_query($l) or die(mysql_error());
					   $n=mysql_num_rows($e);
						 if($n==0)
							 {
						   $is="insert into set_notifications(pat_id,car_id,parameter_status,medical_status,diets_status,activity_status,cdate) values('$pid','$careid','1','1','1','1','$date')";
						   $ie=mysql_query($is);
							 }
						   
						  }
					
					
					///////////////////////////////

				echo json_encode(array("result" =>"success","user_info"=>$response_data), 200);
					
					//echo '{"user_info":'. json_encode(array("result" =>"success"),200) .'}'; 
					
				//echo '{"user_info":'. json_encode($response_data,200) .'}'; 
				
				//echo json_encode($response_data,200); 
				
				//echo json_encode(array('result' => $response_data), 200);
				
				//redirect('/caregiver/search/', 'refresh');
			}
		}
	
		if(!empty($username) and !empty($password) and $selectype=="professional")
		{
			
		
			
			 $use=$this->user->get_profdetails($username,$password);
				
			//echo json_encode(array('result' =>$username ,'status'=>$password), 204);	exit;
			if($use<=0)
			{
				//$this->data['flash']='Please enter correct username or password of professional!';
				echo json_encode(array('result' =>'' ,'status'=>1), 204);	
	  
			}
			else
			{
				$response_data= $this->user->getprof($username);
					
					 $ms="update professionals set is_online='1' where email='$username'";
					$me=mysql_query($ms) or die(mysql_error());
					
				echo json_encode(array('result' => 'success',"user_info"=>$response_data), 200);
			}
		}
	} //login function closed
	
	function get_user_info(){
		$user_id = $_POST['user_id'];
		$user_type = $_POST['user_type'];
		
		//
		if($user_type=="caregiver"){
			 $query = $this->db->query("select name from caregiver where id='$user_id'");
			 $row=$query->result();
		} 
		if($user_type=="professionals"){ 
			 $query = $this->db->query("SELECT name FROM `professionals` WHERE id=".$user_id."");
			  $row = $query->result(); 
			 
			
		}
		echo json_encode(array("result" =>"success","user_info"=>$row), 200);
	}
	
	function owner_register_step1()
	{
		
	  $this->form_validation->set_rules('username', 'Email', 'trim|required|xss_clean|valid_email');
	  if ($this->form_validation->run()==TRUE) {
	  
		  @$b=$_POST['role'];
		
		 if($b=="Owner")
		 {
				echo json_encode(array("result" =>"success","role"=>"owner"), 200);
		
		 }
		 else
		 {
		 echo json_encode(array("result" =>"success","role"=>"professional"), 200);
	
		 }
	  }				
	 else
	 {
		echo json_encode(array("result" =>""), 204);
	 }
	}//Registration step 1 done
	
	////////////REgistration Setp -2 //////////////
	
	function getowners()
		{   
		  
		$num=$this->user->get_ajax_owner($_POST['username'],$_POST['type']);
		 echo json_encode(array("count" =>$num), 200);
		  
	}
	
	function ofterrsa_register()
	{
	
	
		 $email=$_POST['username'];
		 $pwd = $_POST['passwordvalue'];
		 $password=md5($_POST['passwordvalue']);
		 //echo json_encode(array("result" =>$email.$password), 200);exit;
		 if(!empty($email) and !empty($password))
		 {
		 $ups="update owner_table set own_passwd='$password' where owner_email='$email'"; 
		 $upe=mysql_query($ups) or die(mysql_error());
		 
			 if($upe){
			

					$ms="select * from owner_table where owner_email='$email'";
					$me=mysql_query($ms) or die(mysql_error());
					$mr=mysql_fetch_array($me);
					$lic="koala".$mr['own_id'];
					$uname = $mr['own_name'];
					$ltype = "RSA";
					
				    @$to = $email;
					@$to_name =$uname;
					@$subject = 'Koala owner login details.';//$_POST['subject'];
					@$body = "
						<html>
						<head>
						<title>HTML email</title>
						</head>
						<body>
						<table align='left' width='700' border='0'>
						<tr>
						<th align='left'> Name: </th>
						<td>".$uname."</td>
						</tr>
						<tr>
						<th align='left'> Subject: </th>
						<td>Koala owner login details.</td>
						</tr> 
						<tr>
						<th align='left'> User Name :</th>
						<td>".$email."</td>
						</tr>
						<tr>
						<th align='left'> Password :</th>
						<td>".$pwd."</td>
						</tr>
						<tr>
						<th align='left'> Licence  :</th>
						<td>".$lic."</td>
						</tr>
						<tr>
						<th align='left'>Owner Type</th>
						<td>".$ltype."</td></tr>
						</table>
						</body>
						</html>
						";
		
					
					$res = $this->mail_function($to,$to_name,$subject,$body);
				  
				  
				  
				  echo json_encode(array("result" =>"success"), 200);
			 } else {
				 echo json_encode(array("result" =>""), 204);
			 }
		 
		 } else {
			 echo json_encode(array("result" =>""), 204);
			 
		 }
	}
	
	
	////////////REgistration Setp -3{Consortium} //////////////
	
	
	// owner payment and registration is done here 
	function register_own_rsa_step3()
	{   
		$langselect = $_POST['langselect'];
		$own_licence=$_POST['licence'];
		$ltype=$_POST['ltype'];
		$username=$_POST['username'];
		$password=$_POST['passwordvalue'];
		$uname = $_POST['uname'];
		if(!empty($username) and !empty($password))
		{
			if($ltype=="Consortium")
			{
				$rs="select * from owner_con_licence where licence_no='$own_licence' and otype='$ltype' ";
				$re=mysql_query($rs) or die(mysql_error());
				$rn=mysql_num_rows($re);
				if($rn>0)
				{
					@$lat= substr(constant("lat"),0,9);
					@$lon= substr(constant("lon"),0,9);
					$ms="select * from owner_licence where licence_no = '".$own_licence."'";
				   //$ms="select * from owner_table where owner_email='$username'";
				   $me=mysql_query($ms) or die(mysql_error());
				   $mn=mysql_num_rows($me);
				   if($mn<=0)
				   {
						$id=$this->user->add_session_con_owner($lat,$lon,$username,$ltype,$password,$ltype,$uname);
						$mexe=$this->user->in_owner_lice1($id,$own_licence,$username,$ltype);
	   
						@$to = $username;
						@$subject = "Koala owner login details.";
						@$message = "
						<html>
						<head>
						<title>HTML email</title>
						</head>
						<body>
						<table align='left' width='700' border='0'>
						<tr>
						<th align='left'> Name: </th>
						<td>".$uname."</td>
						</tr>
						<tr>
						<th align='left'> Subject: </th>
						<td>Koala owner login details.</td>
						</tr> 
						<tr>
						<th align='left'> User Name :</th>
						<td>".$username."</td>
						</tr>
						<tr>
						<th align='left'> Password :</th>
						<td>".$password."</td>
						</tr>
						<tr>
						<th align='left'> Licence  :</th>
						<td>".$own_licence."</td>
						</tr>
						<tr>
						<th align='left'>Owner Type</th>
						<td>".$ltype."</td></tr>
						</table>
						</body>
						</html>
						";
						$headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
						// More headers
						$headers .= 'From: '." Koala Team " . "\r\n";
					
						if(mail($to,$subject,$message,$headers)){
						 // echo 'Thank you !';
						}else{
						  //echo 'Try again !'; 
						}
						//$this->data['message']="Owner Registered Successfully With Licence id";
						//$this->data['licence']=$own_licence;
						echo json_encode(array("result" =>"success","message"=>"Owner Registered Successfully With Licence id","licence"=>$own_licence), 200);
					}
					else
					{
						//$this->data['message']="<font color='red'>Owner  Already Registered Please Login</font>";
						//$this->data['licence']='';
						
						if($langselect=='Italy'){
							$msg = "Owner già registrato";
						} else{
							$msg = "Owner  Already Registered Please Login";
						}
						
						
						
						echo json_encode(array("result" =>"","message"=>$msg), 204);
					}
		
					//$this->load->view('front/owner_success1', $this->data);
				}
		
				if($rn<=0)
				{
					//$this->data['flash']= "Licence Entered Incorrect please contact to koala ";
					//$this->load->view('front/after_rsa_con_licence', $this->data);
					
					if($langselect=='Italy'){
							$msg = "Il numero di licenza inserito non è corretto, riprova o contatta Koala";
						} else{
							$msg = "Licence entered is incorrect, please retry or contact Koala";
						}
					
					
					echo json_encode(array("result" =>"","message"=>$msg), 204);
				}
			}
			else
			{
				//$this->data['flash']= "Licence Entered Incorrect please contact to koala ";
				//$this->load->view('front/after_rsa_con_licence', $this->data);
				
				if($langselect=='Italy'){
							$msg = "Il numero di licenza inserito non è corretto, riprova o contatta Koala";
						} else{
							$msg = "Licence entered is incorrect, please retry or contact Koala";
						}
				echo json_encode(array("result" =>"","message"=>$msg), 204);
			}
		}
		else
		{
			/*//echo "<script>alert('Un Successfully Please Try Again please enter the all fields ')</script>";*/
			//redirect('/auth/owner_register_step1/', 'refresh');
			if($langselect=='Italy'){
							$msg = "Errore. Riprova ad inserire nei campi";
						} else{
							$msg = "Un Successfully Please Try Again please enter the all fields";
						}
			
			
			echo json_encode(array("result" =>"","message"=>$msg), 204);
		}
	}
	
	
	
	////////////REgistration Setp -3{Freelancer} //////////////
	
	function owner_email_exists(){
		$email=$_POST['username'];
		$use=$this->user->get_owner1($email); 
		
		if($use<=0){
		
			echo json_encode(array("result" =>"success","message" =>"No Result Found"), 200);
			
		} else {
		
			echo json_encode(array("result" =>"","owner_info" =>$use), 204);
		}
	}
	
	function paymentsuccess()
	{
		@$lat= substr(constant("lat"),0,9);
		@$lon= substr(constant("lon"),0,9);
		$email=$_POST['username'];
		$ltype=$_POST['ltype'];
		$lice_type = $_POST['licence'];
		$username=$_POST['username'];
		$password=$_POST['passwordvalue'];
		$uname = $_POST['uname'];
		$role = $_POST['role'];
		if(!empty($email))
		{
			$use=$this->user->get_owner1($email); 
			if($use<=0)
			{
				$id=$this->user->add_session_user123($lat,$lon,$username,$ltype,$password,$lice_type,$uname,$role);
		
				$licence="koala".$id;
				$this->user->in_owner_lice($id,$username,$ltype);
				@$item_transaction =$_REQUEST['pay_id'];

				$uid=$this->user->updatetransation($id,$item_transaction);	
	 
	 
				if(!empty($id))
				{
				
					@$to = $username;
					@$to_name =$uname;
					@$subject = 'Koala owner login details.';//$_POST['subject'];
					@$body = "
						<html>
						<head>
						<title>HTML email</title>
						</head>
						<body>
						<table align='left' width='700' border='0'>
						<tr>
						<th align='left'> Name: </th>
						<td>".$uname."</td>
						</tr>
						<tr>
						<th align='left'> Subject: </th>
						<td>Koala owner login details.</td>
						</tr> 
						<tr>
						<th align='left'> User Name :</th>
						<td>".$username."</td>
						</tr>
						<tr>
						<th align='left'> Password :</th>
						<td>".$password."</td>
						</tr>
						<tr>
						<th align='left'> Licence  :</th>
						<td>".$licence."</td>
						</tr>
						<tr>
						<th align='left'>Owner Type</th>
						<td>".$lice_type."</td></tr>
						</table>
						</body>
						</html>
						";
		
					
					$res = $this->mail_function($to,$to_name,$subject,$body);
					
					if($res){
						echo json_encode(array("result" =>"success","message"=>"Owner Registered Successfully With Licence id","licence"=>$licence), 200);
					}
				}
		
			} else {
	  
					echo json_encode(array("result" =>"","message"=>"Email already exists"), 204);
	  
			}
		}
	
	}
	
	////////////////REg_Step-3 {Profissional}////////////
	function prof_email_exists(){
		$email=$_POST['username'];
		
		$ps="select * from professionals where email='$email'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pn=mysql_num_rows($pe);
		if($pn<=0){
		
			echo json_encode(array("result" =>"success","message" =>"No Result Found"), 200);
			
		} else {
		
			echo json_encode(array("result" =>"","owner_info" =>$pn), 204);
		}
	}
	
	
	function profestion_res_success()
	{ //role:role,ltype:ltype,username:username,passwordvalue:passwordvalue,uname:uname,surname:surname,age:age,gender:gender,address:address,city:city,zipcode:zipcode,state:state,lat:lat,lng:lng,qualification:qualification,admitted_to:admitted_to,from:from
		
		$lat= $_POST['lat'];//$_POST[''];
		$lng= $_POST['lng'];
		$role = $_POST['role'];
		$ltype = $_POST['ltype'];
		$username = $_POST['username'];
		$passwordvalue = $_POST['passwordvalue'];
		$uname = $_POST['uname'];
		$surname = $_POST['surname'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$zipcode = $_POST['zipcode'];
		$state = $_POST['state'];
		$qualification = $_POST['qualification'];
		$admitted_to = $_POST['admitted_to'];
		$from = $_POST['from'];
		$photo = $_POST['photo'];
		
		$trans_no = "";//$_POST['pay_id'];
		$pay_amount = 50;
		$pay_status = "Success";
		$pay_date = date("Y-m-d");

		$ps="select * from professionals where email='$username'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pn=mysql_num_rows($pe);
		if($pn<=0)
		{

		//echo json_encode(array("result" =>"","message"=>$_POST['photo']), 204); exit;
		$pid=$this->user->add_profetional($lat,$lng,$role,$ltype,$username,$passwordvalue,$uname,$surname,$age,$gender,$address,$city,$zipcode,$state,$qualification,$admitted_to,$from,$photo,$trans_no,$pay_amount,$pay_status,$pay_date);
		

		

		$item_transaction = mt_rand(5, 15);//$_REQUEST['txn_id'];

		$uid=$this->user->update_prof_transation($pid,$item_transaction);	
		
		
			$to = $username;
			$to_name = $uname;
			$subject = "Koala professional login details.";
		
		$body = "
		   <html>
		   <body>
		   <table align='left' width='700' border='0'>
		   <tr>
		   <th align='left'> Username: </th>
		   <td>".$username."</td>
		   </tr>
		   <tr>
		   <th align='left'> Password  </th>
		   <td>".$passwordvalue."</td>
		   </tr> 
		   </table>
		   </body>
		   </html>
		   ";
			
			$res = $this->mail_function($to,$to_name,$subject,$body);
			if($res) echo json_encode(array("result" =>"success","message"=>"Registration Successful"), 200);
		}  else {
			
			echo json_encode(array("result" =>"","message"=>"Email already exists"), 204);

		}
		

		
	}
	
	
	//////////////////Forget Password/////////////////
	
	
	function forgetpassword()
	{
		if($_POST)
		{
		  $role=$_POST['role']; //exit;
		  if($role=="Owner" || $role=="owner" )
		  {
			  $licence=$_POST['licence'];
			  $email=$_POST['username'];
			   
			  $os="select * from owner_table where owner_type='$licence' and owner_email='$email'";
			  $oe=mysql_query($os) or die(mysql_error());
			  $res = mysql_fetch_array($oe);
			  $to_name = $res['own_name'];
			  $on=mysql_num_rows($oe);
			  if($on>0)
			  {
				   $password=$_POST['passwordvalue'];
				   $pass=md5($password);
				   $us="update owner_table set own_passwd='$pass' where owner_email='$email'";
				   $ue=mysql_query($us) or die(mysql_error());
				   if($ue)
				   {
						$to = $email;
						
						$subject = "Koala owner forgot password details.";
		
					$body = "
				   <html>
				   <body>
				   <table align='left' width='700' border='0'>
				   <tr>
				   <th align='left'> Username: </th>
				   <td>".$email."</td>
				   </tr>
				   <tr>
				   <th align='left'> Password  </th>
				   <td>".$password."</td>
				   </tr> 
				   <tr>
				   <th align='left'> Owner type:  </th>
				   <td>".$role."</td>
				   </tr>
				   </table>
				   </body>
				   </html>
				   ";
					
					$res = $this->mail_function($to,$to_name,$subject,$body);

					   
					   
					   //if (!$mail->Send()) 
						//{
										//echo json_encode(array("result" =>"success","message"=>"Password Change Successfully"), 200);
									//$this->data['flash']= " Password Change Successfully ";
						//}
					}
					else
					{
						//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
						echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
					}
				}
				else
				{
					//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
					echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
				}
  
   
			}
  
			if($role=="professional")
			{
			   $email=$_POST['username'];
			   $ps="select * from professionals where email='$email'";
			   $pe=mysql_query($ps) or die(mysql_error());
			   $res = mysql_fetch_array($pe);
			   $to_name = $res['name'];
			   $pn=mysql_num_rows($pe);
			   if($pn>0)
			   {
	
				   $password=$_POST['passwordvalue'];
				   $pass=md5($password);
				   $ups="update professionals set password='$pass' where email='$email'";
				   $upe=mysql_query($ups);
				   if($upe)
				   {
					  $to = $email;
						
						$subject = "Koala professional forgot password details.";
		
					$body = "
				   <html>
				   <body>
				   <table align='left' width='700' border='0'>
				   <tr>
				   <th align='left'> Username: </th>
				   <td>".$email."</td>
				   </tr>
				   <tr>
				   <th align='left'> Password  </th>
				   <td>".$password."</td>
				   </tr> 
				 
				   </table>
				   </body>
				   </html>
				   ";
					
					$res = $this->mail_function($to,$to_name,$subject,$body);
					   
					   //if (!$mail->Send()) 
					  // {
									//$this->data['flash']= " Password Change Successfully ";
								//	echo json_encode(array("result" =>"success","message"=>"Password Change Successfully"), 200);
						//}
					}
					else
					{
						//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
						echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
					}
				}
				else
				{
					//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
					echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
				}
			}
  
			if($role=="caregiver")
			{
			   $email=$_POST['username'];
			   $ps="select * from caregiver where email='$email'";
			   $pe=mysql_query($ps) or die(mysql_error());
			   $res = mysql_fetch_array($pe);
			   $to_name = $res['name'];
			   $pn=mysql_num_rows($pe);
			   //echo $pn; exit;
			   if($pn>0)
			   {
					 $password=$_POST['passwordvalue'];
					$pass=md5($password);
					 $ups="update caregiver set passwd='$pass' where email='$email'"; 
					$upe=mysql_query($ups) or die(mysql_error());
					//echo $upe;
					if($upe)
					{
   
					  $to = $email;
						
						$subject = "Koala caregiver forgot password details.";
		
					$body = "
				   <html>
				   <body>
				   <table align='left' width='700' border='0'>
				   <tr>
				   <th align='left'> Username: </th>
				   <td>".$email."</td>
				   </tr>
				   <tr>
				   <th align='left'> Password  </th>
				   <td>".$password."</td>
				   </tr> 
				 
				   </table>
				   </body>
				   </html>
				   ";
					
					$res = $this->mail_function($to,$to_name,$subject,$body);
					    
					  // if (!$mail->Send()) 
					  // {
									//$this->data['flash']= " Password Change Successfully ";
								//	echo json_encode(array("result" =>"success","message"=>"Password Change Successfully"), 200);
							//	}
					   
					}
					else
					{
						//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
						echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
					}
				}
				else
				{
					//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
					echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
				}
			}
  
		}
	}
	
	
	/*After Owner Login*/
	
	public function after_owner_login()
	{
		
		if($_POST['listof']=="patients"){ 
			$patients=$this->user->getpatients($_POST['user_id']);
			
			echo json_encode(array("result" =>"success","patients_list"=>$patients), 200);
		}
		if($_POST['listof']=="caregivers"){
			$caregivers=$this->user->getcaregiver($_POST['user_id']);
			
			echo json_encode(array("result" =>"success","caregivers_list"=>$caregivers), 200);
		}
				
	}
	
	
	public function patient_view(){
	
		$patient_info= $this->user->patient_details($_POST['patient_id']);
		echo json_encode(array("result" =>"success","patient_info"=>$patient_info), 200);
	}
	
	public function caregivers_view(){
		//echo json_encode(array("result" =>$_POST['caregiver_id']), 200); exit;
		$caregiver_info= $this->user->care_details($_POST['caregiver_id']);
		echo json_encode(array("result" =>"success","caregiver_info"=>$caregiver_info), 200);
	}
	
	
	//Upload Image
	

	 
	 public function get_caregiver_patients(){
		 
		$caregiver_info= $this->user->caregiver_patients($_POST['user_id']);
		
		
		if(count($caregiver_info)<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
		
			echo json_encode(array("result" =>"success","patients_list" =>$caregiver_info), 200);
		}
		
		
		
	 }
	 
	 public function get_professionals(){
		
		  $professionals_info = $this->user->getProfessionals1($_POST['user_id']);
		 if($professionals_info){
		  echo json_encode(array("result" =>"success","professionals_info" =>$professionals_info), 200);
		 } else {
			  echo json_encode(array("result" =>"", 204));
		 }
	 }
	 
	 
	 
	  public function get_professionals_all(){
		 
			$professionals_info = $this->user->getProfessionals_all($_POST['user_id']);
		 
		 if(count($professionals_info)<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
		
			  echo json_encode(array("result" =>"success","professionals_info" =>$professionals_info), 200);
		}
		 
		 
		
	 }
	 
	 
	 
	 public function professional_view(){
	
		$professional_view_info= $this->user->getProfessionals_by_id($_POST['professional_id']);
		echo json_encode(array("result" =>"success","professional_info"=>$professional_view_info), 200);
	}
	
	
	
	
	 public function get_pro_patients(){
		 
		$pro_patients_info= $this->user->getPatients2($_POST['user_id']);
		if($pro_patients_info){
			echo json_encode(array("result" =>"success","patients_list" =>$pro_patients_info), 200);
		} else {
			echo json_encode(array("result" =>""), 204);
		}
	 }
	 
	  public function get_por_caregivers(){
		 
		$pro_caregivers_info= $this->user->getprofetionals123($_POST['user_id']);
		if($pro_caregivers_info){
		echo json_encode(array("result" =>"success","caregivers_list" =>$pro_caregivers_info), 200);
		} else {
			echo json_encode(array("result" =>""), 204);
		}
	 }
	
	
	//Owner patient update
	
	public function patient_update(){
	
		$patient_update = $this->user->updatepatient($_POST['patient_id']);
		echo json_encode(array("result" =>"success"), 200);
		
	}
	
	public function owner_patient_insert(){
		//
		$patn=$this->user->getpatient1($_POST['user_id'],$_POST['own_licence']);
		
			if($patn<=0)
			{
				$ms="select * from owner_licence where own_id=".$_POST['user_id']." and licence_no='".$_POST['own_licence']."'";
				$me=mysql_query($ms) or die(mysql_error());
				$mn=mysql_num_rows($me);
				if($mn>0)
				{
					$patient_id = $this->user->owner_insertpatient($_POST['user_id'],$_POST['own_licence']);
					echo json_encode(array("result" =>"success","patient_id"=>$patient_id ), 200);
				} else {
					echo json_encode(array("result" =>"", "message"=>"Licence does not exists."), 204);
				}
			} else {
				
				echo json_encode(array("result" =>"", "message"=>"Patient already exists with this licence."), 204);
			}
	}
	
	///////////Owner -Caregiver editing 
	
	public function caregiver_edit(){
		$caregiver_update = $this->user->updatecaregiversbycid($_POST['caregiver_id']);
		echo json_encode(array("result" =>"success"), 200);
		
	}
	

	//////Owner - Caregiver adding
	public function get_own_patients(){
		$get_own_patients = $this->user->get_patient_details($_POST['user_id']);
		echo json_encode(array("result" =>"success","get_patients"=>$get_own_patients), 200);
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////
	
	
	function addcaregivers()
	{
	   $email=$_POST['user_email'];
	   
	   
	   $id=$_POST['user_id'];
	  // $lice_type=$_POST['own_licence_type'];
	  
	  $lat=0;
	  $lon=0;
	 // echo json_encode(array("result" =>"success","get_patients"=>$_POST), 200); exit;
	 
	   if($_POST){
		 
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
			$mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
			$mail->Subject = "Koala caregiver login details."; //subject      

			$ar=$_POST['cname'];
			$snname=$_POST['sname'];
			$cemail=$_POST['cemail'];
			$ctype=$_POST['ctype'];
			@$patient=$_POST['patient'];
			@$owtype=$_POST['owtype'];

			$ct=count($ar);
		  	
			if(!empty($ar)  and !empty($cemail) and !empty($ctype) and !empty($patient))
			{ 
		
				$ctype;
				$patid=$patient;
				$this->data['patientlicence']=$this->user->get_patient_licence($patid);
				$liceid=$this->data['patientlicence'][0]->own_licence;
				@$this->data['plicencetype']=$this->user->get_patient_type($liceid);
				@$lice_type=$this->data['plicencetype'][0]->licencetype;
				@$pname=$this->data['plicencetype'][0]->name;
		
				$s1="select * from list_caregiver where care_type_id=1 and pat_id='$patid'";
				$e1=mysql_query($s1) or die(mysql_error());
				$n1=mysql_num_rows($e1);
				
				$s2="select * from list_caregiver where care_type_id!=1 and pat_id='$patid'";
				$e2=mysql_query($s2) or die(mysql_error());
				$n2=mysql_num_rows($e2);
	
		
				if($owtype=="yes")
				{
					$pus="update patients set owner_caretype='$ctype' where own_id='$id' and id='$patid'";
					$pue=mysql_query($pus) or die(mysql_error());
				}
		
		
		
		
				if($lice_type!="Extended" or empty($lice_type))
				{
						 $limit=5;
						$mlit=3;
				}
				if($lice_type=="Extended")
				{
						$limit=10;
						$mlit=8;
				}

				$mc="select * from list_caregiver where pat_id='$patid'";
				$me=mysql_query($mc) or die(mysql_error());
				$mn=mysql_num_rows($me);
		
				if($mn<$limit)
				{   
		 
					$mn=count($ctype);
					$cr="select * from caregiver where email='$cemail'";
					$ce=mysql_query($cr) or die(mysql_error());
					$mcr=mysql_fetch_array($ce);
					$cn=mysql_num_rows($ce);

					if($cn<=0)
					{
		
						if($n1<2 and $ctype==1)
						{ 
		
		
							$is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id,lat,lon) values('$ar','$snname','$cemail','$ctype','$patient','0','$id','$lat','$lon')";
		
							$ie=mysql_query($is) or die(mysql_error());
							$cid=mysql_insert_id();
		
							$mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$cid','$id','$ctype','$patient')";		
							$me=mysql_query($mc) or die(mysql_error());	
							
							if($me)
							{
								$body = "
								<html>
								<head>
								<title>HTML email</title>
								</head>
								<body>
								<table align='left' width='700' border='0'>
								<tr>
								<th align='left'>Name:</th>
								<td>$ar</td>
								</tr>
								<tr>
								<th align='left'>Email:</th>
								<td>$cemail</td>
								</tr>
								<tr>
								<th align='left'>To activate your account </th>
								<td> <a href='".EMAIL_URL.'auth/careregister/'.$cid."'> click here </a> </td>
								</tr>
								</table>
								</body>
								</html>
								";

								@$body = eregi_replace("[\]",'',$body);
								$mail->MsgHTML($body);
								$to = $cemail;

								$mail->AddAddress($to, $ar); 


								if ($mail->Send()) { 
									//$this->data['msg']="Caregiver Added Successfully ";
									echo json_encode(array("result" =>"success","message"=>"Caregiver Added Successfully","error_code"=>"4"), 200);
								} else { 
									// $this->data['msg']="Caregiver Added Successfully ";
									echo json_encode(array("result" =>"","message"=>"Mail Not Sent"), 204);
								} 
	
							}
						}
		
						if($n1==2 and $ctype==1)
						{
							//$this->data['msg']="You Have Entered More Than 2 Primary Caregivers With This Patient ";
							echo json_encode(array("result" =>"","message"=>"You Have Entered More Than 2 Primary Caregivers With This Patient"), 204);
						}
						$n2;
						if($n2<=$mlit and $ctype!=1)
						{ 
							$is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id,lat,lon) values('$ar','$snname','$cemail','$ctype','$patient','0','$id','$lat','$lon')";
							$ie=mysql_query($is) or die(mysql_error());
							$cid=mysql_insert_id();
		
							$mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$cid','$id','$ctype','$patient')";		
							$me=mysql_query($mc) or die(mysql_error());
		
							if($me)
							{

								$body = "
								<html>
								<head>
								<title>HTML email</title>
								</head>
								<body>
								<table align='left' width='700' border='0'>
								<tr>
								<th align='left'>Name:</th>
								<td>$ar</td>
								</tr>
								<tr>
								<th align='left'>Email:</th>
								<td>$cemail</td>
								</tr>
								<tr>
								<th align='left'>To activate your account </th>
								<td> <a href='".EMAIL_URL.'auth/careregister/'.$cid."'> click here </a> </td>
								</tr>
								</table>
								</body>
								</html>
								";

								@$body = eregi_replace("[\]",'',$body);
								$mail->MsgHTML($body);
								$to = $cemail;

								$mail->AddAddress($to, $ar); 


								if ($mail->Send()) { 
									echo json_encode(array("result" =>"success","message"=>"Caregiver Added Successfully","error_code"=>"4"), 200);
								} else { 
									echo json_encode(array("result" =>"","message"=>"Mail Not Sent","error_code"=>"1"), 204);	 
								} 

							}
						}
					}
		
					if($cn>0)
					{
		
						$careid=$mcr['id'];
						$mcemail=$mcr['email'];		
						$mcs="select * from list_caregiver where careid='$careid' and own_id='$id' and pat_id='$patient'";$mce=mysql_query($mcs) or die(mysql_error()); 		
						$mcn=mysql_num_rows($mce);
						
						if($mcn<=0) 		
						{	
							if($n1<2 and $ctype==1)
							{ 
		
		
								$mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$careid','$id','$ctype','$patient')";		
								$me=mysql_query($mc) or die(mysql_error());	
		
								if($me)
								{
		
									$body = "
									<html>
									<head>
									<title>HTML email</title>
									</head>
									<body>
									<table align='left' width='700' border='0'>
									<tr>
									<th align='left'>Name:</th>
									<td>$ar</td>
									</tr>
									<tr>

									</tr>
									<tr>
									<th align='left'> you are assigned as $ctype to patient - $patient </th>

									</tr>
									</table>
									</body>
									</html>
									";

									@$body = eregi_replace("[\]",'',$body);
									$mail->MsgHTML($body);
									$to = $mcemail;

									$mail->AddAddress($to, "Caregiver Notification"); 
									if ($mail->Send()) { 
										echo json_encode(array("result" =>"success","message"=>"Caregiver Added Successfully","error_code"=>"4"), 200);
									} else { 
										echo json_encode(array("result" =>"","message"=>"Mail Not Sent","error_code"=>"1"), 204);	 
									} 
								}
							}
		
							if($n1==2 and $ctype==1)
							{
								$msg="You Have Entered More Than 2 Primary Caregivers ";
								echo json_encode(array("result" =>"","message"=>$msg), 204);	 
							}
							$n2;
							if($n2<=$mlit and $ctype!=1)
							{ 
								$mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$careid','$id','$ctype','$patient')";		
								$me=mysql_query($mc) or die(mysql_error());
								$body = "
								<html>
								<head>
								<title>HTML email</title>
								</head>
								<body>
								<table align='left' width='700' border='0'>
								<tr>
								<th align='left'>Name:</th>
								<td>$ar</td>
								</tr>
								<tr>

								</tr>
								<tr>
								<th align='left'> you are assigned as $ctype to patient - $patient </th>

								</tr>
								</table>
								</body>
								</html>
								";

							   @$body = eregi_replace("[\]",'',$body);
							   $mail->MsgHTML($body);
							   $to = $mcemail;

								$mail->AddAddress($to, "Caregiver Notification"); 


								if ($mail->Send()) { 
									echo json_encode(array("result" =>"success","message"=>"Caregiver Added Successfully","error_code"=>"4"), 200);
								} else { 
									echo json_encode(array("result" =>"","message"=>"Mail Not Sent","error_code"=>"1"), 204);	 
								} 
							}
						} else {
		 
							//$this->data['msg']="Trying To Enter Same Caregiver And Patient Again ";
							echo json_encode(array("result" =>"","message"=>"Trying To Enter Same Caregiver And Patient Again ","error_code"=>"2"), 204);	 
						}
					}
				} else { 	
					//$this->data['msg']="<font color='red'>you have already added the $limit caregivers please take extended licence (or) anether licence to add more caregivers</font>";
					echo json_encode(array("result" =>"","message"=>"You have already added the ".$limit." caregivers please take extended licence (or) another licence to add more caregivers.","error_code"=>"3"), 204);	 
				}
			} else {
				//$this->data['msg']="<font color='red'>please fill the all input fields </font>";
				echo json_encode(array("result" =>"","message"=>"please fill the all input fields","error_code"=>"4"), 204);	 
			}
		     
		}
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////// 
	
	
	////Owner - Buy Licence
	
	
	function buy_anether_success()
	{
		$email=$_POST['user_email'];
	    $oid=$_POST['user_id'];
		
		$num=$this->user->get_owner_licence($oid);
		$licence=$this->user->insert_anether_olicence($oid,$email,$num);
		
		echo json_encode(array("result" =>"success","message"=>$licence), 200);
	}
	
	///Profissional to profissional send requests
	
	function sendfallowrequest()
	{
		$sid = $_POST['sid'];
		$rid = $_POST['rid'];
		
		$date=date("Y-m-d");
		$status=0;
		$ip=$_SERVER['REMOTE_ADDR'];
		$is="insert into professional_networks_prf (sid,rid,netstatus,sdate,netip) values('$sid','$rid','$status','$date','$ip')";
		$ie=mysql_query($is) or die(mysql_error());
		if($ie)
		{
			echo json_encode(array("result" =>"success","rid"=>$rid), 200);
		}
	}
	
	
	
	///Caregiver to profissional send requests
	
	function sendfallowrequest1()
	{
		$sid = $_POST['sid'];
		$rid = $_POST['rid'];
		$name = $_POST['user_disp_name'];
		$utype = $_POST['usertype'];
		$receivertype = $_POST['receivertype'];
		
		
		$date=date("Y-m-d");
		$status=0;
		$ip=$_SERVER['REMOTE_ADDR'];
		
		if($utype=="professional")
		{
		$utype=2;	
		
		$is="insert into professional_networks_prf (sid,rid,netstatus,sdate,netip) values('$sid','$rid','$status','$date','$ip')";
		
		}
		if($utype=="caregiver")
		{
		$utype=1;
		
		$is="insert into professional_networks (sid,rid,netstatus,sdate,netip) values('$sid','$rid','$status','$date','$ip')";
		
		}
		$loginusertye=$utype;
		
		//------
		
		$res="professional";
		
		//----------
		
		
		
		
		
		
		
		$ie=mysql_query($is) or die(mysql_error());
		$lid=mysql_insert_id();
		if($ie)
		{
				
				
				
				 $message=$name."  Had Send The Request "; 
				
				if($utype==2){
					
								
				$ntype="professionalnetwork";
				} else {
					$ntype="caregivernetwork";
				}
				
				$nip=$_SERVER['REMOTE_ADDR'];
				$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype) values('$message','$sid','$rid','$lid','0','$ntype','$nip','$res')";
				$nte=mysql_query($nts) or die(mysql_error());
				
				
				
				
				
			$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
			
			$ds="select * from device_tocken where userid='$rid' and user_type='$res'";
			$se=mysql_query($ds) or die(mysql_error());
			while($sr=mysql_fetch_array($se))
			{
			$device=$sr['tockenid'];
			$device_platform = $sr['platform'];
			$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						'sid'           =>$sid,
						'rid'           =>$rid,
						'sname'         =>$name,
						'recivertype'   =>$loginusertye,
						'lid' => $lid,
				'add_req_state'	=> 'newrequest',
						'call_type'     =>"add_request"
					);
					
					
				

				$res = $this->sendPushNotificationToGCM_addreq($device,$msg,$device_platform,$name,$sid,$rid,$loginusertye,$lid);

				//exit;
				/*$registrationIds = array($device);
					
					 $fields = array
					  (
					   'registration_ids'  => $registrationIds,
					   'data'              => $msg
					  );

					  $headers = array
					  (
					   'Authorization: key=' . $API_ACCESS_KEY1,
					   'Content-Type: application/json'
					  );

					  $ch = curl_init();
					  curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					  curl_setopt($ch,CURLOPT_POST, true );
					  curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					  $result = curl_exec($ch );
					  curl_close( $ch );
						$device = array();
					 echo $result;
					 */
			}
			
			
			
			
			
			//echo json_encode(array("result" =>"success","rid"=>$rid), 200);
		}
	}
	
	
	
	
	
	 function sendPushNotificationToGCM_addreq($deviceid,$msg1,$device_platform,$name,$from,$to,$loginusertye,$lid) {

		$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

			//echo $device_platform; exit;
		if($device_platform=="Android"){
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		echo json_encode(array("result" =>"success"), 200); //exit;
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
			$message = "You have a add request from '".$name."'";
			
			$path = base_url().'assets/KoalaProPushCert.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
			$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'sid'           =>$from,
				'rid'           =>$to,
				'sname'         =>$name,
				'recivertype'   =>$loginusertye,
				'lid' => $lid,
				'add_req_state'	=> 'noway',
				'call_type'     =>"add_request"
				
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				 echo json_encode(array("result" =>"success"), 200); exit;
		}
		
		//echo $result;
		echo json_encode(array("result" =>"success"), 200); 
    }
	
	
	
	function rejectrec(){
		 $sid = $_POST['sid'];
		  $rid = $_POST['rid'];
		  $lid = $_POST['lid'];
		  $receivertype = $_POST['receivertype'];
		  $name = $_POST['user_disp_name'];
		  
		  
		  if($receivertype==1) {
		   $us="delete from professional_networks  where id='$lid'";
		   } else {
			     $us="delete from professional_networks_prf  where id='$lid'";
			   
			   
		   }
		 
			   
			   
		   
		   $ue=mysql_query($us) or die(mysql_error());
		   $ms="update popup_notifications set status='1' where n_id='$lid'";
		   $me=mysql_query($ms) or die(mysql_error());
		   
		   if($receivertype==1) {
			   $res = "caregiver";
		   } else if($receivertype==2){
			    $res = "professional";
		   }
		   
		   $message = $name." rejected your add request";
		   
		   
		   if($receivertype==1) {
			
			$ntype="caregivernetwork";
			} else {
				$ntype="professionalnetwork";
			}
			$nip=$_SERVER['REMOTE_ADDR'];
		   
		   $nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype) values('$message','$rid','$sid','$lid','0','$ntype','$nip','$res')";
			$nte=mysql_query($nts) or die(mysql_error());
		  
		  
		  
		  $ds="select * from device_tocken where userid='$sid' and user_type='$res'";
			
				
			$se=mysql_query($ds) or die(mysql_error());
			while($sr=mysql_fetch_array($se))
			{
			$device=$sr['tockenid'];
			$device_platform = $sr['platform'];
			$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'no',
						'call_type'     =>"add_request"
						
			);
		  
		  
		   //$res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,0);
			
			
			$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
				if($device_platform=="Android"){
					
					$registrationIds = array($device);
					
					$fields = array
					(
						'registration_ids'  => $registrationIds,
						'data'              => $msg
					);
					
					$headers = array
					(
						'Authorization: key=' . $API_ACCESS_KEY1,
						'Content-Type: application/json'
					);
					
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt($ch,CURLOPT_POST, true );
					curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					
					
				} elseif($device_platform=="iOS"){
		 
					$deviceToken =$device;
					$passphrase = 'bravemount';
					
					$message = $name." accepted your add request";
					
			
					$path = base_url().'assets/KoalaProPushCert.pem';
			
					//echo $path; exit;
					$ctx = stream_context_create();
			
					$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
					stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
					stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
					stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
					// Open a connection to the APNS server
					$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

					//if (!$fp)
						//exit("Failed to connect: $err $errstr" . PHP_EOL);

					//echo 'Connected to APNS' . PHP_EOL;

					// Create the payload body
					$body['aps'] = array(
						'alert' => $message,
						'sound' => 'default',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'no',
						'call_type'     =>"add_request"
						
						
					);

					// Encode the payload as JSON
					$payload = json_encode($body);

					// Build the binary notification
						
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
					//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
						
						//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
						//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
						//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));

					//if (!$result)
					//	echo 'Message not delivered' . PHP_EOL;
					//else
					//	echo 'Message successfully delivered' . PHP_EOL;

					// Close the connection to the server
					fclose($fp);
			
				 
				}
			
			
				echo $result;
			
			
			
			
			}
	}
	
	
	function get_req_list(){
		$receivertype = $_POST['receivertype'];
		$user_id = $_POST['user_id'];
		
		if($receivertype==1){
			$query = $this->db->query("SELECT * FROM professional_networks  where rid='$user_id' and netstatus='0'");
			$count = $query->num_rows();
			$receivertype = "caregiver";
		} else {
			$query = $this->db->query("SELECT * FROM professional_networks_prf  where rid='$user_id' and netstatus='0'");
			$count = $query->num_rows();
			$receivertype = "professionals";
		}
		if($count>0){
			$result =  $query->result();
			echo json_encode(array("result" =>"success","get_req_list"=>$result,'receivertype'=>$receivertype), 200);
			
		} else {
			echo json_encode(array("result" =>"","message"=>"No requests"), 204);
		}
	
	}
	
	
	
	
	
	function acceptrec()
      {
		  
		
		  $sid = $_POST['sid'];
		  $rid = $_POST['rid'];
		  $lid = $_POST['lid'];
		  $receivertype = $_POST['receivertype'];
		  $name = $_POST['user_disp_name'];
		  
		 //  echo $receivertype; exit;
		   if($receivertype==1) {
		   $us="update professional_networks set netstatus='1',cread='0' where id='$lid'";
		   } else {
			    $us="update professional_networks_prf set netstatus='1',pread='0' where id='$lid'";
			   
		   }
		   $ue=mysql_query($us) or die(mysql_error());
		   $ms="update popup_notifications set status='1' where n_id='$lid'";
		   $me=mysql_query($ms) or die(mysql_error());
		   
		   if($receivertype==1) {
			   $res = "caregiver";
		   } else if($receivertype==2){
			    $res = "professional";
		   }
		   
		   $message = $name." accepted your add request";
		   
		    
			
			if($receivertype==1) {
			
			$ntype="caregivernetwork";
			} else {
				$ntype="professionalnetwork";
			}
			$nip=$_SERVER['REMOTE_ADDR'];
			$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype) values('$message','$rid','$sid','$lid','0','$ntype','$nip','$res')";
			$nte=mysql_query($nts) or die(mysql_error());
			
			
			 $ds="select * from device_tocken where userid='$sid' and user_type='$res'";
			
				
			$se=mysql_query($ds) or die(mysql_error());
			while($sr=mysql_fetch_array($se))
			{
			$device=$sr['tockenid'];
			$device_platform = $sr['platform'];
			$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
			);
			
		
		 // echo $device; exit;
		   
		  //$res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,1);
		  //echo json_encode(array("result" =>"success"), 200);
		// $res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,1);

				$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
				if($device_platform=="Android"){
					
					$registrationIds = array($device);
					
					$fields = array
					(
						'registration_ids'  => $registrationIds,
						'data'              => $msg
					);
					
					$headers = array
					(
						'Authorization: key=' . $API_ACCESS_KEY1,
						'Content-Type: application/json'
					);
					
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt($ch,CURLOPT_POST, true );
					curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					
					
				} elseif($device_platform=="iOS"){
		 
					$deviceToken =$device;
					$passphrase = 'bravemount';
					
					$message = $name." accepted your add request";
					
			
					$path = base_url().'assets/KoalaProPushCert.pem';
			
					//echo $path; exit;
					$ctx = stream_context_create();
			
					$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
					stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
					stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
					stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
					// Open a connection to the APNS server
					$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

					//if (!$fp)
						//exit("Failed to connect: $err $errstr" . PHP_EOL);

					//echo 'Connected to APNS' . PHP_EOL;

					// Create the payload body
					$body['aps'] = array(
						'alert' => $message,
						'sound' => 'default',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
						
					);

					// Encode the payload as JSON
					$payload = json_encode($body);

					// Build the binary notification
						
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
					//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
						
						//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
						//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
						//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));

					//if (!$result)
					//	echo 'Message not delivered' . PHP_EOL;
					//else
					//	echo 'Message successfully delivered' . PHP_EOL;

					// Close the connection to the server
					fclose($fp);
			
				 
				}
			
			
				echo $result;
			
			}
	
	  }
	
	
	
	
	
	function sendPushNotificationToGCM_addreq_succ($deviceid,$msg1,$device_platform,$name,$status) {

		$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		

			echo $status; exit;
		if($device_platform=="Android"){
			
			$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

		
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		//echo "hai"; exit;
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
			
			if($status==1){
				$message = $name." accepted your add request";
			} else {
				$message = $name." rejected your add request";
			}
			
			$path = base_url().'assets/KoalaProPushCert.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
			$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'result_status' => $status,	
'add_req_state'	=> 'noway',				
				'call_type'     =>"add_request"
				
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				 
		}
		
		echo $result;
		
    }
	
	
	
	
	
	/////////Caregiver profile editing
	
	function update_caregiver_profile(){
		
	
			$update = $this->user->updatecaregiversbycid();
			echo json_encode(array("result" =>"success"), 200);
		
	}
	
	function update_own_caregiver_profile(){

			$update = $this->user->updateowncaregiversbycid();
			echo json_encode(array("result" =>"success"), 200);
		
	}
	
	
	/////////////Get Professional Patients Routines
	
	public function patient_routine()
	{
		$pid = $_POST['patient_id'];
		$date = $_POST['now_date'];
		$user_type=  $_POST['user_type'];
		
		$patient_routines = $this->user->getnotification_new($pid,$date,$user_type);
		
	
		if(count($patient_routines)<=0 || !$patient_routines){
		
			echo json_encode(array("result" =>""), 204);
			
		} else {
			echo json_encode(array("result" =>"success","patient_routines" => $patient_routines), 200);
		}
	
	}
	
	
	/////////////Get Professional Patients parameter
	function showparameter()
	{
		$pid = $_POST['patient_id'];
		$eventid=1;
		$parameters = $this->user->fetch_parameter1($pid,$eventid);
		
			
	
	}
	  
	  /////////////Get Professional Patients Drugs
	public function show_drugs()
	{
		$pid = $_POST['patient_id'];
		$eventid=2;
	
		$medicene = $this->user->fetch_medecine($pid,$eventid);
		
	
	
		if($medicene<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			echo json_encode(array("result" =>"success","medicene" => $medicene), 200);
		}
	
	
	
	}
	
	public function show_diets()
	{
		  
		$pid = $_POST['patient_id'];
		$eventid=3;
		$show_diets = $_POST['show_diets'];
		
		
		
		$diets=$this->user->fetch_diets($pid,$eventid,$show_diets );
		
		if($diets<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			echo json_encode(array("result" =>"success","diets" => $diets), 200);
		}
	}
	
	
	public function show_activity()
	{
		$pid = $_POST['patient_id'];
		$eventid=4;
		
		 $activities = $this->user->fetch_activity($pid,$eventid);
		 
		 if($activities<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
		
			echo json_encode(array("result" =>"success","activities" => $activities), 200);
		}
	}
	
	
	function show_report()
	{
	
		$pid = $_POST['patient_id'];
		
		$reports = $this->user->fetch_report1($pid);
	
		
		if($reports<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
		
			echo json_encode(array("result" =>"success","reports" => $reports), 200);
		}
	}
	
	
	function show_report1()
	{
	
		$pid = $_POST['patient_id'];
		
		$from_date = $_POST['from_date'];
		
		$to_date = $_POST['to_date'];
		
		$reports = $this->user->fetch_report2($pid,$from_date,$to_date);
	
		
		if($reports<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
		
			echo json_encode(array("result" =>"success","reports" => $reports), 200);
		}
	}
	
	
	function mail_function($to,$to_name,$subject,$body){
	
		$to = $to;
		$to_name = $to_name;
		$subject= $subject;
		$body = $body;
		
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true; 
		$mail->SMTPSecure = "ssl"; 
		$mail->Port       = 465;
		$mail->Host       = "smtp.yandex.com";
		$mail->Username   = "notification@Koala.Care";
		$mail->Password   = "No1L@S@pp1@mo";
		$mail->SetFrom('notification@Koala.Care', 'Koala Team');
		$mail->Subject = $subject;     
		$body = $body;
		@$body = eregi_replace("[\]",'',$body);
				   $mail->MsgHTML($body);
		$to = $to;
		
		$mail->AddAddress($to, $to_name); 

		if ($mail->Send()) { 
			echo json_encode(array("result" =>"success","message"=>"Mail Sent Successfully"), 200);
			
		} else { 
			echo json_encode(array("result" =>"","message"=>"Mail Not Sent"), 200);
		} 
	
	}
	
	
	
	function view_report2()
	{ 
		$pid = $_POST['patient_id'];
		$date = $_POST['report_date'];
		
		$reports=$this->user->fetch_report_details($pid,$date);
		
		
		if($reports<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			echo json_encode(array("result" =>"success","reports" =>$reports), 200);
		}
	
	}
	
	function view_report_all()
	{ 
		$pid = $_POST['patient_id'];
		$date = $_POST['report_date'];
		$item = $_POST['item'];
		
		$reports=$this->user->fetch_report_details123($pid,$date,$item);
		
		
		if($reports<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			echo json_encode(array("result" =>"success","reports" =>$reports), 200);
		}
	
	}
	
	function getownet_ascare(){
		$pid =  $_POST['patient_id'];
		$id = $_POST['user_id'];
		$octype=$this->user->get_owner_caretype($pid,$id);
		
			echo json_encode(array("result" =>"success","octype" =>$octype), 200);
		
	}
	
	
	///Caregiver Add routines
	
	function add_caregiver_routines(){
		$pid =  $_POST['patient_id'];
		$eid =  $_POST['eid'];
		
		$get_res = $this->user->get_caregiver_routines($pid,$eid);
	
		echo json_encode(array("result" =>"success","details_list" =>$get_res ), 200);
	
	}
	
	
	
	function sendcaregiver_notification($deviceid,$msg1,$device_platform,$name,$from,$to,$loginusertye,$logintype,$rname,$ename,$mesg,$call_type,$ins,$acttype,$pid,$pname,$time,$t)
{ 

	$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

			//echo $device_platform; exit;
		if($device_platform=="Android"){
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
				
			//$message = $name.' has '.$txt.' the event '.strtolower($ename)." ";
			$message=$mesg;
			
			$path = base_url().'assets/KoalaProPushCert.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
			$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'sid'           =>$from,
				'rid'           =>$to,
				'sname'         =>$name,
				'message' => $message,
				'recivertype'   =>$loginusertye,
				'logintype'     =>$logintype,
				'chat'          =>"carenotification", 
			
				//'session_id'    =>$var,
				'call_type'     =>$call_type,
				'time'          =>$time,
				'date'          =>$t,
				'pid'           =>$pid,
				'pname'           =>$pname,
				'ins'           =>$ins,
				'event_type'    =>$acttype,
				'rname'         =>$rname
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				 
		}
		
		 $result;
	}
	
	
	
	
	function  insert_event()
	{
		
		
		$timezone = $_POST['timezone'];
		
		 date_default_timezone_set($timezone);
		
		$pname = $_POST['patient_name'];
		$offset = $_POST['offset_value'];
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$acttype=$_POST['event_type'];
		$actdetails=$_POST['details_id'];
	$careid="";
		$name="";
		
		$last_id_qry = "select note_id from  notifications order by note_id desc limit 0,1";
	$qry = mysql_query($last_id_qry)or die(mysql_error());
		$last_id_res = mysql_fetch_array($qry);
		
		$new_id = $last_id_res['note_id']+1;
		
		if($acttype==1)
		{
			$ps="select * from parameter_name where id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['pname'];
			$ename = "clenical perameters";
		}
		if($acttype==2)
		{
			
			$ps="select * from medicine where med_id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['med_name'];
			$ename = "drugs";
			
		}
		if($acttype==3)
		{
			
			$ps="select * from diets where id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['food_name'];
			$ename = "diets";
			
		}
		if($acttype==4)
		{
			
			$ps="select * from activities where act_id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['act_name'];
			$ename = "activities";
		}
		if($acttype==5)
		{
			$ps="select * from sub_event_type where sub_id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['sub_eventname'];
			$ename = "sleep/wake";
		}
	
	
	
		$time=$_POST['date_time'];
		$date = $_POST['now_date'];//date('Y-m-d');
		
		/*$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time','$date','','-1','0','$adel')";
		$exe=mysql_query($ins11) or die(mysql_error());
		$ins=mysql_insert_id();*/
		/////////////////////////////////////////////////////////////////////////
		if($_POST['frequency']=='week')
		{
			$mday=$_POST['days'];
			$count=count($_POST['days']);
			
			$mdate = $this->insert_event_week();  
 
			$mcount=count($mdate);
			for($j=0;$j<$count;$j++) 
			{
				@$f=$mday[$j];
				for($k=0;$k<$mcount;$k++)
				{  
					if($k==0)
					{
   
						$t=date("Y-m-d");
						
						$date1=$t." ".$_POST['date_time'].":00";
						$nwetime=strtotime($date1)*(1000);
						//$ex = $offset*60000;
						$new1 = $nwetime;
					}
					else
					{
					  $t=$mdate[$k]; 
						
						
					  
					}
					$time1=$time;
					$day = strtolower(date("w",strtotime($t)));
					//echo "if($day==$f)";
					if($f=="all")
					{
						//$msg="okokoko";
						
						$date1=$t." ".$_POST['date_time'].":00";
						$nwetime=strtotime($date1)*(1000);
						//$ex = $offset*60000;
						$new1 = $nwetime;
						
						$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name,gmt_time) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time1','$t','','-1','0','$adel','$new1')";
						$exe=mysql_query($ins11) or die(mysql_error());
						$ins=mysql_insert_id(); 
						unset($mday[1]);unset($mday[2]);unset($mday[3]);unset($mday[4]);unset($mday[5]);unset($mday[6]);unset($mday[7]);
					}
					//echo $day1=$day."<br>";
					if($f!="all")
					{
						if($day==$f) 
						{
							
							$date1=$t." ".$_POST['date_time'].":00";
							$nwetime=strtotime($date1)*(1000);
							//$ex = $offset*60000;
							$new1 = $nwetime;
							
							$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name,gmt_time) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time1','$t','','-1','0','$adel','$new1')";
							$exe=mysql_query($ins11) or die(mysql_error());
							$ins=mysql_insert_id(); 
							
							
							
							
							/*$ms="select * from list_caregiver as lcare, caregiver as care where lcare.careid=care.id and lcare.pat_id='$patient_id' and lcare.careid!='$caregiver_id'";
							$me=mysql_query($ms) or die(mysql_error());
							while($mr=mysql_fetch_array($me))
							{ 
							 

							 
							 @$careid=$mr['careid'];
							 $ms="select * from set_notifications where car_id='$careid'";
							 $me=mysql_query($ms);
							 $mr=mysql_fetch_array($me);
							 $withp=$mr['withp'];
							 if($withp=='no')
							 {
							 $cs="select * from caregiver where id='$careid'";
							 $ce=mysql_query($cs) or die(mysql_error());
							 $cr=mysql_fetch_array($ce);
							 $rname=$cr['name'];  
							 $stype="enotification";
							 $to=$careid;
							 $from=$caregiver_id;
							 $sname="";//$name;
							 $res="caregiver";
							 $loginusertye=$res;
							 $logintype=$res;
							 @$nid=$cr['note_id'];
							 if($acttype==1)
							 {
							  $ename="Clinical parameters";
							 }
								if($acttype==2)
							 {
							  $ename="Drugs";
							 }
							   if($acttype==3)
							 {
							  $ename="Diet";
							 }
							if($acttype==4)
							 {
							  $ename="Activities";
							 }
							if($acttype==5)
							 {
							  $acttype="Sleep / wake";
							 }
							 $ename=strtolower($ename);
							 //$message="$ename event is ".$mtxt." by ". $name. " for the patient ". $pname;
							 $message="new event for Patient $pname in $ename has been added";
							 @$rid=$mr['careid'];
							 //$mps="insert into popup_notifications (note_message,n_sid,n_rid,nm_id,note_type,n_ip,n_date) values('$message','$cid','$to','$nid','$stype','$ip','$date')";
							 //$mpe=mysql_query($mps) or die(mysql_error());
							  
							 $ds="select * from device_tocken where userid='$careid' and user_type='$res'";
							 $se=mysql_query($ds) or die(mysql_error());
							 $sn=mysql_num_rows($se);
							 if($sn>0)
							 {
							   
							 while($sr=mysql_fetch_array($se))
							 {  
							 
							  $device=$sr['tockenid'];
							  $device_platform = $sr['platform'];
							  
							  $msg = array(
								  'message'       => $message,
								  'title'         => $name.' has  event '.$ename,
								  'subtitle'      => 'update patient event',
								  'sid'           =>$from,
								  'rid'           =>$to,
								  'sname'         =>$name,
								  'recivertype'   =>$loginusertye,
								  'logintype'     =>$logintype,
								  'chat'          =>"newevent_added", 
								  'call_type'     =>"newevent_added",
								  'ins'           =>$ins,
								  'pname'         =>$pname,
								  'event_type'    =>$acttype,
								  'time'          =>$time,
								  'date'          =>$t,
								  'pid'          =>$patient_id,
								  'pname'          =>$pname,
								  'vibrate'        => 1,
								  'sound'         => 1
								 );  
							   
							  $call_type="newevent_added";
							   $ins=$ins;
							   $event_type=$ename;   
							   
							   
							 $res = $this->sendcaregiver_notification($device,$msg,$device_platform,$name,$from,$to,$loginusertye,$logintype,$rname,$ename,$message,$call_type,$ins,$acttype,$patient_id,$pname,$time,$t);
									
							  
							 }
							 
							 
							 }
							 

							 }
							  
							 
							 
							}*/
							
							
							
							
     
						}
					}
				}
				if(empty($msg))
				{
				  //echo "Please Select Correct Check Box";
				}
			}

		}
 
		if($_POST['frequency']=='month')
		{
		
			$mday=$_POST['days'];
			$count=count($_POST['days']);
			$month=date("m");
			$year=date("Y");
			$time1=$time;
     
			$ndays= $this->get_days_in_month($month, $year);
			$list = $this->insert_event_month($ndays);
  
			for($j=0;$j<$count;$j++) 
			{
				@$f=$mday[$j];
				$mcount=count($list);
 
				for($d=0; $d<=$mcount; $d++)
				{ 
     
					@$newdate=$list[$d]; 
					$time=mktime(12, 0, 0, date('m'), $d, date('Y'));
					
					if (date('m', $time)==date('m')) date('Y-m-d', $time);
					
					$t=date($newdate, $time);
					
					$date1=$newdate." ".$_POST['date_time'].":00";
					$nwetime=strtotime($date1)*(1000);
					//$ex = $offset*60000;
					$new1 = $nwetime;
					
					
					$day = strtolower(date("w",strtotime($t)));
 
					if(!empty($t))
					{
  
						if($f=="all")
						{

							$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name,gmt_time) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time1','$t','','-1','0','$adel','$new1')";
							$exe=mysql_query($ins11) or die(mysql_error());
							$ins=mysql_insert_id(); 
							unset($mday[1]);unset($mday[2]);unset($mday[3]);unset($mday[4]);unset($mday[5]);unset($mday[6]);unset($mday[7]);
						}
  
						if($f!="all")
						{
    
							if($day==$f)
							{
   
								
									
									
									
									
							
							$date1=$t." ".$_POST['date_time'].":00";
							$nwetime=strtotime($date1)*(1000);
							//$ex = $offset*60000;
							$new1 = $nwetime;
							
							$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name,gmt_time) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time1','$t','','-1','0','$adel','$new1')";
							$exe=mysql_query($ins11) or die(mysql_error());
							$ins=mysql_insert_id(); 
							
							
							
							
							/*$ms="select * from list_caregiver as lcare, caregiver as care where lcare.careid=care.id and lcare.pat_id='$patient_id' and lcare.careid!='$caregiver_id'";
							$me=mysql_query($ms) or die(mysql_error());
							while($mr=mysql_fetch_array($me))
							{ 
							 

							 
							 @$careid=$mr['careid'];
							 $ms="select * from set_notifications where car_id='$careid'";
							 $me=mysql_query($ms);
							 $mr=mysql_fetch_array($me);
							 $withp=$mr['withp'];
							 if($withp=='no')
							 {
							 $cs="select * from caregiver where id='$careid'";
							 $ce=mysql_query($cs) or die(mysql_error());
							 $cr=mysql_fetch_array($ce);
							 $rname=$cr['name'];  
							 $stype="enotification";
							 $to=$careid;
							 $from=$caregiver_id;
							 $sname="";//$name;
							 $res="caregiver";
							 $loginusertye=$res;
							 $logintype=$res;
							 @$nid=$cr['note_id'];
							 if($acttype==1)
							 {
							  $ename="Clinical parameters";
							 }
								if($acttype==2)
							 {
							  $ename="Drugs";
							 }
							   if($acttype==3)
							 {
							  $ename="Diet";
							 }
							if($acttype==4)
							 {
							  $ename="Activities";
							 }
							if($acttype==5)
							 {
							  $acttype="Sleep / wake";
							 }
							 $ename=strtolower($ename);
							 //$message="$ename event is ".$mtxt." by ". $name. " for the patient ". $pname;
							 $message="new event for Patient $pname in $ename has been added";
							 $rid=$mr['careid'];
							 //$mps="insert into popup_notifications (note_message,n_sid,n_rid,nm_id,note_type,n_ip,n_date) values('$message','$cid','$to','$nid','$stype','$ip','$date')";
							 //$mpe=mysql_query($mps) or die(mysql_error());
							  
							 $ds="select * from device_tocken where userid='$careid' and user_type='$res'";
							 $se=mysql_query($ds) or die(mysql_error());
							 $sn=mysql_num_rows($se);
							 if($sn>0)
							 {
							   
							 while($sr=mysql_fetch_array($se))
							 {  
							 
							  $device=$sr['tockenid'];
							  $device_platform = $sr['platform'];
							  
							  $msg = array(
								  'message'       => $message,
								  'title'         => $name.' has  event '.$ename,
								  'subtitle'      => 'update patient event',
								  'sid'           =>$from,
								  'rid'           =>$to,
								  'sname'         =>$name,
								  'recivertype'   =>$loginusertye,
								  'logintype'     =>$logintype,
								  'chat'          =>"newevent_added", 
								  'call_type'     =>"newevent_added",
								  'ins'           =>$ins,
								  'pname'         =>$pname,
								  'event_type'    =>$ename,
								  'time'          =>$time,
								  'date'          =>$t,
								  'pid'          =>$pid,
								  'pname'          =>$pname,
								  'vibrate'        => 1,
								  'sound'         => 1
								 );  
							   
							  $call_type="newevent_added";
							   $ins=$ins;
							   $event_type=$ename;   
							   
							   
							 $res = $this->sendcaregiver_notification($device,$msg,$device_platform,$name,$from,$to,$loginusertye,$logintype,$rname,$ename,$message,$call_type,$ins,$acttype,$pid,$pname,$time,$t);
									
							  
							 }
							 
							 
							 }
							 

							 }
							  
							 
							 
							}*/
							
							
							
							
									
							}
   
						}
   
					} 
   
					if(empty($msg)) 
					{
						//echo $msg="Please Select The Check Box"; 
					}
				}
       
			}
		}
 
		if($_POST['frequency']=='forever')
		{
			$mday=$_POST['days'];
			$strDateFrom=$_POST['startdate'];
			$strDateTo=$_POST['todate'];
  
			$list=$this->getAllDatesBetweenTwoDates($strDateFrom,$strDateTo);
 
			$mcount=count($list);
			$count=count($_POST['days']);
 
			for($j=0;$j<$count;$j++) 
			{   
				@$f=$mday[$j];
				
				for($d=0;$d<$mcount;$d++)
				{  
					$newdate=$list[$d]; 
					$time=mktime(12, 0, 0, date('m'), $d, date('Y'));
					if (date('m', $time)==date('m')) date('Y-m-d', $time);
      
					$t=date($newdate, $time);
					$day = strtolower(date("w",strtotime($t)));
 
					if(!empty($t))
					{
						$time1=$_POST['date_time'];
						
						if($f=="all")
						{
							
								$date1=$newdate." ".$_POST['date_time'].":00";
								$nwetime=strtotime($date1)*(1000);
								$ex = $offset*60000;
								$new1 = $nwetime + $ex;
							
							
							$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name,gmt_time) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time1','$t','','-1','0','$adel','$new1')";
							$exe=mysql_query($ins11) or die(mysql_error());
							$ins=mysql_insert_id();
							
							unset($mday[1]);unset($mday[2]);unset($mday[3]);unset($mday[4]);unset($mday[5]);unset($mday[6]);unset($mday[7]);
						}
						
						if($f!="all")
						{
							if($f==$day)
							{
								
								$date1=$newdate." ".$_POST['date_time'].":00";
								$nwetime=strtotime($date1)*(1000);
								$ex = $offset*60000;
								$new1 = $nwetime + $ex;
  
								$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name,gmt_time) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time1','$t','','-1','0','$adel','$new1')";
								$exe=mysql_query($ins11) or die(mysql_error());
								$ins=mysql_insert_id();
   
   
									
							
							$date1=$t." ".$_POST['date_time'].":00";
							$nwetime=strtotime($date1)*(1000);
							$ex = $offset*60000;
							$new1 = $nwetime + $ex;
							
							$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name,gmt_time) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time1','$t','','-1','0','$adel','$new1')";
							$exe=mysql_query($ins11) or die(mysql_error());
							$ins=mysql_insert_id(); 
							
							
							
							
							/*$ms="select * from list_caregiver as lcare, caregiver as care where lcare.careid=care.id and lcare.pat_id='$patient_id' and lcare.careid!='$caregiver_id'";
							$me=mysql_query($ms) or die(mysql_error());
							while($mr=mysql_fetch_array($me))
							{ 
							 

							 
							 @$careid=$mr['careid'];
							 $ms="select * from set_notifications where car_id='$careid'";
							 $me=mysql_query($ms);
							 $mr=mysql_fetch_array($me);
							 $withp=$mr['withp'];
							 if($withp=='no')
							 {
							 $cs="select * from caregiver where id='$careid'";
							 $ce=mysql_query($cs) or die(mysql_error());
							 $cr=mysql_fetch_array($ce);
							 $rname=$cr['name'];  
							 $stype="enotification";
							 $to=$careid;
							 $from=$caregiver_id;
							 $sname="";//$name;
							 $res="caregiver";
							 $loginusertye=$res;
							 $logintype=$res;
							 @$nid=$cr['note_id'];
							 if($acttype==1)
							 {
							  $ename="Clinical parameters";
							 }
								if($acttype==2)
							 {
							  $ename="Drugs";
							 }
							   if($acttype==3)
							 {
							  $ename="Diet";
							 }
							if($acttype==4)
							 {
							  $ename="Activities";
							 }
							if($acttype==5)
							 {
							  $acttype="Sleep / wake";
							 }
							 $ename=strtolower($ename);
							 //$message="$ename event is ".$mtxt." by ". $name. " for the patient ". $pname;
							 $message="new event for Patient $pname in $ename has been added";
							 $rid=$mr['careid'];
							 //$mps="insert into popup_notifications (note_message,n_sid,n_rid,nm_id,note_type,n_ip,n_date) values('$message','$cid','$to','$nid','$stype','$ip','$date')";
							 //$mpe=mysql_query($mps) or die(mysql_error());
							  
							 $ds="select * from device_tocken where userid='$careid' and user_type='$res'";
							 $se=mysql_query($ds) or die(mysql_error());
							 $sn=mysql_num_rows($se);
							 if($sn>0)
							 {
							   
							 while($sr=mysql_fetch_array($se))
							 {  
							 
							  $device=$sr['tockenid'];
							  $device_platform = $sr['platform'];
							  
							  $msg = array(
								  'message'       => $message,
								  'title'         => $name.' has  event '.$ename,
								  'subtitle'      => 'update patient event',
								  'sid'           =>$from,
								  'rid'           =>$to,
								  'sname'         =>$name,
								  'recivertype'   =>$loginusertye,
								  'logintype'     =>$logintype,
								  'chat'          =>"newevent_added", 
								  'call_type'     =>"newevent_added",
								  'ins'           =>$ins,
								  'pname'         =>$pname,
								  'event_type'    =>$ename,
								  'time'          =>$time,
								  'date'          =>$t,
								  'pid'          =>$pid,
								  'pname'          =>$pname,
								  'vibrate'        => 1,
								  'sound'         => 1
								 );  
							   
							  $call_type="newevent_added";
							  
							   $event_type=$ename;   
							   
							   
							 $res = $this->sendcaregiver_notification($device,$msg,$device_platform,$name,$from,$to,$loginusertye,$logintype,$rname,$ename,$message,$call_type,$ins,$acttype,$pid,$pname,$time,$t);
									
							  
							 }
							 
							 
							 }
							 

							 }
							  
							 
							 
							}*/
							
							
							
							
   
   
							}
						}
					}
				}
 
			}
  
		}
		/////////////////////////////////////////////////////////////////////////
		$pid=$patient_id;
		$cid=$caregiver_id;
		$res="caregiver";
		$message="new event for Patient $pname in $ename has been added";
		$ms="select * from list_caregiver as lcare, caregiver as care where lcare.careid=care.id and lcare.pat_id='$pid' and lcare.careid!='$cid'";
	$me=mysql_query($ms) or die(mysql_error());
	$mstatus="";
	while($mr=mysql_fetch_array($me))
	{ 
		$rid=$mr['careid'];
		$ms1="select * from set_notifications where car_id='$rid'";
		$me1=mysql_query($ms1);
		$mr1=mysql_fetch_array($me1);
		$withp=$mr1['withp'];
		
		 if($acttype==1)
		{
			$mstatus=$mr1['parameter_status'];
		}
		if($acttype==2)
		{
			$mstatus=$mr1['medical_status'];
		}
		if($acttype==3)
		{
			$mstatus=$mr1['diets_status'];
		}
		if($acttype==4)
		{
			$mstatus=$mr1['activity_status'];
		}
		
		
		
		
		
		
		
		if($withp=='no' && $mstatus==1)
		{
		$ntype="newevent_added";
		$nip=$_SERVER['REMOTE_ADDR'];
		$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,ltype) values('$message','$cid','$rid','$new_id','0','$ntype','$res')";
		$nte=mysql_query($nts) or die(mysql_error());
    }
	
	
}
		$mstatus="";
		$ms="select * from list_caregiver as lcare, caregiver as care where lcare.careid=care.id and lcare.pat_id='$patient_id' and lcare.careid!='$caregiver_id'";
							$me=mysql_query($ms) or die(mysql_error());
							while($mr=mysql_fetch_array($me))
							{ 
							 

							 
							 @$careid=$mr['careid'];
							 $ms="select * from set_notifications where car_id='$careid'";
							 $me=mysql_query($ms);
							 $mr=mysql_fetch_array($me);
							 $withp=$mr['withp'];
							  if($acttype==1)
							 {
							  $ename="Clinical parameters";
							  $mstatus=$mr['parameter_status'];
							 }
								if($acttype==2)
							 {
							  $ename="Drugs";
							  $mstatus=$mr['medical_status'];
							 }
							   if($acttype==3)
							 {
							  $ename="Diet";
							  $mstatus=$mr['diets_status'];
							 }
							if($acttype==4)
							 {
							  $ename="Activities";
							  $mstatus=$mr['activity_status'];
							 }
							if($acttype==5)
							 {
							  $acttype="Sleep / wake";
							 }
							 
							 
							 
							 
							if($withp=='no' && $mstatus==1)
							 {
							 $cs="select * from caregiver where id='$careid'";
							 $ce=mysql_query($cs) or die(mysql_error());
							 $cr=mysql_fetch_array($ce);
							 $rname=$cr['name'];  
							 $stype="enotification";
							 $to=$careid;
							 $from=$caregiver_id;
							 $sname="";//$name;
							 $res="caregiver";
							 $loginusertye=$res;
							 $logintype=$res;
							 @$nid=$cr['note_id'];
							
							 $ename=strtolower($ename);
							 //$message="$ename event is ".$mtxt." by ". $name. " for the patient ". $pname;
							 $message="new event for Patient $pname in $ename has been added";
							 @$rid=$mr['careid'];
							 //$mps="insert into popup_notifications (note_message,n_sid,n_rid,nm_id,note_type,n_ip,n_date) values('$message','$cid','$to','$nid','$stype','$ip','$date')";
							 //$mpe=mysql_query($mps) or die(mysql_error());
							  
							 $ds="select * from device_tocken where userid='$careid' and user_type='$res'";
							 $se=mysql_query($ds) or die(mysql_error());
							 $sn=mysql_num_rows($se);
							 if($sn>0)
							 {
							   
							 while($sr=mysql_fetch_array($se))
							 {  
							 
							  $device=$sr['tockenid'];
							  $device_platform = $sr['platform'];
							  
							  $msg = array(
								  'message'       => $message,
								  'title'         => $name.' has  event '.$ename,
								  'subtitle'      => 'update patient event',
								  'sid'           =>$from,
								  'rid'           =>$to,
								  'sname'         =>$name,
								  'recivertype'   =>$loginusertye,
								  'logintype'     =>$logintype,
								  'chat'          =>"newevent_added", 
								  'call_type'     =>"newevent_added",
								  'ins'           =>$new_id,
								  'pname'         =>$pname,
								  'event_type'    =>$acttype,
								  'time'          =>$time,
								  'date'          =>$t,
								  'pid'          =>$patient_id,
								  'pname'          =>$pname,
								  'vibrate'        => 1,
								  'sound'         => 1
								 );  
							   
							  $call_type="newevent_added";
							   @$ins=$ins;
							   $event_type=$ename;   
							   
							   
							 $res = $this->sendcaregiver_notification($device,$msg,$device_platform,$name,$from,$to,$loginusertye,$logintype,$rname,$ename,$message,$call_type,$ins,$acttype,$patient_id,$pname,$time,$t);
									
							  
							 }
							 
							 
							 }
							 

							 }
							  
							 
							 
							}
		
		if(isset($ins))
		{
			echo json_encode(array("result" =>"success","message" =>"Event set successfully","ins" =>$new_id), 200);
		} else {
			echo json_encode(array("result" =>"success","message" =>"Problem occured"), 200);
		}
	
	}

	//Caregiver viwe routines
	function view_notification()
	{
		$id = $_POST['note_id'];
		@$user_id = $_POST['user_id'];
		$get_res = $this->user->view_notification($id);
		
		$us="update popup_notifications set status='1'  where n_id=".(int)$id;
		//$ue=mysql_query($us) or die(mysql_error());
		
		
		echo json_encode(array("result" =>"success","notification_info" =>$get_res), 200);
	}
	
	
		function view_notification_locally()
	{
		$id = $_POST['nm_id'];
		$note_id = $_POST['note_id'];
		$user_id = $_POST['user_id'];
		$get_res = $this->user->view_notification($id);
		
		$us="update popup_notifications set status='1'  where n_id=".(int)$note_id;
		$ue=mysql_query($us) or die(mysql_error());
		
		
		echo json_encode(array("result" =>"success","notification_info" =>$get_res), 200);
	}
	
	
	function edit_notification()
	{
		//echo json_encode(array("result" =>"success","message" =>$_POST), 200); exit;
		$id = $_POST['note_id'];
		$cid1=$_POST['user_id'];
		
		 $red_val = $this->user->status_notification($id,$cid1);
		//exit;
		
		if($red_val>0){
		
			$ue = $this->user->edit_notification($id);
		
			echo json_encode(array("result" =>"success"), 200);
		
			
		} else {
			echo json_encode(array("result" =>"","message" =>"Status already updated."), 204);
		}
		
		
		
	}
	
	///Caregiver Insert Activity
	
	function insert_activity()
	{
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$actdetails=$_POST['actdetails'];
		$acttype=4;
		$actname=$_POST['actname'];
		
		$ins=$this->user->insert_activity($patient_id,$caregiver_id,$actdetails,$acttype,$actname);
		
		
		
		
			echo json_encode(array("result" =>"success"), 200);
		
	//echo json_encode(array("result" =>"success"), 200);
	}
	
	//Caregiver viwe Activity
	function view_activity()
	{
		$id = $_POST['act_id'];
		$get_res = $this->user->view_activity($id);
		echo json_encode(array("result" =>"success","activity_info" =>$get_res), 200);
	}
	
	
	//update activity
	
	
	function edit_activity()
	{
		$act_id = $_POST["act_id"];
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$actdetails=$_POST['actdetails'];
		$acttype=4;
		$actname=$_POST['actname'];
		
		$ins=$this->user->edit_activity($act_id,$patient_id,$caregiver_id,$actdetails,$acttype,$actname);
		
	
			echo json_encode(array("result" =>"success"), 200);
	
	}
	
	///Caregiver medicene
	function insert_medicine()
	{
		$med_id=$_POST["med_id"];
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$med_dose=$_POST['med_dose'];
		$medname=$_POST['med_name'];
		$med_type=$_POST['med_type'];
		$med_note=$_POST['med_note'];
		$med_qua=$_POST['med_qua'];
		$pre=$_POST['pre_date'];
		$exp=$_POST['exp_date'];
		$med_price=$_POST['med_price'];	
		@$meal_type=$_POST['meal_type'];
		$checkboxFourInput = $_POST['checkboxFourInput'];
		$checkboxFourInput1 = $_POST['checkboxFourInput1'];
		$checkboxFourInput2 = $_POST['checkboxFourInput2'];
		
		
		$mn=$this->user->get_medlist($medname,$patient_id,$med_id);
		
		if($mn<=0)
		{
			$ins=$this->user->insert_medicine($med_id,$patient_id,$caregiver_id,$medname,$med_type,$med_note,$med_qua,$pre,$exp,$med_price,$meal_type,$med_dose,$checkboxFourInput,$checkboxFourInput1,$checkboxFourInput2);
		
			if($ins)	
			{
				if($med_id) { $data="Drug has been updated !";   } else { $data="Drug has been added !"; }
				echo json_encode(array("result" =>"success","message"=>$data), 200);
			}
			else
			{
				$data="Unsuccesfull, Try again !";
				echo json_encode(array("result" =>"","message"=>$data), 204);
			}
		}
		else
		{
			$data="You Have Entered Same Drug Details Again!";
			echo json_encode(array("result" =>"","message"=>$data), 204);
		}
	}
	
	
	///View medecin
	function view_medicine()
	{
		$id = $_POST['med_id'];
		$medicine = $this->user->get_medicine($id);
		echo json_encode(array("result" =>"success","medicine_info"=>$medicine), 200);
	
	}
	
	///Caregiver Add Diet
	
	
	function add_diet()
	{
		$diet_id = $_POST['diet_id'];
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$foodname=$_POST['foodname'];
		$foodtype=$_POST['foodtype'];
		$fooddel=$_POST['fooddel'];
		$dateofweek="";//$_POST['dateofweek'];
		$foodlike=$_POST['foodlike'];
		$nfile = "";
	
		
		if($diet_id){
		$fs="select * from diets where food_name='$foodname' and id<>'$diet_id'";
		} else {
		$fs="select * from diets where food_name='$foodname'";
		}
		$fe=mysql_query($fs) or die(mysql_error());
		$fn=mysql_num_rows($fe);
		
			
		if($fn<=0)
		{
			
			if($diet_id){
			
				$ins=$this->user->update_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$nfile,$foodlike,$diet_id);
				
				$flikeid1=$diet_id;
				$foodlike=$_POST['foodlike'];
				$fs="select * from foodlike where patid='$patient_id' and foodid='$diet_id'";
				$fe=mysql_query($fs) or die(mysql_error());
				$fn=mysql_num_rows($fe);
				//echo json_encode(array("result" =>"success","message"=>$foodlike), 200);exit;
				if($fn>0)
				{
				$is="UPDATE foodlike set foodlike='$foodlike' where foodid='$flikeid1'";
				$ie=mysql_query($is) or die(mysql_error());
				}
				if($fn<=0)
				{
				$is="insert into  foodlike (foodid,patid,foodlike) values ('$diet_id','$patient_id','$foodlike') ";
				$ie=mysql_query($is) or die(mysql_error());
					
				}
			
			} else {
			
				$ins=$this->user->insert_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$nfile,$foodlike);

				$is="insert into foodlike(foodid,patid,foodlike) values ('$ins','$patient_id','$foodlike')";
				$ie=mysql_query($is) or die(mysql_error());

			}
			
			if($ie)
			{
				if($diet_id){ $data="Diets has been Updated !"; } else {$data="Diets has been added !"; }
				echo json_encode(array("result" =>"success","message"=>$data), 200);
				
			}
		}
		else
		{
		
			$data="Trying To Add Same Food Name please try with anether name";
			echo json_encode(array("result" =>"","message"=>$data), 204);
		}
	}


	///View diets
	function view_diets()
	{
		$id = $_POST['diet_id'];
		$diets = $this->user->get_diets($id);
		echo json_encode(array("result" =>"success","diets_info"=>$diets), 200);
	
	}


	//Add parameter
	
	function add_parameters_care()
	{ 
		//$numclu=$_POST['measure_names'];
		//$numclu=1;
		//$k = $numclu+$numclu;
		//echo json_encode(array('result'=>'sucess','data'=>$_POST));
		
		//exit;
		  
			$param_id= $_POST['param_id'];
			$pname=$_POST['pname'];
			$numclu=$_POST['measure_names'];
			$date=date("Y-m-d");
			$patient_id=$_POST['patient_id'];
			
			if($param_id){ 
			$ps="select * from parameter_name where pname='$pname' and id<>'$param_id'";
			} else { 
			$ps="select * from parameter_name where pname='$pname'";
			}
			
			//echo json_encode(array("result" =>"success","diets_info"=>$_POST['arr'][]), 200); exit;
			
			$pe=mysql_query($ps) or die(mysql_error());
			$pn=mysql_num_rows($pe);
		
			if($pn<=0)
			{
				$id=$this->user->addparameters($param_id,$pname,$numclu,$date,$patient_id);
			
				
				$num=$numclu;
				if($param_id){ 
					$query = $this->db->query("SELECT * FROM parameter_columns where para_id='$param_id'");
					$res = $query->num_rows();
					$k=$res+1;
				} else { $k=1; }
				//	echo json_encode(array("result" =>"success","diets_info"=>$k), 200); exit;
				for($i=$k;$i<$num+1;$i++)
				{
				$name=$_POST['arr'][$i];
				$this->user->add_paracolumns($param_id,$id,$name);
				
				}	
					
				if($param_id){ 
					echo json_encode(array("result" =>"success","message"=>"Parameters Has Been Update !"), 200);
				} else {
					echo json_encode(array("result" =>"success","message"=>"Parameters Has Been Added !"), 200);
				}
			}
			else
			{
				//$this->data['flash']=" You Are Trying To Enter Same  Parameter Name Again !";
				echo json_encode(array("result" =>"","message"=>" You Are Trying To Enter Same  Parameter Name Again !"), 204);
			}
				
	 
		
	}
	
	
	//view parameter
	
	function view_perameter(){
			$id = $_POST['param_id'];
			$parameter_info = $this->user->view_perameter($id);
			
			echo json_encode(array("result" =>"success","parameter_info"=>$parameter_info ), 200);
	}
	
	
	//Caregiver report mail send
	function send_report_mail()
	{ 
		$pid = $_POST['patient_id'];
		$date = $_POST['report_date'];
		$patient_name = $_POST['patient_name'];
		$reports=$this->user->fetch_report_details($pid,$date);
		
		
		if($reports<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			//echo json_encode(array("result" =>"success","reports" =>$report->email), 200); exit;
			
			$to = $_POST['user_email'];//"satyanarayana.p@bravemount.com";//"kalyankumarp@gmail.com";//$_POST['user_email']; // "mahamoodphp@gmail.com";//
			$to_name = $_POST['user_disp_name'];
			
			$subject = "Koala report of patient - ".$patient_name;
			
			
			
			
			
			$body = "<div>";
			foreach($reports as $report){
			
			if($report->event_type_id=="1") {  
					$event_name = "Clinical parameters";
			} else if($report->event_type_id=="2") {  
				$event_name = "Drugs";
			}else if($report->event_type_id=="3") {  
				$event_name = "Diet";
			}else if($report->event_type_id=="4") {  
				$event_name = "Activities";
			}else if($report->event_type_id=="5") {  
				$event_name = "Sleep / wake";
			}
			
			if($report->status=="1") { $event_status = "Done"; } else { $event_status = "Not Done"; }
			
			$body.="<div style='border:1px solid black; width:600px; margin:0 auto;' ><p style='margin-left:5px;'>Event Name: ".$event_name."</p>";
			
			if($event_name=="Clinical parameters"){
				$sub_id = $report->sub_event_id;
				$select = "select * from parameters where para_id='$sub_id'";
				$qry = mysql_query($select)or die(mysql_error());
				$body.="<p style='margin-left:5px;'>Details: </p>";
				while($res = mysql_fetch_array($qry)){
					if($res['option1']<>NULL || $res['option1']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option1']."</p>";
					}
					if($res['option2']<>NULL || $res['option2']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option2']."</p>";
					}
					if($res['option3']<>NULL || $res['option3']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option3']."</p>";
					}
					if($res['option4']<>NULL || $res['option4']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option1']."</p>";
					}
					if($res['option5']<>NULL || $res['option5']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option1']."</p>";
					}
				}
			} else {
				$body.="<p style='margin-left:5px;'>Details: ".$report->sub_event_name."</p>";
			}
			
			$body.="<p style='margin-left:5px;'>Time: ".$report->note_time."</p>";
			$body.="<p style='margin-left:5px;'>Event Status: ".$event_status."</p></div>";
			$body.="<p style='margin-left:5px;'></p>";
			
			}
			$body.= "</div>";
						
			//echo json_encode(array("result" =>"success","reports" =>$body), 200); exit;
			$res = $this->mail_function($to,$to_name,$subject,$body);
		}
	
	}
	
	
	////////////////////////Internal Notifications
	
	function internal_notification(){
	
		
		 $cs="select * from notifications where  note_date='$date'  and care_id='$mcid' and status=0 order by note_id desc";
		$ce=mysql_query($cs) or die(mysql_error());
		$cn=mysql_num_rows($ce);
		echo json_encode(array("result" =>"success","reports" =>$cn), 200); exit;
		//while($cr=mysql_fetch_array($ce))
		//{
			//$ntime=substr($cr['note_time'],0,5);
		
		//}
		$date = date("Y-m-d");
		$timeFirst  = strtotime($date.' '.$_POST['timeFirst']);
		$timeSecond = strtotime($date.' '.$_POST['timeSecond']);
		
		
		
		
		$differenceInSeconds = $timeSecond - $timeFirst;//heigh-low
		
		echo json_encode(array("differenceInSeconds" =>$differenceInSeconds), 200);
		
	}
	
	
	
	function get_text_session(){
		$rid=$_POST['rid'];
		$userid=$_POST['user_id'];
		$receivertype=$_POST['receivertype'];
		$utype = $_POST['usertype'];
	
	
		if($utype=="professional")
		 {
		 $utype=2; 
		 }
		 if($utype=="caregiver")
		 {
		 $utype=1;
		 }
	
	
		$ch="select * from chat where (chat.from='$userid' and chat.to='$rid') or (chat.from='$rid' and chat.to='$userid') and receivertype='$receivertype' and  usertype='$utype'";
		$ce=mysql_query($ch) or die(mysql_error());
		$cr=mysql_fetch_array($ce);
		$cn=mysql_num_rows($ce);
		
		echo json_encode(array("session_id" =>$cr['text_session']), 200); 
		
	
	}
	
	
function ajax_newchat()
{ 
  

	
	  
 
	
//$this->data['rid']=$_POST['Recvierid'];
//$this->data['ctitle']=$_POST['chatboxtitle'];
//$this->data['ctype']=$_POST['chatwith'];
//$this->data['receivertype']=$_POST['receivertype'];
//$this->load->view('home/ajax_newchat', $this->data);
//session_start();
$rid=$_POST['rid'];
//$ctitle=$_POST['chatboxtitle'];
//$ctype=$_POST['chatwith'];
$userid=$_POST['sid'];

/*
$ch="select * from chat where (chat.from='$userid' and chat.to='$rid') or (chat.from='$rid' and chat.to='$userid')";
$ce=mysql_query($ch) or die(mysql_error());
$cr=mysql_fetch_array($ce);
$cn=mysql_num_rows($ce);
if($cn>0)
{
$session_id=$cr['text_session'];
}
else
{
	$session_id=$_POST['session_id'];
}
*/

//$chatboxtitle=$ctitle;   and text_session='$session_id'
//$loginusertye=$this->session->userdata('usertypeid');
$receivertype;
//and usertype='$loginusertye' and receivertype='$receivertype'
/*
$sub="";
if(!empty($_POST['session_id'])){
$session_id = $_POST['session_id'];
$sub = "and text_session='$session_id'";
}*/
$session_id = $_POST['session_id'];
 $cs="select * from chat where (chat.from='$rid' or chat.to='$rid') and (chat.from='$userid' or chat.to='$userid')  and text_session='$session_id'   order by id desc limit 10";
$ce=mysql_query($cs) or die(mysql_error());
$cn=mysql_num_rows($ce);
if($cn>0)
{
while($cr=mysql_fetch_array($ce))
{
	$s="select * from caregiver where id='$cr[from]'"; 
    $e=mysql_query($s) or die(mysql_error());
	$n=mysql_num_rows($e);
	if($n>0)
	{
	$ns="select * from caregiver where id='$cr[from]'"; 
    $ne=mysql_query($ns) or die(mysql_error());
	$nr=mysql_fetch_array($ne);
	}
	else
	{
     $ns="select * from professionals where id='$cr[from]'"; 
    $ne=mysql_query($ns) or die(mysql_error());
	$nr=mysql_fetch_array($ne);
		
	}
	if($userid==$cr['from'])
	{
		$messages['sty']="right";
	if(!empty($nr['photo']))
	{
	$messages['pimg']=$nr['photo'];
	}
	else
	{
		$messages['pimg']="img-holder2.png";
	}
	}
	else
	{
	$messages['sty']="left";	
	if(!empty($nr['photo']))
	{
	$messages['pimg']=$nr['photo'];	
	}
	
	else
	{
		
		$messages['pimg']="img-holder2.png";
	}
	
	}
	
	$messages['name']=$nr['name'];
	$messages['s'] = '0';
	$messages['f'] = $cr['from'];
	$messages['id'] = $cr['id'];
	$messages['m']=$cr['message'];
	$messages['sent']=$cr['sent'];
	//$messages['session_id']=$cr['text_session'];
	//$_SESSION['MinId'] = $cr['id'];
	$this->session->set_userdata('MinId', $cr['id']);
	$msg[] = $messages;

}
$chatmsgs= $msg;
$lastmessage = reset($chatmsgs);
$msg=array_reverse($msg);
//print_r($lastmessage);
 //$this->session->set_userdata('MinId', $lastmessage['id']);
  //$_SESSION['chatMaxID'] = $lastmessage['id'];
  //$_SESSION['chatMaxID'] = $lastmessage['id'];
  $this->session->set_userdata('chatMaxID', $lastmessage['id']);
  
  //$_SESSION['MinId'] ;
 //print_r($msg);
//echo $result= json_encode($msg);

echo json_encode(array("result" =>"success","caregiver_chat" =>$msg), 200);




?>

<?php
}
}



function ajax_newchathotbeat()
{ 
  


//$this->data['rid']=$_POST['Recvierid'];
//$this->data['ctitle']=$_POST['chatboxtitle'];
//$this->data['ctype']=$_POST['chatwith'];
//$this->data['receivertype']=$_POST['receivertype'];
//$this->load->view('home/ajax_newchat', $this->data);
//session_start();
$rid=$_POST['rid'];
//$ctitle=$_POST['chatboxtitle'];
//$ctype=$_POST['chatwith'];
$userid=$_POST['sid'];
//$chatboxtitle=$ctitle;
//$loginusertye=$this->session->userdata('usertypeid');
$receivertype;
//and usertype='$loginusertye' and receivertype='$receivertype'
$maxid=$this->session->userdata('chatMaxID');


 $cs="select * from chat where (chat.from='$rid' or chat.to='$rid') and (chat.from='$userid' or chat.to='$userid')  and id>'$maxid'   order by id asc";
$ce=mysql_query($cs) or die(mysql_error());
$cn=mysql_num_rows($ce);
if($cn>0)
{
while($cr=mysql_fetch_array($ce))
{
	$s="select * from caregiver where id='$cr[from]'"; 
    $e=mysql_query($s) or die(mysql_error());
	$n=mysql_num_rows($e);
	if($n>0)
	{
	$ns="select * from caregiver where id='$cr[from]'"; 
    $ne=mysql_query($ns) or die(mysql_error());
	$nr=mysql_fetch_array($ne);
	}
	else
	{
     $ns="select * from professionals where id='$cr[from]'"; 
    $ne=mysql_query($ns) or die(mysql_error());
	$nr=mysql_fetch_array($ne);
		
	}
	if($userid==$cr['from'])
	{
		$messages['sty']="right";
	if(!empty($nr['photo']))
	{
	$messages['pimg']=$nr['photo'];
	}
	else
	{
		$messages['pimg']="img-holder2.png";
	}
	}
	else
	{
	$messages['sty']="left";	
	if(!empty($nr['photo']))
	{
	$messages['pimg']=$nr['photo'];	
	}
	
	else
	{
		
		$messages['pimg']="img-holder2.png";
	}
	
	}
	
	$messages['name']=$nr['name'];
	$messages['s'] = '0';
	$messages['f'] = $cr['from'];
	$messages['id'] = $cr['id'];
	$messages['m']=$cr['message'];
	$messages['sent']=$cr['sent'];
	//$_SESSION['MinId'] = $cr['id'];
	$this->session->set_userdata('MinId', $cr['id']);
	
	
	$this->session->set_userdata('chatMaxID', $cr['id']);
	$msg[] = $messages;

}
$chatmsgs= $msg;
$lastmessage = reset($chatmsgs);
$msg=array_reverse($msg);
//print_r($lastmessage);

 $this->session->set_userdata('MinId', $lastmessage['id']);
 
 
  //$_SESSION['chatMaxID'] = $lastmessage['id'];
  //$_SESSION['MinId'] ;
 //print_r($msg);
//echo $result= json_encode($msg);

echo json_encode(array("result" =>"success","caregiver_chat" =>$msg), 200);




?>

<?php
}
}





function sendchatinsert()
{
//echo json_encode(array("result" =>"success","caregiver" =>'okokokokokoko'), 200); 
	//$fr=$_POST['sid'];
	
	
    $name=$_POST['name'];
	$from =$_POST['from']; 
	$to=$_POST['to'] ;
	$message=$_POST['message'];
    $receivertype=$_POST['recivertype'];
	
	$utype=$_POST['usertype'];
	if($utype=="professional")
	{
	$utype=2;	
	}
	if($utype=="caregiver")
	{
	$utype=1;
	}
	$loginusertye=$utype;
	$whn=time(); 
	if($receivertype==2)
	{
		$res="professional";
		$ps="select * from professionals where id='$to'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pr=mysql_fetch_array($pe);
		$rname=$pr['name'];
		
		
	}
    if($receivertype==1)
	{
		$res="caregiver";
		
		$ps="select * from caregiver where id='$to'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pr=mysql_fetch_array($pe);
		$rname=$pr['name'];
		
		
	}



$ch="select * from chat where (chat.from='$from' and chat.to='$to') or (chat.from='$to' and chat.to='$from')";
$ce=mysql_query($ch) or die(mysql_error());
$cr=mysql_fetch_array($ce);
$cn=mysql_num_rows($ce);
if($cn<=0)
{
$var=  uniqid('', true);
//$var;

$sql = "insert into chat (text_session,whn,receivertype,chat.from,chat.to,message,usertype,sent) values ('$var','$whn','$receivertype','".mysql_real_escape_string($from)."', '".mysql_real_escape_string($to)."','".mysql_real_escape_string($message)."','".$loginusertye."',NOW())";
$query = mysql_query($sql) or die(mysql_error());
$lid=mysql_insert_id();
}
if($cn>0)
{
//$ch="select * from chat where (chat.from='$from' or chat.to='$from') and (chat.from='$to' or chat.to='$to')";
//$ce=mysql_query($ch) or die(mysql_error());
//$cr=mysql_fetch_array($ce);
$var=$cr['text_session'];
$sql = "insert into chat (text_session,whn,receivertype,chat.from,chat.to,message,usertype,sent) values ('$var','$whn','$receivertype','".mysql_real_escape_string($from)."', '".mysql_real_escape_string($to)."','".mysql_real_escape_string($message)."','".$loginusertye."',NOW())";
$query = mysql_query($sql) or die(mysql_error());
$lid=mysql_insert_id();
}




$this->session->set_userdata('chatMaxID',$lid);
//$_SESSION['chatMaxID']=mysql_insert_id();

$nmsg=$name."  Had Send New Message ";
$ntype="messages".$utype;
$nip=$_SERVER['REMOTE_ADDR'];
$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype,text_session) values('$nmsg','$from','$to','$lid','0','$ntype','$nip','$res','$var')";
$nte=mysql_query($nts) or die(mysql_error());	

if($utype==2 and $receivertype==2)
{
	$logintype="prof";
}
if($utype==1 and $receivertype==2)
{
	$logintype="care";
}
if($utype==2 and $receivertype==1)
{
	$logintype="careprof";
}
//echo json_encode(array("result" =>"success","caregiver" =>'okokokokokoko'), 200); 

	
	$ds="select * from device_tocken where userid='$to' and user_type='$res'";
	$se=mysql_query($ds) or die(mysql_error());
	while($sr=mysql_fetch_array($se))
	{
		$device=$sr['tockenid'];
		$device_platform = $sr['platform'];
		$msg = array(
						'message'       => $message,
						'title'         => $name.'Send The Message',
						'subtitle'      => 'Chat Mssage',
						'sid'           =>$from,
						'rid'           =>$to,
						'sname'         =>$name,
						'recivertype'   =>$loginusertye,
						'logintype'     =>$logintype,
						'chat'          =>"text", 
						'call_type'     =>"text",
						'session_id'    =>$var,
						'rname'         =>$rname,
						'vibrate'        => 1,
						'sound'         => 1
					);
		$res = $this->sendPushNotificationToGCM($device,$msg,$device_platform,$name,$from,$to,$loginusertye,$logintype,$var,$rname);
        
	}
	//$dev="APA91bE-RtPXNS3cCKIl8QvZT7W7_JJzKjgp1dGHLng4yaS0IhwSqYyyt1A2wR1ndVdCbShcj5Ag4SthsP5q0b0pUerYQdxo3dZ5xK6ukOfRr10TT_6OvKNYdw_rv-UgfPv5uPTEk3LZrn5MtNkTdG2HXiWcad-R0w";
	
//$res = $this->sendPushNotificationToGCM($dev);	
	
	

	
	
	
	
		
	
}

function check_chat_session()
{
	
$from=$_POST['from'];
$to=$_POST['to'];
$session_id=$_POST['session_id'];

$ch="select * from chat where (chat.from='$from' and chat.to='$to') or (chat.from='$to' and chat.to='$from') and text_session='$session_id'";
$ce=mysql_query($ch) or die(mysql_error());
$cr=mysql_fetch_array($ce);
$cn=mysql_num_rows($ce);
if($cn<=0)
{
	echo json_encode(array("result" =>"success","count" =>$cn,"session_id" =>$session_id), 200);
}
else
{
	echo json_encode(array("result" =>"success","count" =>$cn,"session_id" =>$session_id), 200);
}

}


function messageread()
{  
 
	
//$email=$this->session->userdata('owner_care');
//$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
	 $cid1=$_POST['sid'];
	 $rid=$_POST['rid'];
	
	// $pa = $this->user->update_chat($cid1,$rid);
	$us="update chat set recd='1' where chat.to='$cid1' and chat.from='$rid'";
	$ue=mysql_query($us) or die(mysql_error());
	
	//echo json_encode(array("result" =>"success","msg" =>$us), 200); 
	
	//$pa1 = $this->user->update_chat_pop($cid1,$rid);
	
	 $us1="update popup_notifications set status='1' where n_sid='$rid' and n_rid='$cid1'";
	$ue1=mysql_query($us1) or die(mysql_error());

	
}

function local_notifications_read(){
		$n_id = $_POST['n_id'];
		$user_id = $_POST['user_id'];
		
		$us1="update popup_notifications set status='1' where n_id='$n_id' and n_rid='$user_id' ";
		$ue1=mysql_query($us1) or die(mysql_error());
}

function local_notifications_push_read(){
		$n_id = $_POST['n_id'];
		$user_id = $_POST['user_id'];
		
		 $us1="select * from popup_notifications  where n_id='$n_id' and n_rid='$user_id' ";
		$ue1=mysql_query($us1) or die(mysql_error());
		$res = mysql_fetch_array($ue1);
		 $nm_id = $res['nm_id'];
		 
		
		 $kk = "select * from notifications where note_id=".(int)$nm_id;
		$ue1=mysql_query($kk) or die(mysql_error());
		$res = mysql_fetch_array($ue1);
		$event_type_id = $res['event_type_id'];
		
		
		echo json_encode(array("result" =>"success","n_id"=>$n_id,"nm_id" =>$nm_id,"event_type_id"=>$event_type_id), 200); exit;
}


function requestread(){
		$nm_id = $_POST['nm_id'];
		$user_id = $_POST['user_id'];
		$receivertype=  $_POST['receivertype'];
		
		if($receivertype==1){
		
			$sql = "SELECT * FROM professional_networks WHERE id = '$nm_id' and netstatus='1'";
		} else {
			$sql = "SELECT * FROM professional_networks_prf WHERE id = '$nm_id' and netstatus='1'";
		}
			$ue=mysql_query($sql) or die(mysql_error());
			$count = mysql_num_rows($ue);
			
			if($count<=0){
			
			$us1="update popup_notifications set status='1' where nm_id='$nm_id' and n_rid='$user_id' ";
			$ue1=mysql_query($us1) or die(mysql_error());
			} else {
				$us1="update popup_notifications set status='1' where nm_id='$nm_id' and n_rid='$user_id' ";
			$ue1=mysql_query($us1) or die(mysql_error());
				echo $count;
			}
}



function buy_licence()
{
	$oid = $_POST['user_id'];
	$email = $_POST['user_email'];
	$to_name = $_POST['user_disp_name'];
	$num=$this->user->get_owner_licence($oid);
	$id=$this->user->insert_anether_olicence($oid,$email,$num);
	$lic="koala".$oid."_".$num;
	
	if($id)
	{
		
		$subject = "Koala-Licence Purchase Confirmation.";
				$to = $email; //"mahamoodphp@gmail.com";//
				$to_name = $_POST["user_disp_name"];
				$body = "
				<html>
				<head>
				<title>HTML email</title>
				</head>
				<body>
				<table align='left' width='700' border='0'>
				<tr>
				<th align='left'> Email: </th>
				<td>$email</td>
				</tr>
				<tr>
				<th align='left'> Licence Id: </th>
				<td> $lic </td>
				</tr> 
				</table>
				</body>
				</html>
				";
				
				$res = $this->mail_function($to,$to_name,$subject,$body);
				
				
	}
	
	//echo json_encode(array("result" =>"success","caregiver_chat" =>$num), 200); exit;
}


	function upgrade_licence(){

		$licence = $_POST['licence'];
		$id = $_POST['user_id'];
	
		$oid = $_POST['user_id'];
		$email = $_POST['user_email'];
		$to_name = $_POST['user_disp_name'];
		
		$lic=$licence;
	
	
		$num=$this->user->get_owner_by_licence($licence,$id);
			
		
		if($num>0)
		{
			$id=$this->user->updateownerlicencestatus1($licence);
		
			
				$subject = "Koala-Licence Upgrade Confirmation.";
				$to = $email; //"mahamoodphp@gmail.com";//
				$to_name = $_POST["user_disp_name"];
				$body = "
				<html>
				<head>
				<title>HTML email</title>
				</head>
				<body>
				<table align='left' width='700' border='0'>
				<tr>
				<th align='left'> Email: </th>
				<td>$email</td>
				</tr>
				<tr>
				<th align='left'> Licence Id: </th>
				<td> $lic </td>
				</tr> 
				</table>
				</body>
				</html>
				";
			
			$res = $this->mail_function($to,$to_name,$subject,$body);
		} else {
			
			echo json_encode(array("result" =>"success","message" =>"Entered Licence is already Extended or Invalid."), 200); exit;
		}
	 
	 

	}
	
	
	
	
	
	
	
	
	
	 function sendPushNotificationToGCM($deviceid,$msg1,$device_platform,$name,$from,$to,$loginusertye,$logintype,$var,$rname) {

		$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

			//echo $device_platform; exit;
		if($device_platform=="Android"){
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
			$message = $name.'Send The Message';
			
			$path = base_url().'assets/KoalaProPushCert.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
			$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'sid'           =>$from,
				'rid'           =>$to,
				'sname'         =>$name,
				'recivertype'   =>$loginusertye,
				'logintype'     =>$logintype,
				'chat'          =>"text", 
				'call_type'     =>"text",
				'session_id'    =>$var,
				'rname'         =>$rname
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				 
		}
		
		echo $result;
		
    }
	
	
	
	function push()
	{
		
// Your API key
$api_key = "AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw";

// Client IDs from your application
$registration_ids = array($device);

$message = "hello, test!!";

// URL to POST to
$gcm_url = 'https://android.googleapis.com/gcm/send';

// data to be posted
$fields = array(
                'registration_ids'  => $registration_ids,
                'data'              => array( "message" => $message ),
                );

// headers for the request
$headers = array( 
                    'Authorization: key=' . $api_key,
                    'Content-Type: application/json'
                );

$curl_handle = curl_init();

// set CURL options
curl_setopt( $curl_handle, CURLOPT_URL, $gcm_url );

curl_setopt( $curl_handle, CURLOPT_POST, true );
curl_setopt( $curl_handle, CURLOPT_HTTPHEADER, $headers);
curl_setopt( $curl_handle, CURLOPT_RETURNTRANSFER, true );

curl_setopt( $curl_handle, CURLOPT_POSTFIELDS, json_encode( $fields ) );

// send
$response = curl_exec($curl_handle);

curl_close($curl_handle);

// let's check the response
$data = json_decode($response);

foreach ($data['results'] as $key => $value) {
    if ($value['registration_id']) {
        printf("%s has a new registration id: %s\r\n", $key, $value['registration_id']);
    }
    if ($value['error']) {
        printf("%s encountered error: %s\r\n", $key, $value['error']);
    }
    if ($value['message_id']) {
        printf("%s was successfully sent, message id: %s", $key, $value['message_id']);
    }
}

	}
	
	
	
	
	////////////////////
	
	function save_token_id(){
		
		$userid = $_POST['user_id'];
		$tockenid = $_POST['device_token'];
		$platform = $_POST['device_platform'];
		$user_type = $_POST['user_type'];
		$num = $this->user->check_token_id($userid,$tockenid,$platform,$user_type);
		
		if($num>0){
			$res = $this->user->update_token_id($userid,$tockenid,$platform,$user_type);
			echo json_encode(array("result" =>"success","message" =>"Token Updated Successfully."), 200); exit;
		} else {
			
			$res = $this->user->insert_token_id($userid,$tockenid,$platform,$user_type);
			
			echo json_encode(array("result" =>"success","message" =>"Token Inserted Successfully."), 200); exit;
		}
		
		
	}
	
	
	function destroy_token_id(){
		
		$userid = $_POST['user_id'];
		$tockenid = $_POST['device_token'];
		$user_type = $_POST['user_type'];
		 $this->user->delete_token_id($userid,$tockenid,$user_type);
		
		if($user_type=="caregiver"){
		
		$cs="update caregiver set is_online='0' where id='$userid'";
		$ce=mysql_query($cs) or die(mysql_error());
		} else {
			$cs="update professionals set is_online='0' where id='$userid'";
		$ce=mysql_query($cs) or die(mysql_error());
		}
		
		echo json_encode(array("result" =>"success","message" =>"Token Deleted Successfully."), 200); exit;
		
	}

	function is_online()
	 {
		 
		 $uid = $_POST['uid'];
		 $type = $_POST['type']; 
		 $login_type = $_POST['login_type'];
		
		 if($login_type=="professional") {
		 
			if($type=='video_cc')
			{
				$cs="select * from caregiver where is_online='1' and id='$uid'";
				
				
				$ce=mysql_query($cs) or die(mysql_error());
				 $cn=mysql_num_rows($ce);	
				
				if($cn>0){
					echo json_encode(array("result" =>"online"), 204); exit; 
				} else {
					echo json_encode(array("result" =>"offline"), 204); exit; 	
				}
			
			}
			if($type=='video_cp' || $type=='video_pp')
			{
						if($type=='video_cp'){ 
						$cs="select * from caregiver where is_online='1' and id='$uid'"; //exit;
						} else {
				 $cs="select * from  professionals where is_online='1' and id='$uid'"; //exit;
						}
				$ce=mysql_query($cs) or die(mysql_error());
				$cn=mysql_num_rows($ce);
				
				if($cn>0){
					echo json_encode(array("result" =>"online"), 204); exit; 
				} else {
					echo json_encode(array("result" =>"offline"), 204); exit; 	
				}
				
			}
		   
		 }else if($login_type=="caregiver") {
		   
		   if($type=='video_cc' || $type=='video_cp')
			{
			if($type=='video_cp'){ 
				$cs="select * from professionals where is_online='1' and id='$uid'"; //exit;
			} else {
				$cs="select * from caregiver where is_online='1' and id='$uid'"; //exit;
			} 
			
			
			
			$ce=mysql_query($cs) or die(mysql_error());
			 $cn=mysql_num_rows($ce);	
			
			if($cn>0){
					echo json_encode(array("result" =>"online"), 204); exit; 
				} else {
					echo json_encode(array("result" =>"offline"), 204); exit; 	
				}
			
			
			}
			if($type=='video_pp')
			{
			$cs="select * from  professionals where is_online='1' and id='$uid'";
			$ce=mysql_query($cs) or die(mysql_error());
			 $cn=mysql_num_rows($ce);
			 
			 if($cn>0){
					echo json_encode(array("result" =>"online"), 204); exit; 
				} else {
					echo json_encode(array("result" =>"offline"), 204); exit; 	
				}
		   }
			}
		   
		   
		
		
	 }
	
	
	
	
/*  Send Video call push notification   */
	
function videocall()
{
	     
 		
		
		
		
		$login_user_type = $_POST['login_user_type'];
		$reciver_id = $_POST['reciver_id'];
		$user_type= $_POST['user_type'];
		$token_id =	$_POST['token_id'];
		$session_id = $_POST['session_id'];
		$sender_name = $_POST['sender_name'];
		$sender_id = $_POST['sender_id'];
		
		
		$ps="select * from popup_notifications where is_oncall=1 and (n_rid='$reciver_id' or n_sid = '$reciver_id')  and note_type='$user_type'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pn=mysql_num_rows($pe);
		
		if($pn>0){
			echo json_encode(array("result" =>"In Call","message" =>"Already in call"), 204); exit; 
		} else {
	
	
			//$note_type=$user_type;
			$ip=$_SERVER['REMOTE_ADDR'];
			$date=date("Y-m-d");
			$message=$sender_name." had Send a video call request ";
			
			

	
			if($user_type=="video_cp")
			{
				if($login_user_type=="caregiver"){
					$res="professional";	
				} else {
					$res="caregiver";
				}
			}
			if($user_type=="video_pp")
			{
			$res="professional";
			}
			if($user_type=="video_cc")
			{
			$res="caregiver";
			}
		
			$ps="insert into popup_notifications (n_sid,n_rid,status,note_type,n_ip,n_date,token_id,session_id,note_message,is_oncall,ltype) values('$sender_id','$reciver_id','0','$user_type','$ip','$date','$token_id','$session_id','$message','1','$res')";
			$pe=mysql_query($ps) or die(mysql_error());
		
			define('API_ACCESS_KEY', 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw');
			$device=" ";
			$devices = " ";
			$device_array = array();
		
			$ds="select * from device_tocken where userid='$reciver_id' and user_type='$res'";
			$se=mysql_query($ds) or die(mysql_error());
			$num = mysql_num_rows($se);
		 
			if($num>0){
				while($sr=mysql_fetch_array($se))
				{
					
				
					//$device .= $sr['tockenid'].",";
					$device_array[] = $sr['tockenid'];
					$device_platform = $sr['platform'];
					
					
					if($device_platform=="Android"){
				
				
						$registrationIds = $device_array;
			
						$msg = array(
						  'message'       => $message,//"Testing Video Call Message",
						  'title'         => "Koala- Video Call",
						  'video_session' => $session_id,
						  'video_token' => $token_id,
						  'call_type' => "video",
						  'video_call_type' => $user_type,
						  'sender_id' => $sender_id,
						  'sender_name' => $sender_name,
						  'type'      => '2',
						   'vibrate'   => 1,
						   'sound'     => 1
							);
			
			

		
						$fields = array('registration_ids' => $registrationIds, 'data' => $msg);


						$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);

						$ch = curl_init();
						curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
						curl_setopt($ch,CURLOPT_POST, true );
						curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$result = curl_exec($ch );
						curl_close( $ch );

						//echo $result;
		
						//echo json_encode(array("result" =>"success","message" =>"Connection created"), 200); 
					} elseif($device_platform=="iOS"){
		 
						$deviceToken =$sr['tockenid'];
						$passphrase = 'bravemount';
			
						$message=$sender_name." had Send a video call request ";
			
						$path = base_url().'assets/KoalaProPushCert.pem';
			
						//echo $path; exit;
						$ctx = stream_context_create();
			
						$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
						stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
						stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
						stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
						// Open a connection to the APNS server
						$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			
						$body['aps'] = array(
							'alert' => $message,
							'sound' => 'beep.caf',
							'video_session' => $session_id,
						  'video_token' => $token_id,
						  'call_type' => "video",
						  'video_call_type' => $user_type,
						  'sender_id' => $sender_id,
						  'sender_name' => $sender_name,
						  'type'      => '2'
							
						);

						// Encode the payload as JSON
						$payload = json_encode($body);

						// Build the binary notification
				
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			

						// Send it to the server
						$result = fwrite($fp, $msg, strlen($msg));

						//if (!$result)
						//	echo 'Message not delivered' . PHP_EOL;
						//else
						//	echo 'Message successfully delivered' . PHP_EOL;

						// Close the connection to the server
						fclose($fp);
			
				 
					}
				
				}	  
			}
					echo json_encode(array("result" =>"success","message" =>"Connection created."), 200); exit; 
		}//else close
	}
	
	
	
	
	
		
	function test_push(){
		
		// API access key from Google API's Console
define('API_ACCESS_KEY', 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw');
  
 
  
$registrationIds = array("APA91bEMkFLwSZ0ZDeK2iSPFfLtV7OBHX3q3iC9eXIFPhYxpUkVgF92DWa64i4NJuyru_RbM81q0H_5cZQxb0SF3RdLpbLwPnavIBdfblWPAwd8TrfT2o0exGlyzyiMN9gxW52mNaBur");


  
  $msg = array(
     'message'       => "Testing Video Call Message"
     
    );
  
  





  $fields = array
  (
   'registration_ids'  => $registrationIds,
   'data'              => $msg
  );

  $headers = array
  (
   'Authorization: key=' . API_ACCESS_KEY,
   'Content-Type: application/json'
  );

  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
  curl_setopt($ch,CURLOPT_POST, true );
  curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch );
  curl_close( $ch );

  echo $result;
  
  echo "nonononononononononno";
		
	}
	
	
	
	
	///////////////////////Disconnect Video call

	function reject_videocall()
	{
	     
 		$reciver_id = $_POST['reciver_id'];
		$user_type= $_POST['user_type'];
		$token_id =	$_POST['token_id'];
		$session_id = $_POST['session_id'];
		$sender_name = $_POST['sender_name'];
		$sender_id = $_POST['sender_id'];
	
		
		if($_POST['call_status']!="accept" && $_POST['call_status']!="reject") 
		$call_status = "miss";
		else 
		$call_status = $_POST['call_status'];
	
		//$note_type=$user_type;
		$ip=$_SERVER['REMOTE_ADDR'];
		$date=date("Y-m-d");
		$message=$sender_name." had rejected your video call request";
		
				//$msg=$name." had rejected your call";
		$ps="update popup_notifications set call_status='$call_status',is_oncall='0' where session_id='$session_id' and status='0'";
		$pe=mysql_query($ps) or die(mysql_error());
	
		

		
		if($user_type=="video_cp")
		{
		$res="caregiver";
		}
		if($user_type=="video_pp")
		{
		$res="professional";
		}
		
		
		define('API_ACCESS_KEY', 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw');
		$device=" ";
		$devices = " ";
		$device_array = array();
		
		$ds="select * from device_tocken where userid='$reciver_id' and user_type='$res'";
		 $se=mysql_query($ds) or die(mysql_error());
		 $num = mysql_num_rows($se);
		 if($num>0){
			while($sr=mysql_fetch_array($se))
			{
				if($sr['tockenid']){
				
				//$device .= $sr['tockenid'].",";
				$device_array[] = $sr['tockenid'];
				$device_platform = $sr['platform'];
				
				
                if($device_platform=="Android"){
				
				
					 $registrationIds = $device_array;
			
		
			
					  $msg = array(
					  'message'       => $message,
					  'title'         => "Koala- Video Call",
					  'video_session' => $session_id,
					  'video_token' => $token_id,
					  'call_type' => $_POST['call_type'],//"video_call_rejection",
					  'video_call_type' => $user_type,
					  'sender_id' => $sender_id,
					  'sender_name' => $sender_name,
					  'type'      => '2',
					   'vibrate'   => 1,
					   'sound'     => 1
						);
			
			

		
						$fields = array('registration_ids' => $registrationIds, 'data' => $msg);


						$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);

						$ch = curl_init();
						curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
						curl_setopt($ch,CURLOPT_POST, true );
						curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$result = curl_exec($ch );
						curl_close( $ch );

						echo $result;
		
						//echo json_encode(array("result" =>"success","message" =>"Connection destroyed"), 200); 
					} elseif($device_platform=="iOS"){
		 
						$deviceToken =$sr['tockenid'];
						$passphrase = 'bravemount';
			
						$message=$sender_name." had rejected your video call request";
			
						$path = base_url().'assets/KoalaProPushCert.pem';
			
						//echo $path; exit;
						$ctx = stream_context_create();
			
						$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
						stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
						stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
						stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
						// Open a connection to the APNS server
						$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			
						$body['aps'] = array(
							'alert' => $message,
							'sound' => 'default',
							'video_session' => $session_id,
							  'video_token' => $token_id,
							  'call_type' => $_POST['call_type'],//"video_call_rejection",
							  'video_call_type' => $user_type,
							  'sender_id' => $sender_id,
							  'sender_name' => $sender_name,
							  'type'      => '2'
							
						);

						// Encode the payload as JSON
						$payload = json_encode($body);

						// Build the binary notification
				
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			

						// Send it to the server
						$result = fwrite($fp, $msg, strlen($msg));

						//if (!$result)
						//	echo 'Message not delivered' . PHP_EOL;
						//else
						//	echo 'Message successfully delivered' . PHP_EOL;

						// Close the connection to the server
						fclose($fp);
			
				 
					}
				
				
				
				}
			}
						echo $result;
		}
	
    }
	
	
	
	
	
	
	
	////////////////If in video call or not check condition
	
	function check_call(){
		
		$session_id = $_POST['session_id'];
		
		$ps="update popup_notifications set call_status='accept' where session_id='$session_id'";
		$pe=mysql_query($ps) or die(mysql_error());
		
		$ps="select * from popup_notifications where session_id='$session_id' and is_oncall='1'";
		$pe=mysql_query($ps) or die(mysql_error());
		$count = mysql_num_rows($pe);
		echo json_encode(array("result" =>"success","count" =>$count), 200); 
	}
	
	
	
	
	
	
	////GET Notifications

	function get_notifications(){
		$user_id = $_POST['user_id'];
		$user_type = $_POST['user_type'];
		
		//all notification read
					
		
		$notifications_count=$this->user->get_notifications_count($user_id,$user_type);
		  
				if($notifications_count<=0)
				{
					echo json_encode(array('result' =>'' ,'count'=>$notifications_count,'message'=>'No new notifications'), 204);
				}
				else
				{
					
					
					
					$notifications= $this->user->get_notifications($user_id,$user_type);
					
					 $update_qry = "update popup_notifications set status=1 where n_rid='$user_id' and ltype='$user_type'  ";
					mysql_query($update_qry)or die(mysql_error());
					
					echo json_encode(array("result" =>"success","all_notifications"=>$notifications), 200);
					
				}
			
	}	
	
	
	function get_notifications_unread_count(){
		$user_id = $_POST['user_id'];
			$user_type = $_POST['user_type'];
		$notifications_count=$this->user->get_notifications_count_unread($user_id,$user_type);
		 echo json_encode(array('result' =>'success' ,'count'=>$notifications_count,'message'=>'No new notifications'), 200); 
				
			
	}	
	
	
	////Requesting waiting list professional from caregivers
	
	function get_wainting_requests(){
		$user_id = $_POST['user_id'];
		
		$care_requests_count=$this->user->get_wainting_requests_count($user_id);
		
		if($care_requests_count<=0)
				{
					echo json_encode(array('result' =>'' ,'count'=>$care_requests_count,'message'=>'No new requests'), 204);
				}
				else
				{
		
					$care_requests=$this->user->get_wainting_requests($user_id);
					echo json_encode(array("result" =>"success","care_requests"=>$care_requests), 200);
					
				}
				
			
	}	
	
	//Notification settings
	function caregiver_set_notifications()
	{ 
		$cid1= $_POST['user_id'];
		$pid = $_POST['patient_id'];
		$notification_settings=$this->user->get_notify($cid1,$pid);
	 

		
		echo json_encode(array("result" =>"success","notification_settings"=>$notification_settings), 200);
	}
	
	function edit_notif()
	{
		$withp=$_POST['status'];  
		$patient_id=$_POST['patient_id'];
		$caregiver_id=$_POST['user_id'];
		$parameter=$_POST['perameters'];
		$medicine=$_POST['medicines'];
		$diet=$_POST['diet'];
		$activity=$_POST['activities'];
		$cdate=date("Y-m-d");
		$nid=$_POST['note_id'];
		
		
		$ins=$this->user->update_set_notification($patient_id,$caregiver_id,$parameter,$medicine,$diet,$activity,$cdate,$nid,$withp);
		echo json_encode(array("result" =>$ins), 200);
				
		//echo json_encode(array("result" =>"success"), 200);
	
	}
	
	function care_notification_default(){
		
			$careid = $_POST['user_id'];
		
			  $ls="select * from list_caregiver where careid='$careid'";
			  $le=mysql_query($ls) or die(mysql_error());
			  while($lr=mysql_fetch_array($le))
			  {
			   $pid=$lr['pat_id'];
			   $date=date("Y-m-d");
			   $l="select * from set_notifications where car_id='$careid' and pat_id='$pid'";
			   $e=mysql_query($l) or die(mysql_error());
			   $n=mysql_num_rows($e);
				 if($n==0)
				 {
			   $is="insert into set_notifications(pat_id,car_id,parameter_status,medical_status,diets_status,activity_status,cdate) values('$pid','$careid','1','1','1','1','$date')";
			   $ie=mysql_query($is);
				 }
			   
			  }
		
	}
	
	
	function sub_event_col(){
		
			$pid = $_POST['id'];
		
			$query = $this->db->query("SELECT * FROM parameter_name where id='$pid'");	
			$res =  $query->result();
			
			echo json_encode(array("result" =>"success","parameter_name"=>$res), 200);
	}
	
	/////////////////////////////////////////
	function insert_event_week()
{ 

  $t="";
  
 
  //echo $time1;
  $mdate=array();
  $mday=array();
  
 for($d=0;$d<7;$d++)
 {  
  
 // $Date=Date('Y-m-d', strtotime("- days"));
  
  
    $NewDate=Date('Y-m-d', strtotime("+$d days"));
    $time=mktime(12, 0, 0, date('m'), $d, date('Y'));
  if (date('m', $time)==date('m'))
     //$list[]=date('Y-m-d', $time);
        $t=date($NewDate, $time);
   $day = strtolower(date("w",strtotime($t)));
  $mdate[]=$t;
 if($day==0)
 {
  break;
 }
 
  
 
  
  }
  
 return $mdate;
 
 
}




function get_days_in_month($month, $year)
{
 
   return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
}


function insert_event_month($ndays)
{
 
 
  $f;
 $list=array();
   
  for($d=1; $d<=$ndays; $d++)
            {
    $time=mktime(12, 0, 0, date('m'), $d, date('Y'));
    if (date('m', $time)==date('m'))
      date('Y-m-d', $time);
   $t=date('Y-m-d', $time);
   $day = strtolower(date("w",strtotime($t)));
  
  $curdate =date("Y-m-d");
  
  if($t>=$curdate)
  {
   $list[]=$t;
  
        }
 }
 
 return $list;
 
 
 
}




function getAllDatesBetweenTwoDates($strDateFrom,$strDateTo)
{
 
$aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));
      
    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        $d=0;
  while ($iDateFrom<$iDateTo)
        {
   $d++;
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
  $NewDate=date('Y-m-d',$iDateFrom);
   
    //$NewDate=Date('Y-m-d', strtotime("+$d days"));
    $time=mktime(12, 0, 0, date('m'), $d, date('Y'));
    if (date('m', $time)==date('m'))
       $list[]=date('Y-m-d', $time);
    $t=date($NewDate, $time);
  $day = strtolower(date("w",strtotime($t)));
   
        }
    }
    return $aryRange;

 
}




function patient_events(){
	
		$patient_id = $_POST['patient_id'];
		$present_date = date("Y-m-d");
		$user_id = $_POST['user_id'];
				
		$query = $this->db->query("SELECT note_id FROM set_notifications WHERE pat_id='$patient_id' AND car_id='$user_id' ");//AND withp='yes'
		$count = $query->num_rows();
		
		if($count>0){
		
			$query = $this->db->query("SELECT note_id FROM set_notifications WHERE pat_id='$patient_id' AND car_id='$user_id' ");//AND note_date='$present_date' AND status='-1'
			
			$res=  $query->result();
			echo json_encode(array("result" =>"success","local_notifications"=>$res,"date"=>$present_date), 200);
		} else {
			echo json_encode(array("result" =>""), 204);
		}
		
}

function patient_events1(){
	
		$patient_id = $_POST['patient_id'];
		$present_date = date("Y-m-d");
		$user_id = $_POST['user_id'];
				
				
		
		
			$query = $this->db->query("SELECT * FROM notifications WHERE pat_id='$patient_id' AND note_date='$present_date' AND status='-1'");$res=  $query->result();
			
			
			
			
			echo json_encode(array("result" =>"success","local_notifications"=>$res,"date"=>$present_date), 200);
		
		
}




function  list_caregiver_patient(){
	
	//$timezone = $_POST['timezone'];
		
		 
		 
		// $this->session->set_userdata('timezone', $timezone);
   // $timezone=$this->session->userdata('timezone');
	//	 date_default_timezone_set($timezone);
		 
	$user_id = $_POST['user_id'];
	
	$query = $this->db->query("SELECT * FROM  list_caregiver WHERE careid='$user_id'");
	$count = $query->num_rows();
	
	if($count>0){
		$res=  $query->result();
		
		
		echo json_encode(array("result" =>"success","pat_ids"=>$res), 200);
	} }
	
	

function list_pat_not_id(){
		$patient_id = $_POST['patient_id'];
		$present_date = date("Y-m-d");
		$user_id = $_POST['user_id'];
		
		$query = $this->db->query("SELECT note_id FROM notifications WHERE pat_id='$patient_id'  AND note_date='$present_date'");$res=  $query->result();
		echo json_encode(array("result" =>"success","pat_ids"=>$res), 200); }


// checking pending events when view the patient details 
          
       function getPendingEvents(){
            $patient_id = $_POST['patient_id'];
           $data = $this->user->getPendingEventsByPatient($patient_id);
           if($data['count'] > 0){
			   echo json_encode(array("result" =>"success","patient_id"=>$patient_id,"count"=>$data['count'] ), 200);
			   }
		   
	   }
	   
	    function getPendingEvents1(){
            $patient_id = $_POST['patient_id'];
			$c_id = $_POST['c_id'];
           $data = $this->user->getPendingEventsByPatient1($patient_id,$c_id);
           if($data['count'] > 0){
			   echo json_encode(array("result" =>"success","patient_id"=>$patient_id,"count"=>$data['count'] ), 200);
			   }
		}



/* view set event details  */
function show_parameter1()
{
	 $id=$_POST['para_id'];
	 $eid=$_POST['event_id'];

	$ustatus=$this->user->num_notification($eid);
		 $status=$ustatus[0]->status;
		 if($status==1)
		{
			//redirect('/caregiver/showparameter/'.$pid);
		}
		$this->data['parameters']=$this->user->parameteslist();
		
		
		$this->data['id']=$id;
	  $this->data['evid']=$eid;
	  ?>
<form  method="post" id="form-register" name="form-register" action="" enctype="multipart/form-data">
				<div class="content-cont nw-evnt">
						<div class="fieldset"><label>Type</label>
                        <select  name="acttype" required id="act" style="width:60%;">
						<?php 
						foreach($this->data['parameters'] as $temp ) 
						{ 
						$id1=$temp->id;
						if($id1==$id)
						{
						?>
                        <option value="<?php echo $temp->id; ?>" selected><?php  echo $temp->pname; ?></option>
						<?php } } ?>
                        </select>
                        </div>
						<div class="clearfix"></div>
		              <?php 
					   $es="select * from parameter_columns where para_id='$id'"; 
					  $ex=mysql_query($es) or die(mysql_error());
					  $i=0;
					  while($er=mysql_fetch_array($ex))
					  {
						  $i++;
					  ?> 
<div class="fieldset"> <label> <?php echo $er['paraname']; ?> </label>
<input type="text"	name="para_<?php echo $i; ?>"  required="required" onkeypress="return isNumberKey(event)"/>
</div>	   
<?php } ?>

<div class="clearfix"></div>    
                    
					<div class="btn-cont">
						<div class="col-md-5 pull-left btn no-padding btns3">
							<a href="javascript:void(0)"  class="btn btn-success back-btn" aria-label="Left Align" style="margin-right:10px;" onclick="addparameter1()">
										 Confirm 
										</button>
<a href="patient-pendingevents-list.html" class="btn btn-default"  > Cancel </a>
										
							
							
							
						</div>
					</div>
				</div>
				</form>
	  
	  
<?php	  
		
}



/*  add the paramert when the event is set */
function add_parameter1()
{
	
	
	 $id=$_POST['para_id'];
	 $eid=$_POST['event_id'];
	$patient_id=$_POST['patid'];
	$caregiver_id=$_POST['user_id'];
	//echo $pnum=extract($_POST);
	$ps="select * from parameter_columns where para_id='$id'";
	$pe=mysql_query($ps) or die(mysql_error());
	 $pnum=mysql_num_rows($pe);
	
	
		
		
		$date=date("Y-m-d");
		$praid=$_POST['acttype'];
		
		
		if($pnum==1)
		{
			 $opt1=$_POST['para_1'];
			
		}
	
		
		
		if($pnum==2)
		{
			@$opt1=$_POST['para_1'];
			@$opt2=$_POST['para_2'];
		}
		
		
			
		if($pnum==3)
		{
			@$opt1=$_POST['para_1'];
			@$opt2=$_POST['para_2'];
			@$opt3=$_POST['para_3'];
		}
	
	
		
		if($pnum==4)
		{
			@$opt1=$_POST['para_1'];
			@$opt2=$_POST['para_2'];
			@$opt3=$_POST['para_3'];
			@$opt4=$_POST['para_4'];
		}
		
		
		
		
		if($pnum==5)
		{
			@$opt1=$_POST['para_1'];
			@$opt2=$_POST['para_2'];
			@$opt3=$_POST['para_3'];
			@$opt4=$_POST['para_4'];
			@$opt5=$_POST['para_5'];
		}

		
	$ustatus=$this->user->num_notification($eid);
		 $status=$ustatus[0]->status;
		
			
		
		
		$ins=$this->user->parametesinsert($caregiver_id,$patient_id,$praid,@$opt1,@$opt2,@$opt3,@$opt4,@$opt5);
		
	$ups="update notifications set notifications.status=1 where note_id='$eid'";
	$upe=mysql_query($ups) or die(mysql_error());	
		
	echo json_encode(array("result" =>"success"), 200);
	

	
		
		if($status==1)
		{
			//redirect('/caregiver/showparameter/'.$pid);
		}
		
		
		
	
	
	
	
}







		
/*  patients pending routines diaplay */		
function getPendingEventsList(){ 
           $patient_id = $_POST['patient_id'];
           $data = $this->user->getPendingEventdetails($patient_id);
		   $height=$_POST['height'];
		   
           if(count($data) > 0){
              ?>
              <div id="event_pending1" style="display:block; overflow-y:auto; height:<?php echo ($height-200) ?>px; margin-bottom:10%;">

                <form id="pending_events_list"   method="post" name="pending_events_list"  >
               <table width="611" class="table table-bordered table-hover">
                    <tr><td>Event</td><td>Time</td><td>Action</td></tr>
            
                <?php
				$i=0;
               foreach($data as $pendingEvent){
                   $i++;
				   $seconds = $pendingEvent['gmt_time'] / 1000;
                    $event_name = $this->db->query("select evn_name from event_type where evn_id=".$pendingEvent['event_type_id'])->row_array();          
                   ?>
                    <tr>
                        <td><?php echo $event_name['evn_name'];?></td>
                        <td>
                            <?php //echo date("d-m-Y h:m:s", $seconds); 
							echo  date("d-m-Y", strtotime($pendingEvent['note_date'])).' '.$pendingEvent['note_time']
							?>
                            <input type="hidden" name="event_id_<?php echo $i;?>" value="<?php echo $pendingEvent['note_id'];?>"/>
                        </td>
                        <td>
						<?php
						if($pendingEvent['event_type_id'] == 1)	{
						$s1="select * from parameter_name where id='".$pendingEvent['sub_event_id']."'";
							$e1=mysql_query($s1) or die(mysql_error());
							$r1=mysql_fetch_array($e1);
							$sename=$r1['pname'];
						?>
<a href="javascript:void(0)" onclick="add_care_parameter('<?php echo $r1['id']; ?>','<?php echo $pendingEvent['note_id'];  ?>')"> click here for update </a> 
						 
						<?php }else{
						?>
						<select name="status_<?php echo $i; ?>">
                                <option value="-1">Select</option>
                                <option value="1">Done</option>
                                <option value="0">Not Done</option>
                            </select></td>
							<?php }?>
                    </tr>
                       
              <?php  }  ?>
               </table>
               </div>
                    <button class="btn btn-success" type="button" name="submit" onclick="return updateevents()">Update</button>
                   <a href="professional-patient-routine.html" class="btn btn-default"> Cancel</a>
                </form>   
               
           <?php }
        }
	
	
  // updated all events from patient home page
       function updateAllEventsByCaregiver()
       {
       
	   	 if($_POST)
			 {
			 $count=extract($_POST);
			@$stringData =  $_POST['event_id_2'];
			  $patid =  $_POST['patid'];
			 //$data=explode('&',$stringData);
			 //echo  $patid;status_
			  
			  $cid1=$_POST['user_id'];
		$name = $_POST['user_disp_name'];
		$pname=$_POST['patient_name'];
			  
			  $data = $this->user->getPendingEventsByPatient($patid);
			  $ct=$data['count'];
			 for($i=0;$i<=$ct;$i++)
			 {
					 @$mst=$_POST['status_'.$i];
					 if($mst!=-1)
					 {
						 if($mst==0 or $mst==1)
						 {
				 @$sta=$_POST['status_'.$i];
				 @$eid=$_POST['event_id_'.$i];
				 
		//$this->db->query("UPDATE notifications SET status='".$sta."' WHERE note_id='".$eid."'");
				@ $this->user->group_edit_notification($sta,$eid,$patid,$cid1,$name,$pname);
				 
				 
						 }
					 }
				 
			 }
			 
			  echo json_encode(array("result" =>"success"), 200);
			 
			 
				 }
           
            
                     
                     
       }	
	
	
	/*function updateAllEventsByCaregiver()
	{
	
            
           extract($_POST);
           $result = array();
           $i=0; 
         $total = count($status);
           foreach($event_id as $key=>&$val){ // Loop though one array
                      
                      if($status[$key] != -1){                      
                        $result[$key] =  array(0=>$val,1=>$status[$key]); 
                        $this->db->query("UPDATE notifications SET status='".$status[$key]."' WHERE note_id='".$val."'");
                        $i++;
                      }
                    }
                    
                if($total > $i){
                  $remaining = $total-$i;
                
                    ?>
                 <h3><?php echo $i." Notifications updated you have ".$remaining." remaing";?></h3>
                <button id="pending_event_view" class="btn-success" onclick=" return view_pending_events();">View</button>
               <button  id="pending_event_cancel" onclick=" return cancel_pending_events();">Cancel</button>
                <?php
                }else{?>
                     <h3><?php echo $i." Notifications updated successfully";?></h3>
               <?php }  
                
                     
                     
       
	}*/
	
		function delete_diets()
		{
    
			if(isset($_POST['diet_id'])){
				$eventid=3;
				$id=$_POST['diet_id'];
				$pid=$_POST["patient_id"];//$this->session->userdata('pid');
				$data = $this->user->getPendingmoredetails($pid,$eventid);
				 $ds="update  diets set is_delete='1' where id='$id'";
				$de=mysql_query($ds) or die(mysql_error());
			} else if(isset($_POST['param_id'])){
				
				$eventid=1;
				$id=$_POST['param_id'];
				$pid=$_POST["patient_id"];//$this->session->userdata('pid');
				$data = $this->user->getPendingmoredetails($pid,$eventid);
				 $ds="update  parameter_name set is_delete='1' where id='$id'";
				$de=mysql_query($ds) or die(mysql_error());
			} else if(isset($_POST['med_id'])){
				
				$eventid=2;
				$id=$_POST['med_id'];
				$pid=$_POST["patient_id"];//$this->session->userdata('pid');
				$data = $this->user->getPendingmoredetails($pid,$eventid);
				 $ds="update  medicine set is_delete='1' where med_id='$id'";
				$de=mysql_query($ds) or die(mysql_error());
			} else if(isset($_POST['act_id'])){
				
				$eventid=4;
				$id=$_POST['act_id'];
				$pid=$_POST["patient_id"];//$this->session->userdata('pid');
				$data = $this->user->getPendingmoredetails($pid,$eventid);
				 $ds="update  activities set is_delete='1' where act_id='$id'";
				$de=mysql_query($ds) or die(mysql_error());
			}
			
			
			
		
		if(count($data))
		{
			foreach($data as $pendingEvent)
			{
				$nid = $pendingEvent['note_id'];
				$subid=$pendingEvent['sub_event_id'];
				$gmt=$pendingEvent['gmt_time'];
				$milliseconds = round(microtime(true) * 1000);
    
				if($gmt>$milliseconds)
				{
					$ns="delete from notifications where note_id='$nid'";
					$ne=mysql_query($ns) or die(mysql_error());
				}
			}
		}
  
		echo json_encode(array("result" =>"success"), 200); exit;
		//redirect('/caregiver/show_diets/'.$pid);
	}

	////////////////////////////
	
	function insert_agenda(){
		
		//print_r($_POST); exit;
		
		$pid = $_POST['user_id'];
		$title=$_POST['title'];
		$details=$_POST['details'];
		$time=$_POST['time'];
		
		$date=$_POST['date'];
		
		$data = $this->user->insert_agenda($title,$details,$time,$date,$pid);
		 
		if($data){
		echo  json_encode(array("result" =>"success"), 200); exit;	
		} else {
			echo json_encode(array("result" =>"unsuccessful"), 204); exit;
		}
		
	}
	
	function get_agenda(){
		
		$pid = $_POST['user_id'];
		$date=date("Y-m-d");
		$data = $this->user->getagenda1($pid);
		if($data){
		echo  json_encode(array("result" =>"success", "agendas"=>$data), 200); exit;	
		} else {
			echo json_encode(array("result" =>"unsuccessful"), 204); exit;
		}
		
	}
	
	
	function get_agenda_info(){
		
		$pid = $_POST['user_id'];
		$agenda_id=  $_POST['agenda_id'];
		$date=date("Y-m-d");
		$data = $this->user->getagenda_info($pid,$agenda_id);
		echo  json_encode(array("result" =>"success", "agendas"=>$data), 200); exit;	
		
	}
	
	
	function update_agenda(){
		
		//print_r($_POST); exit;
		$agenda_id=  $_POST['agenda_id'];
		$pid = $_POST['user_id'];
		$title=$_POST['title'];
		$details=$_POST['details'];
		$time=$_POST['time'];
		
		$date=$_POST['date'];
		
		$this->db->query("UPDATE agenda SET title='$title',details='$details',time='$time',date='$date' where prof_id='$pid' AND id='$agenda_id'");
		 
		
		echo  json_encode(array("result" =>"success"), 200); exit;	
		
		
	}
	
	
	function delete_agenda_info(){
		
		$pid = $_POST['user_id'];
		$agenda_id=  $_POST['agenda_id'];
		$date=date("Y-m-d");
		
		$this->db->query("DELETE FROM agenda where prof_id='$pid' AND id='$agenda_id'");
		
		echo  json_encode(array("result" =>"success"), 200); exit;	
		
	}
	
	
	
	function Send_Consultation_Request()
	{
		//echo $_POST['user_email']; exit;
		
		$row = $this->user->get_caregiver1($_POST['user_email']);
		
		$pid=$row['pat_id'];
		$careid=$_POST['user_id'];
		$subject=$_POST['subject'];
		$message=$_POST['message'];
		$sendid=$careid;
		$recid=$_POST['professional_id'];//$prid;
		$patid=$pid;
		@$lat= constant("lat");
		@$lon= constant("lon");
		$ip=$_SERVER['REMOTE_ADDR'];
		$date = date('Y-m-d H:i:s');
		$name = $_POST['user_disp_name'];

		$cs="insert into cons_request_messages (sender,recipient,message,date,subject,ip,patid,lat,lon) values('$sendid','$recid','$message','$date','$subject','$ip','$patid','$lat','$lon') ";
		$ce=mysql_query($cs) or die(mysql_error());
		
		$lid=mysql_insert_id();

		$nmsg="Consultation request from ".$name;
			
			$rid = $recid;
		$res="professional";
		
		$ds="select * from device_tocken where userid='$rid' and user_type='$res'";
		$se=mysql_query($ds) or die(mysql_error());
		
		
		$ntype="caregiver_consultation";
		while($sr=mysql_fetch_array($se))
		{
			$device_platform = $sr['platform'];
			$device=$sr['tockenid'];
			$msg = array(
			'message'       => $message,
			'title'         => "Consultation request from ".$name,
			//'subtitle'      => 'Chat Mssage',
			'call_type'     =>"caregiver_consultation",
			'lid'			=> $lid,
			'vibrate'       => 1,
			'sound'         => 1
			 );
			 $this->sendPushNotificationToGCM_sendconsultation($device,$msg,$device_platform,$name,$careid,$recid,$ntype,$lid,$nmsg);
			
		}
		$res="professional";
		$ntype="caregiver_consultation";
		$nip=$_SERVER['REMOTE_ADDR'];
		$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype)  values('$nmsg','$sendid','$recid','$lid','0','$ntype','$ip','$res')";
		$nte=mysql_query($nts) or die(mysql_error());


		if($ce)
		{
			echo json_encode(array("result" =>"success","message"=>"Request Sent Successfully"), 200); 
		}

	}
	
	
	function sendPushNotificationToGCM_sendconsultation($deviceid,$msg1,$device_platform,$name,$from,$to,$loginusertye,$lid,$nmsg) {

		$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

			//echo $deviceid; exit;
			//print_r()
			
			//echo $device_platform; exit;
		if($device_platform=="Android"){
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
//echo $result;  exit;
	//	echo json_encode(array("result" =>"success"), 200); //exit;
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
			$message = $nmsg; //"Consultation request from ".$name;
			
			$path = base_url().'assets/KoalaProPushCert.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
			$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'sid'           =>$from,
				'rid'           =>$to,
				'sname'         =>$name,
				'recivertype'   =>$loginusertye,
				'lid' => $lid,
				
				'call_type'     =>"caregiver_consultation"
				
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				// echo json_encode(array("result" =>"success"), 200); exit;
		}
		
		//echo $result;
		echo json_encode(array("result" =>"success"), 200); 
    }
	
	
	function getconsultaion_new(){
		$recid=$_POST['user_id'];
		$res = $this->user->getconsultaion_new($recid);
		if($res){
		echo json_encode(array("result" =>"success","consultaions"=>$res), 200); 
		} else {
			echo json_encode(array("result" =>""), 204); 
		}
	}
	
	function show_Consultation_Request(){
		$id = $_POST['nm_id'];
		$res = $this->user->show_Consultation_Request($id);
		echo json_encode(array("result" =>"success","consultaions"=>$res), 200); 
	}
	
	
	
	function consultation_replay()
	{
		$subject=$_POST['subject'];
		$name = $_POST['user_disp_name'];
		$rid=$_POST['receiver'];
		$sid=$_POST['user_id'];
		$message=$_POST['message'];
		$when = time();
		  $user_type = $_POST['user_type']; 
		$date = date('Y-m-d H:i:s');
		//$uid=$session_id;
		$stype=1;
		$read=0;

		
		$d = "INSERT INTO cons_request_messages SET sender=".$sid.",
													recipient='".$rid."',
													subject='".$subject."',
													message='".$message."',
													date='".$date."',
													stype='1',
													cons_request_messages.read='0'";
		//$d="insert into cons_request_messages(sender,recipient,subject,message,date,stype,read) values('$sid','$rid','$subject','$msg','$date',".$stype.",".$read.")";
  
		$e=mysql_query($d) or die(mysql_error());

		$lid=mysql_insert_id();

		$nmsg="Consultation reply from ".$name;//$name."  Had Replay To The Your Consultation Request ";
  
	//echo $_POST['user_type']; exit;
		if($_POST['user_type']=="professional"){
		$res="caregiver";
		} else { $res="professional"; }
		
		//echo $_POST['user_type']."--".$res; exit;
		
		$ds="select * from device_tocken where userid='$rid' and user_type='$res'";
		$se=mysql_query($ds) or die(mysql_error());
		
		$ntype="caregiver_consultation";
		while($sr=mysql_fetch_array($se))
		{
			$device_platform = $sr['platform'];
			$device=$sr['tockenid'];
			$msg = array(
			'message'       => $message,
			'title'         => "Consultation reply from ".$name,
			//'subtitle'      => 'Chat Mssage',
			'call_type'     =>"caregiver_consultation",
			'lid'			=> $lid,
			'vibrate'       => 1,
			'sound'         => 1
			 );
			 $this->sendPushNotificationToGCM_sendconsultation($device,$msg,$device_platform,$name,$sid,$rid,$ntype,$lid,$nmsg);
			
		}
		
		
		
		
		$ntype="caregiver_consultation";
		$nip=$_SERVER['REMOTE_ADDR'];
		
		
		$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype) values('$nmsg','$sid','$rid','$lid','0','$ntype','$nip','$res')";
		$nte=mysql_query($nts) or die(mysql_error());
		
		if($nte)
		{
		 echo json_encode(array("result" =>"success","message"=>"Reply Sent Successfully"), 200); 
		}
	}
	
	
	function check_licence(){
		
		$user_id=$_POST['user_id'];
		$licence=$_POST['licence'];
		//echo "SELECT * FROM owner_licence WHERE own_id=".$user_id." AND licence_no='".$licence."' AND licencetype='Extended'"; exit;
		 $query = $this->db->query("SELECT * FROM `owner_licence` WHERE own_id=".$user_id." AND licence_no='".$licence."' AND licencetype='Extended'");
		 //$row = $query->result(); 
		 $count = $query->num_rows();
		//echo $count; exit;
		 if($count>0){
			echo json_encode(array("result" =>""), 200); 
		 } else {
			echo json_encode(array("result" =>"success"), 204); 
		 }
	}
	
	function professional_update(){
		$user_id=$_POST['user_id'];
		$name=$_POST['name'];
		$surname=$_POST['surname'];
		$age=$_POST['age'];
		$gender=$_POST['gender'];
		$address=$_POST['address'];
		$city=$_POST['city'];
		$zipcode=$_POST['zipcode'];
		$state=$_POST['state'];
		$lat=$_POST['lat'];
		$lng=$_POST['lng'];
		$qualification=$_POST['qualification'];
		$admitted_to=$_POST['admitted_to'];
		$demo2=$_POST['demo2'];
		$image_name=$_POST['image_name'];
		
		
		@$qry = "UPDATE professionals SET name='".$name."',
											surname='".$surname."',
											age='".$age."',
											gender='".$gender."',
											address='".$address."',
											city='".$city."',
											state='".$state."',
											zipcode='".$zipcode."',
											lat='".$lat."',
											lon='".$lng."',
											qualification='".$qualification."',
											doctor_board_admitted_to='".mysql_escape_string($admitted_to)."',
											professionals.from='".$demo2."',
											photo='".$image_name."' where id=".$user_id."";
		//exit;
		 $res = mysql_query($qry) or die(mysql_error());
		
		
		if($res){
			echo json_encode(array("result" =>"success"), 200); 
		 } else {
			 echo json_encode(array("result" =>""), 204); 
		 }
		
	}
	
	
	function delete_notification()
	{
		$nid = $_POST['note_id'];
		$ds="delete from notifications where note_id='$nid'";
		$de=mysql_query($ds) or die(mysql_error());
		echo json_encode(array("result" =>"success"), 200); 
	
	}
	
	function get_associate_caregivers(){
		
		
		$pid = $_POST['patient_id'];
		 $cid1 = $_POST['user_id']; 
		$caregivers= $this->user->getcaregivers_new($pid,$cid1);
		echo  json_encode(array("result" =>"success", "caregivers_list"=>$caregivers), 200); //exit;	
	}
	
} //Class Closed
?>