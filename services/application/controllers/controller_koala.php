<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Controller_koala extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		
		header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
		header('Access-Control-Allow-Origin: *');  
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		
		 $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		$this->load->model('user');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('email');
		
		/*$ip = $_SERVER['REMOTE_ADDR']; 
 
		// the IP address to query
		@$query = unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
		//echo @$lat=$query['lat'];
		//@$lon=$query['lon'];
		$timezone=$query['timezone'];
		//$ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
		date_default_timezone_set("$timezone");*/
		
		
		
	}

	function index()
	{
		$this->load->helper('url');
		//$this->load->view('welcome_message');
		//print_r($_POST);
	}
	
	public function login_post()
	{
				
		if(isset($_POST['submit']))
		{
			$username=$this->input->post('username');
			$pass=$this->input->post('password');
			$password=md5($this->input->post('password'));
			$selectype=$this->input->post('selectype');
			@$_REQUEST['remember'];
			
			if(@$_REQUEST['remember'] == "on") {    // if user check the remember me checkbox        
				setcookie('remember_me', $username, time()+60*60*24*100, "/");
				setcookie('remember_pass', $pass, time()+60*60*24*100, "/");
			}
			else {   // if user not check the remember me checkbox
				setcookie('remember_me', 'gone', time()-60*60*24*100, "/"); 
				setcookie('remember_pass', 'gone', time()-60*60*24*100, "/");
			}
		}
		
		
		if(!empty($username) && !empty($password) && $selectype=="owner")
		{ 
			$use=$this->user->get_users_name($username);
			
			
			if(!empty($use)){
				$status=$use['own_status'];
				$type=$use['l_type'];
				$owner_id=$use['own_id'];
				$owner_licence=$this->user->get_users_lice($owner_id);
				
				if($status=="active")
				{
					$result=$this->user->ownlogin($username,$password);
					
				}
		 
				if($status=="active" and $result>0 and $type!="RSA" and $type!="Consortium")
				{
					//$this->session->set_userdata('owner_name', $username);
					//$this->data['flash']='';
					echo json_encode(array("result" =>"success", "user_info"=>$use), 200);
				  //echo json_encode(array('result' => 'success','status'=>1), 200);
				  //redirect('/front/after_owner_login/'.$owner_id, 'refresh');
				}
		   
				if($status=="active" and $result>0 and ($type=="RSA"))
				{
					$this->session->set_userdata('owner_name', $username);
					$this->data['flash']='';
					//redirect('/front/after_consortiam_login/'.$owner_id, 'refresh'); 
					//echo json_encode(array('result' => 'success','status'=>1), 200);
				echo json_encode(array("result" =>"success","user_info"=>$use), 200);
				}
  
				if($status=="active" and $result>0 and ($type=="Consortium"))
				{
					$this->session->set_userdata('owner_name', $username);
					$this->data['flash']='';
				  //redirect('/front/after_consortiam_login/'.$owner_id, 'refresh'); 
				  // echo json_encode(array('result' => 'success','status'=>1), 200);
			echo json_encode(array("result" =>"success","user_info"=>$use), 200);
				}
  
				if($status=="inactive")
				{
					echo json_encode(array('result' =>'' ,'status'=>0), 204);	
					//$this->data['flash']='User status is in active !'; 
				}
		  
				if(@$result<=0)
				{
					echo json_encode(array('result' =>'' ,'status'=>0), 204);	
					//$this->data['flash']='Username and password does not match !'; 
				}
		
			} else {
			
				echo json_encode(array('result' =>'' ,'status'=>0), 204);	
			}
		}
 
		if(!empty($username) and !empty($password) and $selectype=="caregiver")
		{
			$use=$this->user->get_caregiver_tologin($username,$password);
	  
			if($use<=0)
			{
				//$this->data['flash']='Please enter correct username or password of caregiver!';
				//echo json_encode(array('result' =>'' ,'status'=>0), 204);	
				echo json_encode(array('result' =>'' ,'status'=>0), 204);
			}
			else
			{
				$result= $this->session->set_userdata('owner_care', $username);
				 $email=$this->session->userdata('owner_care');
					$response_data= $this->user->caregiver_details($email);
					
					echo json_encode(array("result" =>"success","user_info"=>$response_data), 200);
					
					//echo '{"user_info":'. json_encode(array("result" =>"success"),200) .'}'; 
					
				//echo '{"user_info":'. json_encode($response_data,200) .'}'; 
				
				//echo json_encode($response_data,200); 
				
				//echo json_encode(array('result' => $response_data), 200);
				
				//redirect('/caregiver/search/', 'refresh');
			}
		}
	
		if(!empty($username) and !empty($password) and $selectype=="professional")
		{
			
		
			
			 $use=$this->user->get_profdetails($username,$password);
				
			//echo json_encode(array('result' =>$username ,'status'=>$password), 204);	exit;
			if($use<=0)
			{
				//$this->data['flash']='Please enter correct username or password of professional!';
				echo json_encode(array('result' =>'' ,'status'=>1), 204);	
	  
			}
			else
			{
				$response_data= $this->user->getprof($username);
					
					
				echo json_encode(array('result' => 'success',"user_info"=>$response_data), 200);
			}
		}
	} //login function closed
	
	
	
	function owner_register_step1()
	{
		
	  $this->form_validation->set_rules('username', 'Email', 'trim|required|xss_clean|valid_email');
	  if ($this->form_validation->run()==TRUE) {
	  
		  @$b=$_POST['role'];
		
		 if($b=="Owner")
		 {
				echo json_encode(array("result" =>"success","role"=>"owner"), 200);
		
		 }
		 else
		 {
		 echo json_encode(array("result" =>"success","role"=>"professional"), 200);
	
		 }
	  }				
	 else
	 {
		echo json_encode(array("result" =>""), 204);
	 }
	}//Registration step 1 done
	
	////////////REgistration Setp -2 //////////////
	
	function getowners()
		{   
		  
		$num=$this->user->get_ajax_owner($_POST['username'],$_POST['type']);
		 echo json_encode(array("count" =>$num), 200);
		  
	}
	
	function ofterrsa_register()
	{
	
	
		 $email=$_POST['username'];
		 $password=md5($_POST['passwordvalue']);
		 //echo json_encode(array("result" =>$email.$password), 200);exit;
		 if(!empty($email) and !empty($password))
		 {
		 $ups="update owner_table set own_passwd='$password' where owner_email='$email'"; 
		 $upe=mysql_query($ups) or die(mysql_error());
		 
			 if($upe){
				  echo json_encode(array("result" =>"success"), 200);
			 } else {
				 echo json_encode(array("result" =>""), 204);
			 }
		 
		 } else {
			 echo json_encode(array("result" =>""), 204);
			 
		 }
	}
	
	
	////////////REgistration Setp -3{Consortium} //////////////
	
	
	// owner payment and registration is done here 
	function register_own_rsa_step3()
	{   
		$own_licence=$_POST['licence'];
		$ltype=$_POST['ltype'];
		$username=$_POST['username'];
		$password=$_POST['passwordvalue'];
		$uname = $_POST['uname'];
		if(!empty($username) and !empty($password))
		{
			if($ltype=="Consortium")
			{
				$rs="select * from owner_con_licence where licence_no='$own_licence' and otype='$ltype' ";
				$re=mysql_query($rs) or die(mysql_error());
				$rn=mysql_num_rows($re);
				if($rn>0)
				{
					@$lat= substr(constant("lat"),0,9);
					@$lon= substr(constant("lon"),0,9);
					$ms="select * from owner_licence where licence_no = '".$own_licence."'";
				   //$ms="select * from owner_table where owner_email='$username'";
				   $me=mysql_query($ms) or die(mysql_error());
				   $mn=mysql_num_rows($me);
				   if($mn<=0)
				   {
						$id=$this->user->add_session_con_owner($lat,$lon,$username,$ltype,$password,$ltype,$uname);
						$mexe=$this->user->in_owner_lice1($id,$own_licence,$username,$ltype);
	   
						@$to = $username;
						@$subject = "Koala Login Info";
						@$message = "
						<html>
						<head>
						<title>HTML email</title>
						</head>
						<body>
						<table>
						<tr>
						<th> Name: </th>
						<td>{$uname}</td>
						</tr>
						<tr>
						<th> Subject: </th>
						<td>{Your Login Details }</td>
						</tr> 
						<tr>
						<th> User Name :</th>
						<td>{$username}</td>
						</tr>
						<tr>
						<th> Password :</th>
						<td>{$password}</td>
						</tr>
						</table>
						</body>
						</html>
						";
						$headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
						// More headers
						$headers .= 'From: '." Koala Team " . "\r\n";
					
						if(mail($to,$subject,$message,$headers)){
						 // echo 'Thank you !';
						}else{
						  //echo 'Try again !'; 
						}
						//$this->data['message']="Owner Registered Successfully With Licence id";
						//$this->data['licence']=$own_licence;
						echo json_encode(array("result" =>"success","message"=>"Owner Registered Successfully With Licence id","licence"=>$own_licence), 200);
					}
					else
					{
						//$this->data['message']="<font color='red'>Owner  Already Registered Please Login</font>";
						//$this->data['licence']='';
						echo json_encode(array("result" =>"","message"=>"Owner  Already Registered Please Login"), 204);
					}
		
					//$this->load->view('front/owner_success1', $this->data);
				}
		
				if($rn<=0)
				{
					//$this->data['flash']= "Licence Entered Incorrect please contact to koala ";
					//$this->load->view('front/after_rsa_con_licence', $this->data);
					echo json_encode(array("result" =>"","message"=>"Licence Entered Incorrect please contact to koala"), 204);
				}
			}
			else
			{
				//$this->data['flash']= "Licence Entered Incorrect please contact to koala ";
				//$this->load->view('front/after_rsa_con_licence', $this->data);
				echo json_encode(array("result" =>"","message"=>"Licence Entered Incorrect please contact to koala "), 204);
			}
		}
		else
		{
			/*//echo "<script>alert('Un Successfully Please Try Again please enter the all fields ')</script>";*/
			//redirect('/auth/owner_register_step1/', 'refresh');
			echo json_encode(array("result" =>"","message"=>"Un Successfully Please Try Again please enter the all fields"), 204);
		}
	}
	
	
	
	////////////REgistration Setp -3{Freelancer} //////////////
	
	function owner_email_exists(){
		$email=$_POST['username'];
		$use=$this->user->get_owner1($email); 
		
		if($use<=0){
		
			echo json_encode(array("result" =>"success","message" =>"No Result Found"), 200);
			
		} else {
		
			echo json_encode(array("result" =>"","owner_info" =>$use), 204);
		}
	}
	
	function paymentsuccess()
	{
		@$lat= substr(constant("lat"),0,9);
		@$lon= substr(constant("lon"),0,9);
		$email=$_POST['username'];
		$ltype=$_POST['ltype'];
		$lice_type = $_POST['licence'];
		$username=$_POST['username'];
		$password=$_POST['passwordvalue'];
		$uname = $_POST['uname'];
		$role = $_POST['role'];
		if(!empty($email))
		{
			$use=$this->user->get_owner1($email); 
			if($use<=0)
			{
				$id=$this->user->add_session_user123($lat,$lon,$username,$ltype,$password,$lice_type,$uname,$role);
		
				$licence="koala".$id;
				$this->user->in_owner_lice($id,$username,$ltype);
				$item_transaction =$_REQUEST['pay_id'];

				$uid=$this->user->updatetransation($id,$item_transaction);	
	 
	 
				if(!empty($id))
				{
				
					@$to = $username;
					@$to_name =$uname;
					@$subject = 'Koala Owner Licence';//$_POST['subject'];
					@$body = "
					<html>
					<head>
					<title>HTML email</title>
					</head>
					<body>
					<table>
					<tr>
					<th> Name: </th>
					<td>$uname</td>
					</tr>
					<tr>
					<th> Subject: </th>
					<td>Your Login Details</td>
					</tr> 
					<tr>
					<th> User Name :</th>
					<td>$username</td>
					</tr>
					<tr>
					<th> Password :</th>
					<td>$password</td>
					</tr>
					<tr>
					<th> Licence ID </th>
					<td> $licence  </td>
					</tr>
					</table>
					</body>
					</html>
					";
					
		
					
					$res = $this->mail_function($to,$to_name,$subject,$body);
					
					if($res){
						echo json_encode(array("result" =>"success","message"=>"Owner Registered Successfully With Licence id","licence"=>$licence), 200);
					}
				}
		
			} else {
	  
					echo json_encode(array("result" =>"","message"=>"Email Already Exists"), 204);
	  
			}
		}
	
	}
	
	////////////////REg_Step-3 {Profissional}////////////
	function prof_email_exists(){
		$email=$_POST['username'];
		
		$ps="select * from professionals where email='$email'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pn=mysql_num_rows($pe);
		if($pn<=0){
		
			echo json_encode(array("result" =>"success","message" =>"No Result Found"), 200);
			
		} else {
		
			echo json_encode(array("result" =>"","owner_info" =>$pn), 204);
		}
	}
	
	
	function profestion_res_success()
	{ //role:role,ltype:ltype,username:username,passwordvalue:passwordvalue,uname:uname,surname:surname,age:age,gender:gender,address:address,city:city,zipcode:zipcode,state:state,lat:lat,lng:lng,qualification:qualification,admitted_to:admitted_to,from:from
		
		$lat= $_POST['lat'];//$_POST[''];
		$lng= $_POST['lng'];
		$role = $_POST['role'];
		$ltype = $_POST['ltype'];
		$username = $_POST['username'];
		$passwordvalue = $_POST['passwordvalue'];
		$uname = $_POST['uname'];
		$surname = $_POST['surname'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$zipcode = $_POST['zipcode'];
		$state = $_POST['state'];
		$qualification = $_POST['qualification'];
		$admitted_to = $_POST['admitted_to'];
		$from = $_POST['from'];
		$photo = $_POST['photo'];
		
		$trans_no = $_POST['pay_id'];
		$pay_amount = 50;
		$pay_status = "Success";
		$pay_date = date("Y-m-d");

		$ps="select * from professionals where email='$username'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pn=mysql_num_rows($pe);
		if($pn<=0)
		{

		//echo json_encode(array("result" =>"","message"=>$_POST['photo']), 204); exit;
		$pid=$this->user->add_profetional($lat,$lng,$role,$ltype,$username,$passwordvalue,$uname,$surname,$age,$gender,$address,$city,$zipcode,$state,$qualification,$admitted_to,$from,$photo,$trans_no,$pay_amount,$pay_status,$pay_date);
		

		

		$item_transaction = mt_rand(5, 15);//$_REQUEST['txn_id'];

		$uid=$this->user->update_prof_transation($pid,$item_transaction);	
		
		
			$to = $username;
			$to_name = $uname;
			$subject = "Koala Registration success as Professional.";
		
		$body = "
		   <html>
		   <head>
		   <title>HTML email</title>
		   </head>
		   <body>
		   <table>
		   <tr>
		   <th> Username: </th>
		   <td>".$username."</td>
		   </tr>
		   <tr>
		   <th> Password  </th>
		   <td>".$passwordvalue."</td>
		   </tr> 
		   </table>
		   </body>
		   </html>
		   ";
			$res = $this->mail_function($to,$to_name,$subject,$body);
			if($res) echo json_encode(array("result" =>"success","message"=>"Registration Successful"), 200);
		}  else {
			
			echo json_encode(array("result" =>"","message"=>"Email Already Exists"), 204);

		}
		

		
	}
	
	
	//////////////////Forget Password/////////////////
	
	
	function forgetpassword()
	{
		if($_POST)
		{
		  $role=$_POST['role']; 
		  if($role=="Owner" || $role=="owner" )
		  {
			  $licence=$_POST['licence'];
			  $email=$_POST['username'];
			   
			  $os="select * from owner_table where owner_type='$licence' and owner_email='$email'";
			  $oe=mysql_query($os) or die(mysql_error());
			  $on=mysql_num_rows($oe);
			  if($on>0)
			  {
				   $password=$_POST['passwordvalue'];
				   $pass=md5($password);
				   $us="update owner_table set own_passwd='$pass' where owner_email='$email'";
				   $ue=mysql_query($us) or die(mysql_error());
				   if($ue)
				   {
					   /*
					   $mail = new PHPMailer();
					   $mail->IsSMTP();
					   $mail->SMTPAuth   = true; 
					   $mail->SMTPSecure = "smtp"; 
					   $mail->Host       = "smtpout.secureserver.net";
					   $mail->Username   = "ali.m@bravemount.com";
					   $mail->Password   = "Wings@100";
					   $mail->SetFrom('ali.m@bravemount.com', 'Koala Team'); //from (verified email address)
					   $mail->Subject = "Forgot Password Details"; //subject      
						  @$to = $email;
					   @$subject ="Forgot Password Details";
					   @$message ="
					   <html>
					   <head>
					   <title>HTML email</title>
					   </head>
					   <body>
					   <table>
					   <th> Subject: </th>
					   <td>{Your Login Details }</td>
					   </tr> 
					   <tr>
					   <th> User Name :</th>
					   <td>{$email}</td>
					   </tr>
					   <tr>
					   <th> Password :</th>
					   <td>{$password}</td>
					   </tr>
					   </table>
					   </body>
					   </html>
					   ";
					   
					  
					   
							   @$body = eregi_replace("[\]",'',$message);
							   $mail->MsgHTML($body);   
						 $to = $email;
					   //$subject = "Caregiver Registration Details";
					   $mail->AddAddress($to, "Koala"); */
					   
					   
					   //if (!$mail->Send()) 
						//{
										echo json_encode(array("result" =>"success","message"=>"Password Change Successfully"), 200);
									//$this->data['flash']= " Password Change Successfully ";
						//}
					}
					else
					{
						//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
						echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
					}
				}
				else
				{
					//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
					echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
				}
  
   
			}
  
			if($role=="professional")
			{
			   $email=$_POST['username'];
			   $ps="select * from professionals where email='$email'";
			   $pe=mysql_query($ps) or die(mysql_error());
			   echo $pn=mysql_num_rows($pe);
			   if($pn>0)
			   {
	
				   $password=$_POST['passwordvalue'];
				   $pass=md5($password);
				   $ups="update professionals set password='$pass' where email='$email'";
				   $upe=mysql_query($ups);
				   if($upe)
				   {
					   /*
					   $mail = new PHPMailer();
					   $mail->IsSMTP();
					   $mail->SMTPAuth   = true; 
					   $mail->SMTPSecure = "smtp"; 
					   $mail->Host       = "smtpout.secureserver.net";
					   $mail->Username   = "ali.m@bravemount.com";
					   $mail->Password   = "Wings@100";
					   $mail->SetFrom('ali.m@bravemount.com', 'Koala Team'); //from (verified email address)
					   $mail->Subject = "Forgot Password Details"; //subject      
						  @$to = $email;
					   @$subject ="Forgot Password Details";
					   @$message ="
					   <html>
					   <head>
					   <title>HTML email</title>
					   </head>
					   <body>
					   <table>
					   <th> Subject: </th>
					   <td>{Your Login Details }</td>
					   </tr> 
					   <tr>
					   <th> User Name :</th>
					   <td>{$email}</td>
					   </tr>
					   <tr>
					   <th> Password :</th>
					   <td>{$password}</td>
					   </tr>
					   </table>
					   </body>
					   </html>
					   ";
					   $headers = "MIME-Version: 1.0" . "\r\n";
					   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
					   // More headers
					   $headers .= 'From: '." Koala Team " . "\r\n";
					  
					   
							   @$body = eregi_replace("[\]",'',$message);
							   $mail->MsgHTML($body);   
						 $to = $email;
					   //$subject = "Caregiver Registration Details";
					   $mail->AddAddress($to, "Koala"); 
					   */
					   
					   //if (!$mail->Send()) 
					  // {
									//$this->data['flash']= " Password Change Successfully ";
									echo json_encode(array("result" =>"success","message"=>"Password Change Successfully"), 200);
						//}
					}
					else
					{
						//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
						echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
					}
				}
				else
				{
					//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
					echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
				}
			}
  
			if($role=="caregiver")
			{
			   $email=$_POST['username'];
			   $ps="select * from caregiver where email='$email'";
			   $pe=mysql_query($ps) or die(mysql_error());
			   $pn=mysql_num_rows($pe);
			   
			   if($pn>0)
			   {
					$password=$_POST['passwordvalue'];
					$pass=md5($password);
					$ups="update caregiver set passwd='$pass' where email='$email'";
					$upe=mysql_query($ups) or die(mysql_error());
					if($upe)
					{
   
					  /* $mail = new PHPMailer();
					   $mail->IsSMTP();
					   $mail->SMTPAuth   = true; 
					   $mail->SMTPSecure = "smtp"; 
					   $mail->Host       = "smtpout.secureserver.net";
					   $mail->Username   = "ali.m@bravemount.com";
					   $mail->Password   = "Wings@100";
					   $mail->SetFrom('ali.m@bravemount.com', 'Koala Team'); //from (verified email address)
					   $mail->Subject = "Forgot Password Details"; //subject      
						  @$to = $email;
					   @$subject ="Forgot Password Details";
					   @$message ="
					   <html>
					   <head>
					   <title>HTML email</title>
					   </head>
					   <body>
					   <table>
					   <th> Subject: </th>
					   <td>{Your Login Details }</td>
					   </tr> 
					   <tr>
					   <th> User Name :</th>
					   <td>{$email}</td>
					   </tr>
					   <tr>
					   <th> Password :</th>
					   <td>{$password}</td>
					   </tr>
					   </table>
					   </body>
					   </html>
					   ";
							   @$body = eregi_replace("[\]",'',$message);
							   $mail->MsgHTML($body);   
						 $to = $email;
					   //$subject = "Caregiver Registration Details";
					   $mail->AddAddress($to, "Koala"); 
					   */
					   
					  // if (!$mail->Send()) 
					  // {
									//$this->data['flash']= " Password Change Successfully ";
									echo json_encode(array("result" =>"success","message"=>"Password Change Successfully"), 200);
							//	}
					   
					}
					else
					{
						//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
						echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
					}
				}
				else
				{
					//$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
					echo json_encode(array("result" =>"","message"=>"Plese Enter The Correct Email Address To Get The Passowrd"), 204);
				}
			}
  
		}
	}
	
	
	/*After Owner Login*/
	
	public function after_owner_login()
	{
		
		if($_POST['listof']=="patients"){ 
			$patients=$this->user->getpatients($_POST['user_id']);
			
			echo json_encode(array("result" =>"success","patients_list"=>$patients), 200);
		}
		if($_POST['listof']=="caregivers"){
			$caregivers=$this->user->getcaregiver($_POST['user_id']);
			
			echo json_encode(array("result" =>"success","caregivers_list"=>$caregivers), 200);
		}
				
	}
	
	
	public function patient_view(){
	
		$patient_info= $this->user->patient_details($_POST['patient_id']);
		echo json_encode(array("result" =>"success","patient_info"=>$patient_info), 200);
	}
	
	public function caregivers_view(){
		//echo json_encode(array("result" =>$_POST['caregiver_id']), 200); exit;
		$caregiver_info= $this->user->care_details($_POST['caregiver_id']);
		echo json_encode(array("result" =>"success","caregiver_info"=>$caregiver_info), 200);
	}
	
	
	//Upload Image
	

	 
	 public function get_caregiver_patients(){
		 
		$caregiver_info= $this->user->caregiver_patients($_POST['user_id']);
		
		
		if($caregiver_info<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
		
			echo json_encode(array("result" =>"success","patients_list" =>$caregiver_info), 200);
		}
		
		
		
	 }
	 
	 public function get_professionals(){
		
		  $professionals_info = $this->user->getProfessionals1($_POST['user_id']);
		 
		  echo json_encode(array("result" =>"success","professionals_info" =>$professionals_info), 200);
	 }
	 
	 
	 
	  public function get_professionals_all(){
		 
			$professionals_info = $this->user->getProfessionals_all($_POST['user_id']);
		 
		 if($professionals_info<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
		
			  echo json_encode(array("result" =>"success","professionals_info" =>$professionals_info), 200);
		}
		 
		 
		
	 }
	 
	 
	 
	 public function professional_view(){
	
		$professional_view_info= $this->user->getProfessionals_by_id($_POST['professional_id']);
		echo json_encode(array("result" =>"success","professional_info"=>$professional_view_info), 200);
	}
	
	
	
	
	 public function get_pro_patients(){
		 
		$pro_patients_info= $this->user->getPatients2($_POST['user_id']);
		echo json_encode(array("result" =>"success","patients_list" =>$pro_patients_info), 200);
	 }
	 
	  public function get_por_caregivers(){
		 
		$pro_caregivers_info= $this->user->getprofetionals123($_POST['user_id']);
		echo json_encode(array("result" =>"success","caregivers_list" =>$pro_caregivers_info), 200);
	 }
	
	
	//Owner patient update
	
	public function patient_update(){
	
		$patient_update = $this->user->updatepatient($_POST['patient_id']);
		echo json_encode(array("result" =>"success"), 200);
		
	}
	
	public function owner_patient_insert(){
		//
		$patn=$this->user->getpatient1($_POST['user_id'],$_POST['own_licence']);
		
			if($patn<=0)
			{
				$ms="select * from owner_licence where own_id=".$_POST['user_id']." and licence_no='".$_POST['own_licence']."'";
				$me=mysql_query($ms) or die(mysql_error());
				$mn=mysql_num_rows($me);
				if($mn>0)
				{
					$patient_id = $this->user->owner_insertpatient($_POST['user_id'],$_POST['own_licence']);
					echo json_encode(array("result" =>"success","patient_id"=>$patient_id ), 200);
				} else {
					echo json_encode(array("result" =>"", "message"=>"Licence does not exists."), 204);
				}
			} else {
				
				echo json_encode(array("result" =>"", "message"=>"Patient already exists with this licence."), 204);
			}
	}
	
	///////////Owner -Caregiver editing 
	
	public function caregiver_edit(){
		$caregiver_update = $this->user->updatecaregiversbycid($_POST['caregiver_id']);
		echo json_encode(array("result" =>"success"), 200);
		
	}
	

	//////Owner - Caregiver adding
	public function get_own_patients(){
		$get_own_patients = $this->user->get_patient_details($_POST['user_id']);
		echo json_encode(array("result" =>"success","get_patients"=>$get_own_patients), 200);
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////
	
	
	function addcaregivers()
	{
	   $email=$_POST['user_email'];
	   
	   
	   $id=$_POST['user_id'];
	  // $lice_type=$_POST['own_licence_type'];
	  
	  $lat=0;
	  $lon=0;
	 // echo json_encode(array("result" =>"success","get_patients"=>$_POST), 200); exit;
	 
	   if($_POST){
		 
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
			$mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
			$mail->Subject = "Caregiver Login Details"; //subject      

			$ar=$_POST['cname'];
			$snname=$_POST['sname'];
			$cemail=$_POST['cemail'];
			$ctype=$_POST['ctype'];
			@$patient=$_POST['patient'];
			@$owtype=$_POST['owtype'];

			$ct=count($ar);
		  	
			if(!empty($ar)  and !empty($cemail) and !empty($ctype) and !empty($patient))
			{ 
		
				$ctype;
				$patid=$patient;
				$this->data['patientlicence']=$this->user->get_patient_licence($patid);
				$liceid=$this->data['patientlicence'][0]->own_licence;
				@$this->data['plicencetype']=$this->user->get_patient_type($liceid);
				@$lice_type=$this->data['plicencetype'][0]->licencetype;
				@$pname=$this->data['plicencetype'][0]->name;
		
				$s1="select * from list_caregiver where care_type_id=1 and pat_id='$patid'";
				$e1=mysql_query($s1) or die(mysql_error());
				$n1=mysql_num_rows($e1);
				
				$s2="select * from list_caregiver where care_type_id!=1 and pat_id='$patid'";
				$e2=mysql_query($s2) or die(mysql_error());
				$n2=mysql_num_rows($e2);
	
		
				if($owtype=="yes")
				{
					$pus="update patients set owner_caretype='$ctype' where own_id='$id' and id='$patid'";
					$pue=mysql_query($pus) or die(mysql_error());
				}
		
		
		
		
				if($lice_type!="Extended" or empty($lice_type))
				{
						 $limit=5;
						$mlit=3;
				}
				if($lice_type=="Extended")
				{
						$limit=10;
						$mlit=8;
				}

				$mc="select * from list_caregiver where pat_id='$patid'";
				$me=mysql_query($mc) or die(mysql_error());
				$mn=mysql_num_rows($me);
		
				if($mn<$limit)
				{   
		 
					$mn=count($ctype);
					$cr="select * from caregiver where email='$cemail'";
					$ce=mysql_query($cr) or die(mysql_error());
					$mcr=mysql_fetch_array($ce);
					$cn=mysql_num_rows($ce);

					if($cn<=0)
					{
		
						if($n1<2 and $ctype==1)
						{ 
		
		
							$is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id,lat,lon) values('$ar','$snname','$cemail','$ctype','$patient','0','$id','$lat','$lon')";
		
							$ie=mysql_query($is) or die(mysql_error());
							$cid=mysql_insert_id();
		
							$mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$cid','$id','$ctype','$patient')";		
							$me=mysql_query($mc) or die(mysql_error());	
							
							if($me)
							{
								$body = "
								<html>
								<head>
								<title>HTML email</title>
								</head>
								<body>
								<table>
								<tr>
								<th>Name:</th>
								<td>$ar</td>
								</tr>
								<tr>
								<th>Email:</th>
								<td>$cemail</td>
								</tr>
								<tr>
								<th>lick the below link to activate your account </th>
								<td> <a href='".EMAIL_URL.'auth/careregister/'.$cid."'> click here </a> </td>
								</tr>
								</table>
								</body>
								</html>
								";

								@$body = eregi_replace("[\]",'',$body);
								$mail->MsgHTML($body);
								$to = $cemail;

								$mail->AddAddress($to, $ar); 


								if ($mail->Send()) { 
									//$this->data['msg']="Caregiver Added Successfully ";
									echo json_encode(array("result" =>"success","message"=>"Caregiver Added Successfully"), 200);
								} else { 
									// $this->data['msg']="Caregiver Added Successfully ";
									echo json_encode(array("result" =>"","message"=>"Mail Not Sent"), 204);
								} 
	
							}
						}
		
						if($n1==2 and $ctype==1)
						{
							//$this->data['msg']="You Have Entered More Than 2 Primary Caregivers With This Patient ";
							echo json_encode(array("result" =>"","message"=>"You Have Entered More Than 2 Primary Caregivers With This Patient"), 204);
						}
						$n2;
						if($n2<=$mlit and $ctype!=1)
						{ 
							$is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id,lat,lon) values('$ar','$snname','$cemail','$ctype','$patient','0','$id','$lat','$lon')";
							$ie=mysql_query($is) or die(mysql_error());
							$cid=mysql_insert_id();
		
							$mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$cid','$id','$ctype','$patient')";		
							$me=mysql_query($mc) or die(mysql_error());
		
							if($me)
							{

								$body = "
								<html>
								<head>
								<title>HTML email</title>
								</head>
								<body>
								<table>
								<tr>
								<th>Name:</th>
								<td>$ar</td>
								</tr>
								<tr>
								<th>Email:</th>
								<td>$cemail</td>
								</tr>
								<tr>
								<th>lick the below link to activate your account </th>
								<td> <a href='".EMAIL_URL.'auth/careregister/'.$cid."'> click here </a> </td>
								</tr>
								</table>
								</body>
								</html>
								";

								@$body = eregi_replace("[\]",'',$body);
								$mail->MsgHTML($body);
								$to = $cemail;

								$mail->AddAddress($to, $ar); 


								if ($mail->Send()) { 
									echo json_encode(array("result" =>"success","message"=>"Caregiver Added Successfully"), 200);
								} else { 
									echo json_encode(array("result" =>"","message"=>"Mail Not Sent"), 204);	 
								} 

							}
						}
					}
		
					if($cn>0)
					{
		
						$careid=$mcr['id'];
						$mcemail=$mcr['email'];		
						$mcs="select * from list_caregiver where careid='$careid' and own_id='$id' and pat_id='$patient'";$mce=mysql_query($mcs) or die(mysql_error()); 		
						$mcn=mysql_num_rows($mce);
						
						if($mcn<=0) 		
						{	
							if($n1<2 and $ctype==1)
							{ 
		
		
								$mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$careid','$id','$ctype','$patient')";		
								$me=mysql_query($mc) or die(mysql_error());	
		
								if($me)
								{
		
									$body = "
									<html>
									<head>
									<title>HTML email</title>
									</head>
									<body>
									<table>
									<tr>
									<th>Name:</th>
									<td>$ar</td>
									</tr>
									<tr>

									</tr>
									<tr>
									<th> you are assigned as $ctype to patient - $patient </th>

									</tr>
									</table>
									</body>
									</html>
									";

									@$body = eregi_replace("[\]",'',$body);
									$mail->MsgHTML($body);
									$to = $mcemail;

									$mail->AddAddress($to, "Caregiver Notification"); 
									if ($mail->Send()) { 
										echo json_encode(array("result" =>"success","message"=>"Caregiver Added Successfully"), 200);
									} else { 
										echo json_encode(array("result" =>"","message"=>"Mail Not Sent"), 204);	 
									} 
								}
							}
		
							if($n1==2 and $ctype==1)
							{
								$msg="You Have Entered More Than 2 Primary Caregivers ";
								echo json_encode(array("result" =>"","message"=>$msg), 204);	 
							}
							$n2;
							if($n2<=$mlit and $ctype!=1)
							{ 
								$mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$careid','$id','$ctype','$patient')";		
								$me=mysql_query($mc) or die(mysql_error());
								$body = "
								<html>
								<head>
								<title>HTML email</title>
								</head>
								<body>
								<table>
								<tr>
								<th>Name:</th>
								<td>$ar</td>
								</tr>
								<tr>

								</tr>
								<tr>
								<th> you are assigned as $ctype to patient - $patient </th>

								</tr>
								</table>
								</body>
								</html>
								";

							   @$body = eregi_replace("[\]",'',$body);
							   $mail->MsgHTML($body);
							   $to = $mcemail;

								$mail->AddAddress($to, "Caregiver Notification"); 


								if ($mail->Send()) { 
									echo json_encode(array("result" =>"success","message"=>"Caregiver Added Successfully"), 200);
								} else { 
									echo json_encode(array("result" =>"","message"=>"Mail Not Sent"), 204);	 
								} 
							}
						} else {
		 
							//$this->data['msg']="Trying To Enter Same Caregiver And Patient Again ";
							echo json_encode(array("result" =>"","message"=>"Trying To Enter Same Caregiver And Patient Again "), 204);	 
						}
					}
				} else { 	
					//$this->data['msg']="<font color='red'>you have already added the $limit caregivers please take extended licence (or) anether licence to add more caregivers</font>";
					echo json_encode(array("result" =>"","message"=>"you have already added the ".$limit." caregivers please take extended licence (or) anether licence to add more caregivers"), 204);	 
				}
			} else {
				//$this->data['msg']="<font color='red'>please fill the all input fields </font>";
				echo json_encode(array("result" =>"","message"=>"please fill the all input fields"), 204);	 
			}
		     
		}
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////// 
	
	
	////Owner - Buy Licence
	
	
	function buy_anether_success()
	{
		$email=$_POST['user_email'];
	    $oid=$_POST['user_id'];
		
		$num=$this->user->get_owner_licence($oid);
		$licence=$this->user->insert_anether_olicence($oid,$email,$num);
		
		echo json_encode(array("result" =>"success","message"=>$licence), 200);
	}
	
	///Profissional to profissional send requests
	
	function sendfallowrequest()
	{
		$sid = $_POST['sid'];
		$rid = $_POST['rid'];
		
		$date=date("Y-m-d");
		$status=0;
		$ip=$_SERVER['REMOTE_ADDR'];
		$is="insert into professional_networks_prf (sid,rid,netstatus,sdate,netip) values('$sid','$rid','$status','$date','$ip')";
		$ie=mysql_query($is) or die(mysql_error());
		if($ie)
		{
			echo json_encode(array("result" =>"success","rid"=>$rid), 200);
		}
	}
	
	
	
	///Caregiver to profissional send requests
	
	function sendfallowrequest1()
	{
		$sid = $_POST['sid'];
		$rid = $_POST['rid'];
		$name = $_POST['user_disp_name'];
		$utype = $_POST['usertype'];
		$receivertype = $_POST['receivertype'];
		
		
		$date=date("Y-m-d");
		$status=0;
		$ip=$_SERVER['REMOTE_ADDR'];
		
		if($utype=="professional")
		{
		$utype=2;	
		
		$is="insert into professional_networks_prf (sid,rid,netstatus,sdate,netip) values('$sid','$rid','$status','$date','$ip')";
		
		}
		if($utype=="caregiver")
		{
		$utype=1;
		
		$is="insert into professional_networks (sid,rid,netstatus,sdate,netip) values('$sid','$rid','$status','$date','$ip')";
		
		}
		$loginusertye=$utype;
		
		//------
		
		$res="professional";
		
		//----------
		
		
		
		
		
		
		
		$ie=mysql_query($is) or die(mysql_error());
		$lid=mysql_insert_id();
		if($ie)
		{
				
				
				
				 $message=$name."  Had Send The Request "; 
				
				if($utype==2){
					
								
				$ntype="professionalnetwork";
				} else {
					$ntype="caregivernetwork";
				}
				
				$nip=$_SERVER['REMOTE_ADDR'];
				$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip) values('$message','$sid','$rid','$lid','0','$ntype','$nip')";
				$nte=mysql_query($nts) or die(mysql_error());
				
				
				
				
				
			$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
			
			$ds="select * from device_tocken where userid='$rid' and user_type='$res'";
			$se=mysql_query($ds) or die(mysql_error());
			while($sr=mysql_fetch_array($se))
			{
			$device=$sr['tockenid'];
			$device_platform = $sr['platform'];
			$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						'sid'           =>$sid,
						'rid'           =>$rid,
						'sname'         =>$name,
						'recivertype'   =>$loginusertye,
						'lid' => $lid,
				'add_req_state'	=> 'newrequest',
						'call_type'     =>"add_request"
					);
					
					
				

				$res = $this->sendPushNotificationToGCM_addreq($device,$msg,$device_platform,$name,$sid,$rid,$loginusertye,$lid);

				//exit;
				/*$registrationIds = array($device);
					
					 $fields = array
					  (
					   'registration_ids'  => $registrationIds,
					   'data'              => $msg
					  );

					  $headers = array
					  (
					   'Authorization: key=' . $API_ACCESS_KEY1,
					   'Content-Type: application/json'
					  );

					  $ch = curl_init();
					  curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					  curl_setopt($ch,CURLOPT_POST, true );
					  curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					  $result = curl_exec($ch );
					  curl_close( $ch );
						$device = array();
					 echo $result;
					 */
			}
			
			
			
			
			
			//echo json_encode(array("result" =>"success","rid"=>$rid), 200);
		}
	}
	
	
	
	
	
	 function sendPushNotificationToGCM_addreq($deviceid,$msg1,$device_platform,$name,$from,$to,$loginusertye,$lid) {

		$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

			//echo $device_platform; exit;
		if($device_platform=="Android"){
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

	//	echo json_encode(array("result" =>"success"), 200); //exit;
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
			$message = "You have a add request from '".$name."'";
			
			$path = base_url().'assets/koalaDevpush1.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
			$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'sid'           =>$from,
				'rid'           =>$to,
				'sname'         =>$name,
				'recivertype'   =>$loginusertye,
				'lid' => $lid,
				'add_req_state'	=> 'noway',
				'call_type'     =>"add_request"
				
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				// echo json_encode(array("result" =>"success"), 200); exit;
		}
		
		//echo $result;
		echo json_encode(array("result" =>"success"), 200); 
    }
	
	
	
	function rejectrec(){
		 $sid = $_POST['sid'];
		  $rid = $_POST['rid'];
		  $lid = $_POST['lid'];
		  $receivertype = $_POST['receivertype'];
		  $name = $_POST['user_disp_name'];
		  
		  
		  if($receivertype==1) {
		   $us="delete from professional_networks  where id='$lid'";
		   } else {
			     $us="delete from professional_networks_prf  where id='$lid'";
			   
			   
		   }
		 
			   
			   
		   
		   $ue=mysql_query($us) or die(mysql_error());
		   $ms="update popup_notifications set status='1' where n_id='$lid'";
		   $me=mysql_query($ms) or die(mysql_error());
		   
		   if($receivertype==1) {
			   $res = "caregiver";
		   } else if($receivertype==2){
			    $res = "professional";
		   }
		   
		   $message = $name." rejected your add request";
		   
		   
		   if($receivertype==1) {
			
			$ntype="caregivernetwork";
			} else {
				$ntype="professionalnetwork";
			}
			$nip=$_SERVER['REMOTE_ADDR'];
		   
		   $nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip) values('$message','$rid','$sid','$lid','0','$ntype','$nip')";
			$nte=mysql_query($nts) or die(mysql_error());
		  
		  
		  
		  $ds="select * from device_tocken where userid='$sid' and user_type='$res'";
			
				
			$se=mysql_query($ds) or die(mysql_error());
			while($sr=mysql_fetch_array($se))
			{
			$device=$sr['tockenid'];
			$device_platform = $sr['platform'];
			$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'no',
						'call_type'     =>"add_request"
						
			);
		  
		  
		   //$res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,0);
			
			
			$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
				if($device_platform=="Android"){
					
					$registrationIds = array($device);
					
					$fields = array
					(
						'registration_ids'  => $registrationIds,
						'data'              => $msg
					);
					
					$headers = array
					(
						'Authorization: key=' . $API_ACCESS_KEY1,
						'Content-Type: application/json'
					);
					
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt($ch,CURLOPT_POST, true );
					curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					
					
				} elseif($device_platform=="iOS"){
		 
					$deviceToken =$device;
					$passphrase = 'bravemount';
					
					$message = $name." accepted your add request";
					
			
					$path = base_url().'assets/koalaDevpush1.pem';
			
					//echo $path; exit;
					$ctx = stream_context_create();
			
					$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
					stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
					stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
					stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
					// Open a connection to the APNS server
					$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

					//if (!$fp)
						//exit("Failed to connect: $err $errstr" . PHP_EOL);

					//echo 'Connected to APNS' . PHP_EOL;

					// Create the payload body
					$body['aps'] = array(
						'alert' => $message,
						'sound' => 'default',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'no',
						'call_type'     =>"add_request"
						
						
					);

					// Encode the payload as JSON
					$payload = json_encode($body);

					// Build the binary notification
						
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
					//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
						
						//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
						//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
						//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));

					//if (!$result)
					//	echo 'Message not delivered' . PHP_EOL;
					//else
					//	echo 'Message successfully delivered' . PHP_EOL;

					// Close the connection to the server
					fclose($fp);
			
				 
				}
			
			
				echo $result;
			
			
			
			
			}
	}
	
	
	function acceptrec()
      {
		  
		
		  $sid = $_POST['sid'];
		  $rid = $_POST['rid'];
		  $lid = $_POST['lid'];
		  $receivertype = $_POST['receivertype'];
		  $name = $_POST['user_disp_name'];
		  
		 //  echo $receivertype; exit;
		   if($receivertype==1) {
		   $us="update professional_networks set netstatus='1',cread='0' where id='$lid'";
		   } else {
			    $us="update professional_networks_prf set netstatus='1',pread='0' where id='$lid'";
			   
		   }
		   $ue=mysql_query($us) or die(mysql_error());
		   $ms="update popup_notifications set status='1' where n_id='$lid'";
		   $me=mysql_query($ms) or die(mysql_error());
		   
		   if($receivertype==1) {
			   $res = "caregiver";
		   } else if($receivertype==2){
			    $res = "professional";
		   }
		   
		   $message = $name." accepted your add request";
		   
		    
			
			if($receivertype==1) {
			
			$ntype="caregivernetwork";
			} else {
				$ntype="professionalnetwork";
			}
			$nip=$_SERVER['REMOTE_ADDR'];
			$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip) values('$message','$rid','$sid','$lid','0','$ntype','$nip')";
			$nte=mysql_query($nts) or die(mysql_error());
			
			
			 $ds="select * from device_tocken where userid='$sid' and user_type='$res'";
			
				
			$se=mysql_query($ds) or die(mysql_error());
			while($sr=mysql_fetch_array($se))
			{
			$device=$sr['tockenid'];
			$device_platform = $sr['platform'];
			$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
			);
			
		
		 // echo $device; exit;
		   
		  //$res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,1);
		  //echo json_encode(array("result" =>"success"), 200);
		// $res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,1);

				$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
				if($device_platform=="Android"){
					
					$registrationIds = array($device);
					
					$fields = array
					(
						'registration_ids'  => $registrationIds,
						'data'              => $msg
					);
					
					$headers = array
					(
						'Authorization: key=' . $API_ACCESS_KEY1,
						'Content-Type: application/json'
					);
					
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt($ch,CURLOPT_POST, true );
					curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					
					
				} elseif($device_platform=="iOS"){
		 
					$deviceToken =$device;
					$passphrase = 'bravemount';
					
					$message = $name." accepted your add request";
					
			
					$path = base_url().'assets/koalaDevpush1.pem';
			
					//echo $path; exit;
					$ctx = stream_context_create();
			
					$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
					stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
					stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
					stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
					// Open a connection to the APNS server
					$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

					//if (!$fp)
						//exit("Failed to connect: $err $errstr" . PHP_EOL);

					//echo 'Connected to APNS' . PHP_EOL;

					// Create the payload body
					$body['aps'] = array(
						'alert' => $message,
						'sound' => 'default',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
						
					);

					// Encode the payload as JSON
					$payload = json_encode($body);

					// Build the binary notification
						
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
					//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
						
						//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
						//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
						//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));

					//if (!$result)
					//	echo 'Message not delivered' . PHP_EOL;
					//else
					//	echo 'Message successfully delivered' . PHP_EOL;

					// Close the connection to the server
					fclose($fp);
			
				 
				}
			
			
				echo $result;
			
			}
	
	  }
	
	
	
	
	
	function sendPushNotificationToGCM_addreq_succ($deviceid,$msg1,$device_platform,$name,$status) {

		$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		

			echo $status; exit;
		if($device_platform=="Android"){
			
			$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

		
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		//echo "hai"; exit;
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
			
			if($status==1){
				$message = $name." accepted your add request";
			} else {
				$message = $name." rejected your add request";
			}
			
			$path = base_url().'assets/koalaDevpush1.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
			$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'result_status' => $status,	
'add_req_state'	=> 'noway',				
				'call_type'     =>"add_request"
				
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				 
		}
		
		echo $result;
		
    }
	
	
	
	
	
	/////////Caregiver profile editing
	
	function update_caregiver_profile(){
		
	
			$update = $this->user->updatecaregiversbycid();
			echo json_encode(array("result" =>"success"), 200);
		
	}
	
	function update_own_caregiver_profile(){

			$update = $this->user->updateowncaregiversbycid();
			echo json_encode(array("result" =>"success"), 200);
		
	}
	
	
	/////////////Get Professional Patients Routines
	
	public function patient_routine()
	{
		$pid = $_POST['patient_id'];
		$date = $_POST['now_date'];
		$user_type=  $_POST['user_type'];
		
		$patient_routines = $this->user->getnotification_new($pid,$date,$user_type);
		
	
		if($patient_routines<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			echo json_encode(array("result" =>"success","patient_routines" => $patient_routines), 200);
		}
	
	}
	
	
	/////////////Get Professional Patients parameter
	function showparameter()
	{
		$pid = $_POST['patient_id'];
		$eventid=1;
		$parameters = $this->user->fetch_parameter1($pid,$eventid);
		
		
		if($parameters<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			echo json_encode(array("result" =>"success","parameters_routines" => $parameters), 200);
		}
		
	
	}
	  
	  /////////////Get Professional Patients Drugs
	public function show_drugs()
	{
		$pid = $_POST['patient_id'];
		$eventid=2;
	
		$medicene = $this->user->fetch_medecine($pid,$eventid);
		
	
	
		if($medicene<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			echo json_encode(array("result" =>"success","medicene" => $medicene), 200);
		}
	
	
	
	}
	
	public function show_diets()
	{
		  
		$pid = $_POST['patient_id'];
		$eventid=3;
		$show_diets = $_POST['show_diets'];
		
		
		
		$diets=$this->user->fetch_diets($pid,$eventid,$show_diets );
		
		if($diets<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			echo json_encode(array("result" =>"success","diets" => $diets), 200);
		}
	}
	
	
	public function show_activity()
	{
		$pid = $_POST['patient_id'];
		$eventid=4;
		
		 $activities = $this->user->fetch_activity($pid,$eventid);
		 
		 if($activities<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
		
			echo json_encode(array("result" =>"success","activities" => $activities), 200);
		}
	}
	
	
	function show_report()
	{
	
		$pid = $_POST['patient_id'];
		
		$reports = $this->user->fetch_report1($pid);
		
		
		if($reports<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
		
			echo json_encode(array("result" =>"success","reports" => $reports), 200);
		}
	}
	
	
	function mail_function($to,$to_name,$subject,$body){
	
		$to = $to;
		$to_name = $to_name;
		$subject= $subject;
		$body = $body;
		
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true; 
		$mail->SMTPSecure = "ssl"; 
		$mail->Port       = 465;
		$mail->Host       = "smtp.yandex.com";
		$mail->Username   = "notification@Koala.Care";
		$mail->Password   = "No1L@S@pp1@mo";
		$mail->SetFrom('notification@Koala.Care', 'Koala Team');
		$mail->Subject = $subject;     
		$body = $body;
		@$body = eregi_replace("[\]",'',$body);
				   $mail->MsgHTML($body);
		$to = $to;
		
		$mail->AddAddress($to, $to_name); 

		if ($mail->Send()) { 
			echo json_encode(array("result" =>"success","message"=>"Mail Sent Successfully"), 200);
			
		} else { 
			echo json_encode(array("result" =>"","message"=>"Mail Not Sent"), 200);
		} 
	
	}
	
	
	
	function view_report2()
	{ 
		$pid = $_POST['patient_id'];
		$date = $_POST['report_date'];
		
		$reports=$this->user->fetch_report_details($pid,$date);
		
		
		if($reports<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			echo json_encode(array("result" =>"success","reports" =>$reports), 200);
		}
	
	}
	
	function getownet_ascare(){
		$pid =  $_POST['patient_id'];
		$id = $_POST['user_id'];
		$octype=$this->user->get_owner_caretype($pid,$id);
		
			echo json_encode(array("result" =>"success","octype" =>$octype), 200);
		
	}
	
	
	///Caregiver Add routines
	
	function add_caregiver_routines(){
		$pid =  $_POST['patient_id'];
		$eid =  $_POST['eid'];
		
		$get_res = $this->user->get_caregiver_routines($pid,$eid);
	
		echo json_encode(array("result" =>"success","details_list" =>$get_res ), 200);
	
	}
	
	function  insert_event()
	{
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$acttype=$_POST['event_type'];
		$actdetails=$_POST['details_id'];
	
		
	
	
		if($acttype==1)
		{
			$ps="select * from parameter_name where id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['pname'];
			
		}
		if($acttype==2)
		{
			
			$ps="select * from medicine where med_id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['med_name'];
			
			
		}
		if($acttype==3)
		{
			
			$ps="select * from diets where id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['food_name'];
			
			
		}
		if($acttype==4)
		{
			
			$ps="select * from activities where act_id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['act_name'];
			
		}
		if($acttype==5)
		{
			$ps="select * from sub_event_type where sub_id='$actdetails'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pr=mysql_fetch_array($pe);
			$adel=$pr['sub_eventname'];
			
		}
	
	
	
		$time=$_POST['date_time'];
		$date = $_POST['now_date'];//date('Y-m-d');
		
		$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time','$date','','0','0','$adel')";
		$exe=mysql_query($ins11) or die(mysql_error());
		$ins=mysql_insert_id();
		
		if($ins)
		{
			echo json_encode(array("result" =>"success","message" =>"Event set successfully","ins" =>$ins), 200);
		} else {
			echo json_encode(array("result" =>"success","message" =>"Problem occured"), 200);
		}
	
	}

	//Caregiver viwe routines
	function view_notification()
	{
		$id = $_POST['note_id'];
		$get_res = $this->user->view_notification($id);
		echo json_encode(array("result" =>"success","notification_info" =>$get_res), 200);
	}
	
	function edit_notification()
	{
		//echo json_encode(array("result" =>"success","message" =>$_POST), 200); exit;
		$id = $_POST['note_id'];
		$ue = $this->user->edit_notification($id);
		//if($ue==true){
			echo json_encode(array("result" =>"success"), 200);
		//}
	}
	
	///Caregiver Insert Activity
	
	function insert_activity()
	{
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$actdetails=$_POST['actdetails'];
		$acttype=4;
		$actname=$_POST['actname'];
		
		$ins=$this->user->insert_activity($patient_id,$caregiver_id,$actdetails,$acttype,$actname);
		
		
		
		
			echo json_encode(array("result" =>"success"), 200);
		
	//echo json_encode(array("result" =>"success"), 200);
	}
	
	//Caregiver viwe Activity
	function view_activity()
	{
		$id = $_POST['act_id'];
		$get_res = $this->user->view_activity($id);
		echo json_encode(array("result" =>"success","activity_info" =>$get_res), 200);
	}
	
	
	//update activity
	
	
	function edit_activity()
	{
		$act_id = $_POST["act_id"];
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$actdetails=$_POST['actdetails'];
		$acttype=4;
		$actname=$_POST['actname'];
		
		$ins=$this->user->edit_activity($act_id,$patient_id,$caregiver_id,$actdetails,$acttype,$actname);
		
	
			echo json_encode(array("result" =>"success"), 200);
	
	}
	
	///Caregiver medicene
	function insert_medicine()
	{
		$med_id=$_POST["med_id"];
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$med_dose=$_POST['med_dose'];
		$medname=$_POST['med_name'];
		$med_type=$_POST['med_type'];
		$med_note=$_POST['med_note'];
		$med_qua=$_POST['med_qua'];
		$pre=$_POST['pre_date'];
		$exp=$_POST['exp_date'];
		$med_price=$_POST['med_price'];	
		$meal_type=$_POST['meal_type'];
		$checkboxFourInput = $_POST['checkboxFourInput'];
		$checkboxFourInput1 = $_POST['checkboxFourInput1'];
		$checkboxFourInput2 = $_POST['checkboxFourInput2'];
		
		
		$mn=$this->user->get_medlist($medname,$patient_id,$med_id);
		
		if($mn<=0)
		{
			$ins=$this->user->insert_medicine($med_id,$patient_id,$caregiver_id,$medname,$med_type,$med_note,$med_qua,$pre,$exp,$med_price,$meal_type,$med_dose,$checkboxFourInput,$checkboxFourInput1,$checkboxFourInput2);
		
			if($ins)	
			{
				if($med_id) { $data="Drug has been updated !";   } else { $data="Drug has been added !"; }
				echo json_encode(array("result" =>"success","message"=>$data), 200);
			}
			else
			{
				$data="Unsuccesfull, Try again !";
				echo json_encode(array("result" =>"","message"=>$data), 204);
			}
		}
		else
		{
			$data="You Have Entered Same Drug Details Again!";
			echo json_encode(array("result" =>"","message"=>$data), 204);
		}
	}
	
	
	///View medecin
	function view_medicine()
	{
		$id = $_POST['med_id'];
		$medicine = $this->user->get_medicine($id);
		echo json_encode(array("result" =>"success","medicine_info"=>$medicine), 200);
	
	}
	
	///Caregiver Add Diet
	
	
	function add_diet()
	{
		$diet_id = $_POST['diet_id'];
		$patient_id=$_POST["patient_id"];
		$caregiver_id=$_POST['user_id'];
		$foodname=$_POST['foodname'];
		$foodtype=$_POST['foodtype'];
		$fooddel=$_POST['fooddel'];
		$dateofweek="";//$_POST['dateofweek'];
		$foodlike=$_POST['foodlike'];
		$nfile = "";
	
		
		if($diet_id){
		$fs="select * from diets where food_name='$foodname' and id<>'$diet_id'";
		} else {
		$fs="select * from diets where food_name='$foodname'";
		}
		$fe=mysql_query($fs) or die(mysql_error());
		$fn=mysql_num_rows($fe);
		
			
		if($fn<=0)
		{
			
			if($diet_id){
			
				$ins=$this->user->update_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$nfile,$foodlike,$diet_id);
				
				$flikeid1=$diet_id;
				$foodlike=$_POST['foodlike'];
				$fs="select * from foodlike where patid='$patient_id' and foodid='$diet_id'";
				$fe=mysql_query($fs) or die(mysql_error());
				$fn=mysql_num_rows($fe);
				//echo json_encode(array("result" =>"success","message"=>$foodlike), 200);exit;
				if($fn>0)
				{
				$is="UPDATE foodlike set foodlike='$foodlike' where foodid='$flikeid1'";
				$ie=mysql_query($is) or die(mysql_error());
				}
				if($fn<=0)
				{
				$is="insert into  foodlike (foodid,patid,foodlike) values ('$diet_id','$patient_id','$foodlike') ";
				$ie=mysql_query($is) or die(mysql_error());
					
				}
			
			} else {
			
				$ins=$this->user->insert_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$nfile,$foodlike);

				$is="insert into foodlike(foodid,patid,foodlike) values ('$ins','$patient_id','$foodlike')";
				$ie=mysql_query($is) or die(mysql_error());

			}
			
			if($ie)
			{
				if($diet_id){ $data="Diets has been Updated !"; } else {$data="Diets has been added !"; }
				echo json_encode(array("result" =>"success","message"=>$data), 200);
				
			}
		}
		else
		{
		
			$data="Trying To Add Same Food Name please try with anether name";
			echo json_encode(array("result" =>"","message"=>$data), 204);
		}
	}


	///View diets
	function view_diets()
	{
		$id = $_POST['diet_id'];
		$diets = $this->user->get_diets($id);
		echo json_encode(array("result" =>"success","diets_info"=>$diets), 200);
	
	}


	//Add parameter
	
	function add_parameters_care()
	{ 
		//$numclu=$_POST['measure_names'];
		//$numclu=1;
		//$k = $numclu+$numclu;
		
		  
			$param_id= $_POST['param_id'];
			$pname=$_POST['pname'];
			$numclu=$_POST['measure_names'];
			$date=date("Y-m-d");
			
			if($param_id){ 
			$ps="select * from parameter_name where pname='$pname' and id<>'$param_id'";
			} else { 
			$ps="select * from parameter_name where pname='$pname'";
			}
			
			//echo json_encode(array("result" =>"success","diets_info"=>$_POST['arr'][]), 200); exit;
			
			$pe=mysql_query($ps) or die(mysql_error());
			$pn=mysql_num_rows($pe);
		
			if($pn<=0)
			{
				$id=$this->user->addparameters($param_id,$pname,$numclu,$date);
			
				
				$num=$numclu;
				if($param_id){ 
					$query = $this->db->query("SELECT * FROM parameter_columns where para_id='$param_id'");
					$res = $query->num_rows();
					$k=$res+1;
				} else { $k=1; }
				//	echo json_encode(array("result" =>"success","diets_info"=>$k), 200); exit;
				for($i=$k;$i<$num+1;$i++)
				{
				$name=$_POST['arr'][$i];
				$this->user->add_paracolumns($param_id,$id,$name);
				
				}	
					
				if($param_id){ 
					echo json_encode(array("result" =>"success","message"=>"Parameters Has Been Update !"), 200);
				} else {
					echo json_encode(array("result" =>"success","message"=>"Parameters Has Been Added !"), 200);
				}
			}
			else
			{
				//$this->data['flash']=" You Are Trying To Enter Same  Parameter Name Again !";
				echo json_encode(array("result" =>"","message"=>" You Are Trying To Enter Same  Parameter Name Again !"), 204);
			}
				
	 
		
	}
	
	
	//view parameter
	
	function view_perameter(){
			$id = $_POST['param_id'];
			$parameter_info = $this->user->view_perameter($id);
			
			echo json_encode(array("result" =>"success","parameter_info"=>$parameter_info ), 200);
	}
	
	
	//Caregiver report mail send
	function send_report_mail()
	{ 
		$pid = $_POST['patient_id'];
		$date = $_POST['report_date'];
		$patient_name = $_POST['patient_name'];
		$reports=$this->user->fetch_report_details($pid,$date);
		
		
		if($reports<=0){
		
			echo json_encode(array("result" =>"","message" =>"No Result Found"), 204);
			
		} else {
			//echo json_encode(array("result" =>"success","reports" =>$report->email), 200); exit;
			
			$to = $_POST['user_email'];//"satyanarayana.p@bravemount.com";//"kalyankumarp@gmail.com";//$_POST['user_email']; // "mahamoodphp@gmail.com";//
			$to_name = $_POST['user_disp_name'];
			
			$subject = "Koala report of patient - ".$patient_name;
			
			
			
			
			
			$body = "<div>";
			foreach($reports as $report){
			
			if($report->event_type_id=="1") {  
					$event_name = "Clinical parameters";
			} else if($report->event_type_id=="2") {  
				$event_name = "Drugs";
			}else if($report->event_type_id=="3") {  
				$event_name = "Diet";
			}else if($report->event_type_id=="4") {  
				$event_name = "Activities";
			}else if($report->event_type_id=="5") {  
				$event_name = "Sleep / wake";
			}
			
			if($report->status=="1") { $event_status = "Done"; } else { $event_status = "Not Done"; }
			
			$body.="<div style='border:1px solid black; width:600px; margin:0 auto;' ><p style='margin-left:5px;'>Event Name: ".$event_name."</p>";
			
			if($event_name=="Clinical parameters"){
				$sub_id = $report->sub_event_id;
				$select = "select * from parameters where para_id='$sub_id'";
				$qry = mysql_query($select)or die(mysql_error());
				$body.="<p style='margin-left:5px;'>Details: </p>";
				while($res = mysql_fetch_array($qry)){
					if($res['option1']<>NULL || $res['option1']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option1']."</p>";
					}
					if($res['option2']<>NULL || $res['option2']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option2']."</p>";
					}
					if($res['option3']<>NULL || $res['option3']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option3']."</p>";
					}
					if($res['option4']<>NULL || $res['option4']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option1']."</p>";
					}
					if($res['option5']<>NULL || $res['option5']<>""){
						$body.="<p style='margin-left:5px;'>".$res['option1']."</p>";
					}
				}
			} else {
				$body.="<p style='margin-left:5px;'>Details: ".$report->sub_event_name."</p>";
			}
			
			$body.="<p style='margin-left:5px;'>Time: ".$report->note_time."</p>";
			$body.="<p style='margin-left:5px;'>Event Status: ".$event_status."</p></div>";
			$body.="<p style='margin-left:5px;'></p>";
			
			}
			$body.= "</div>";
						
			//echo json_encode(array("result" =>"success","reports" =>$body), 200); exit;
			$res = $this->mail_function($to,$to_name,$subject,$body);
		}
	
	}
	
	
	////////////////////////Internal Notifications
	
	function internal_notification(){
	
		
		 $cs="select * from notifications where  note_date='$date'  and care_id='$mcid' and status=0 order by note_id desc";
		$ce=mysql_query($cs) or die(mysql_error());
		$cn=mysql_num_rows($ce);
		echo json_encode(array("result" =>"success","reports" =>$cn), 200); exit;
		//while($cr=mysql_fetch_array($ce))
		//{
			//$ntime=substr($cr['note_time'],0,5);
		
		//}
		$date = date("Y-m-d");
		$timeFirst  = strtotime($date.' '.$_POST['timeFirst']);
		$timeSecond = strtotime($date.' '.$_POST['timeSecond']);
		
		
		
		
		$differenceInSeconds = $timeSecond - $timeFirst;//heigh-low
		
		echo json_encode(array("differenceInSeconds" =>$differenceInSeconds), 200);
		
	}
	
	
	
	function get_text_session(){
		$rid=$_POST['rid'];
		$userid=$_POST['user_id'];
		$receivertype=$_POST['receivertype'];
		$utype = $_POST['usertype'];
	
	
		if($utype=="professional")
		 {
		 $utype=2; 
		 }
		 if($utype=="caregiver")
		 {
		 $utype=1;
		 }
	
	
		$ch="select * from chat where (chat.from='$userid' and chat.to='$rid') or (chat.from='$rid' and chat.to='$userid') and receivertype='$receivertype' and  usertype='$utype'";
		$ce=mysql_query($ch) or die(mysql_error());
		$cr=mysql_fetch_array($ce);
		$cn=mysql_num_rows($ce);
		
		echo json_encode(array("session_id" =>$cr['text_session']), 200); 
		
	
	}
	
	
function ajax_newchat()
{ 
  

	
	  
 
	
//$this->data['rid']=$_POST['Recvierid'];
//$this->data['ctitle']=$_POST['chatboxtitle'];
//$this->data['ctype']=$_POST['chatwith'];
//$this->data['receivertype']=$_POST['receivertype'];
//$this->load->view('home/ajax_newchat', $this->data);
//session_start();
$rid=$_POST['rid'];
//$ctitle=$_POST['chatboxtitle'];
//$ctype=$_POST['chatwith'];
$userid=$_POST['sid'];

/*
$ch="select * from chat where (chat.from='$userid' and chat.to='$rid') or (chat.from='$rid' and chat.to='$userid')";
$ce=mysql_query($ch) or die(mysql_error());
$cr=mysql_fetch_array($ce);
$cn=mysql_num_rows($ce);
if($cn>0)
{
$session_id=$cr['text_session'];
}
else
{
	$session_id=$_POST['session_id'];
}
*/

//$chatboxtitle=$ctitle;   and text_session='$session_id'
//$loginusertye=$this->session->userdata('usertypeid');
$receivertype;
//and usertype='$loginusertye' and receivertype='$receivertype'
/*
$sub="";
if(!empty($_POST['session_id'])){
$session_id = $_POST['session_id'];
$sub = "and text_session='$session_id'";
}*/
$session_id = $_POST['session_id'];
 $cs="select * from chat where (chat.from='$rid' or chat.to='$rid') and (chat.from='$userid' or chat.to='$userid')  and text_session='$session_id'   order by id desc limit 10";
$ce=mysql_query($cs) or die(mysql_error());
$cn=mysql_num_rows($ce);
if($cn>0)
{
while($cr=mysql_fetch_array($ce))
{
	$s="select * from caregiver where id='$cr[from]'"; 
    $e=mysql_query($s) or die(mysql_error());
	$n=mysql_num_rows($e);
	if($n>0)
	{
	$ns="select * from caregiver where id='$cr[from]'"; 
    $ne=mysql_query($ns) or die(mysql_error());
	$nr=mysql_fetch_array($ne);
	}
	else
	{
     $ns="select * from professionals where id='$cr[from]'"; 
    $ne=mysql_query($ns) or die(mysql_error());
	$nr=mysql_fetch_array($ne);
		
	}
	if($userid==$cr['from'])
	{
		$messages['sty']="right";
	if(!empty($nr['photo']))
	{
	$messages['pimg']=$nr['photo'];
	}
	else
	{
		$messages['pimg']="img-holder2.png";
	}
	}
	else
	{
	$messages['sty']="left";	
	if(!empty($nr['photo']))
	{
	$messages['pimg']=$nr['photo'];	
	}
	
	else
	{
		
		$messages['pimg']="img-holder2.png";
	}
	
	}
	
	$messages['name']=$nr['name'];
	$messages['s'] = '0';
	$messages['f'] = $cr['from'];
	$messages['id'] = $cr['id'];
	$messages['m']=$cr['message'];
	$messages['sent']=$cr['sent'];
	//$messages['session_id']=$cr['text_session'];
	//$_SESSION['MinId'] = $cr['id'];
	$this->session->set_userdata('MinId', $cr['id']);
	$msg[] = $messages;

}
$chatmsgs= $msg;
$lastmessage = reset($chatmsgs);
$msg=array_reverse($msg);
//print_r($lastmessage);
 //$this->session->set_userdata('MinId', $lastmessage['id']);
  //$_SESSION['chatMaxID'] = $lastmessage['id'];
  //$_SESSION['chatMaxID'] = $lastmessage['id'];
  $this->session->set_userdata('chatMaxID', $lastmessage['id']);
  
  //$_SESSION['MinId'] ;
 //print_r($msg);
//echo $result= json_encode($msg);

echo json_encode(array("result" =>"success","caregiver_chat" =>$msg), 200);




?>

<?php
}
}



function ajax_newchathotbeat()
{ 
  


//$this->data['rid']=$_POST['Recvierid'];
//$this->data['ctitle']=$_POST['chatboxtitle'];
//$this->data['ctype']=$_POST['chatwith'];
//$this->data['receivertype']=$_POST['receivertype'];
//$this->load->view('home/ajax_newchat', $this->data);
//session_start();
$rid=$_POST['rid'];
//$ctitle=$_POST['chatboxtitle'];
//$ctype=$_POST['chatwith'];
$userid=$_POST['sid'];
//$chatboxtitle=$ctitle;
//$loginusertye=$this->session->userdata('usertypeid');
$receivertype;
//and usertype='$loginusertye' and receivertype='$receivertype'
$maxid=$this->session->userdata('chatMaxID');


 $cs="select * from chat where (chat.from='$rid' or chat.to='$rid') and (chat.from='$userid' or chat.to='$userid')  and id>'$maxid'   order by id asc";
$ce=mysql_query($cs) or die(mysql_error());
$cn=mysql_num_rows($ce);
if($cn>0)
{
while($cr=mysql_fetch_array($ce))
{
	$s="select * from caregiver where id='$cr[from]'"; 
    $e=mysql_query($s) or die(mysql_error());
	$n=mysql_num_rows($e);
	if($n>0)
	{
	$ns="select * from caregiver where id='$cr[from]'"; 
    $ne=mysql_query($ns) or die(mysql_error());
	$nr=mysql_fetch_array($ne);
	}
	else
	{
     $ns="select * from professionals where id='$cr[from]'"; 
    $ne=mysql_query($ns) or die(mysql_error());
	$nr=mysql_fetch_array($ne);
		
	}
	if($userid==$cr['from'])
	{
		$messages['sty']="right";
	if(!empty($nr['photo']))
	{
	$messages['pimg']=$nr['photo'];
	}
	else
	{
		$messages['pimg']="img-holder2.png";
	}
	}
	else
	{
	$messages['sty']="left";	
	if(!empty($nr['photo']))
	{
	$messages['pimg']=$nr['photo'];	
	}
	
	else
	{
		
		$messages['pimg']="img-holder2.png";
	}
	
	}
	
	$messages['name']=$nr['name'];
	$messages['s'] = '0';
	$messages['f'] = $cr['from'];
	$messages['id'] = $cr['id'];
	$messages['m']=$cr['message'];
	$messages['sent']=$cr['sent'];
	//$_SESSION['MinId'] = $cr['id'];
	$this->session->set_userdata('MinId', $cr['id']);
	
	
	$this->session->set_userdata('chatMaxID', $cr['id']);
	$msg[] = $messages;

}
$chatmsgs= $msg;
$lastmessage = reset($chatmsgs);
$msg=array_reverse($msg);
//print_r($lastmessage);

 $this->session->set_userdata('MinId', $lastmessage['id']);
 
 
  //$_SESSION['chatMaxID'] = $lastmessage['id'];
  //$_SESSION['MinId'] ;
 //print_r($msg);
//echo $result= json_encode($msg);

echo json_encode(array("result" =>"success","caregiver_chat" =>$msg), 200);




?>

<?php
}
}





function sendchatinsert()
{
//echo json_encode(array("result" =>"success","caregiver" =>'okokokokokoko'), 200); 
	//$fr=$_POST['sid'];
	
	
    $name=$_POST['name'];
	$from =$_POST['from']; 
	$to=$_POST['to'] ;
	$message=$_POST['message'];
    $receivertype=$_POST['recivertype'];
	
	$utype=$_POST['usertype'];
	if($utype=="professional")
	{
	$utype=2;	
	}
	if($utype=="caregiver")
	{
	$utype=1;
	}
	$loginusertye=$utype;
	$whn=time(); 
	if($receivertype==2)
	{
		$res="professional";
		$ps="select * from professionals where id='$to'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pr=mysql_fetch_array($pe);
		$rname=$pr['name'];
		
		
	}
    if($receivertype==1)
	{
		$res="caregiver";
		
		$ps="select * from caregiver where id='$to'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pr=mysql_fetch_array($pe);
		$rname=$pr['name'];
		
		
	}



$ch="select * from chat where (chat.from='$from' and chat.to='$to') or (chat.from='$to' and chat.to='$from')";
$ce=mysql_query($ch) or die(mysql_error());
$cr=mysql_fetch_array($ce);
$cn=mysql_num_rows($ce);
if($cn<=0)
{
$var=  uniqid('', true);
$var;

$sql = "insert into chat (text_session,whn,receivertype,chat.from,chat.to,message,usertype,sent) values ('$var','$whn','$receivertype','".mysql_real_escape_string($from)."', '".mysql_real_escape_string($to)."','".mysql_real_escape_string($message)."','".$loginusertye."',NOW())";
$query = mysql_query($sql) or die(mysql_error());
$lid=mysql_insert_id();
}
if($cn>0)
{
//$ch="select * from chat where (chat.from='$from' or chat.to='$from') and (chat.from='$to' or chat.to='$to')";
//$ce=mysql_query($ch) or die(mysql_error());
//$cr=mysql_fetch_array($ce);
$var=$cr['text_session'];
$sql = "insert into chat (text_session,whn,receivertype,chat.from,chat.to,message,usertype,sent) values ('$var','$whn','$receivertype','".mysql_real_escape_string($from)."', '".mysql_real_escape_string($to)."','".mysql_real_escape_string($message)."','".$loginusertye."',NOW())";
$query = mysql_query($sql) or die(mysql_error());
$lid=mysql_insert_id();
}




$this->session->set_userdata('chatMaxID',$lid);
//$_SESSION['chatMaxID']=mysql_insert_id();

$nmsg=$name."  Had Send New Message ";
$ntype="messages".$utype;
$nip=$_SERVER['REMOTE_ADDR'];
$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip) values('$nmsg','$from','$to','$lid','0','$ntype','$nip')";
$nte=mysql_query($nts) or die(mysql_error());	

if($utype==2 and $receivertype==2)
{
	$logintype="prof";
}
if($utype==1 and $receivertype==2)
{
	$logintype="care";
}
if($utype==2 and $receivertype==1)
{
	$logintype="careprof";
}
//echo json_encode(array("result" =>"success","caregiver" =>'okokokokokoko'), 200); 

	
	$ds="select * from device_tocken where userid='$to' and user_type='$res'";
	$se=mysql_query($ds) or die(mysql_error());
	while($sr=mysql_fetch_array($se))
	{
		$device=$sr['tockenid'];
		$device_platform = $sr['platform'];
		$msg = array(
						'message'       => $message,
						'title'         => $name.'Send The Message',
						'subtitle'      => 'Chat Mssage',
						'sid'           =>$from,
						'rid'           =>$to,
						'sname'         =>$name,
						'recivertype'   =>$loginusertye,
						'logintype'     =>$logintype,
						'chat'          =>"text", 
						'call_type'     =>"text",
						'session_id'    =>$var,
						'rname'         =>$rname,
						'vibrate'        => 1,
						'sound'         => 1
					);
		$res = $this->sendPushNotificationToGCM($device,$msg,$device_platform,$name,$from,$to,$loginusertye,$logintype,$var,$rname);
        
	}
	//$dev="APA91bE-RtPXNS3cCKIl8QvZT7W7_JJzKjgp1dGHLng4yaS0IhwSqYyyt1A2wR1ndVdCbShcj5Ag4SthsP5q0b0pUerYQdxo3dZ5xK6ukOfRr10TT_6OvKNYdw_rv-UgfPv5uPTEk3LZrn5MtNkTdG2HXiWcad-R0w";
	
//$res = $this->sendPushNotificationToGCM($dev);	
	
	

	
	
	
	
		
	
}

function check_chat_session()
{
	
$from=$_POST['from'];
$to=$_POST['to'];
$session_id=$_POST['session_id'];

$ch="select * from chat where (chat.from='$from' and chat.to='$to') or (chat.from='$to' and chat.to='$from') and text_session='$session_id'";
$ce=mysql_query($ch) or die(mysql_error());
$cr=mysql_fetch_array($ce);
$cn=mysql_num_rows($ce);
if($cn<=0)
{
	echo json_encode(array("result" =>"success","count" =>$cn,"session_id" =>$session_id), 200);
}
else
{
	echo json_encode(array("result" =>"success","count" =>$cn,"session_id" =>$session_id), 200);
}

}


function messageread()
{  
 
	
//$email=$this->session->userdata('owner_care');
//$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
	 $cid1=$_POST['sid'];
	 $rid=$_POST['rid'];
	
	// $pa = $this->user->update_chat($cid1,$rid);
	$us="update chat set recd='1' where chat.to='$cid1' and chat.from='$rid'";
	$ue=mysql_query($us) or die(mysql_error());
	
	//echo json_encode(array("result" =>"success","msg" =>$us), 200); 
	
	//$pa1 = $this->user->update_chat_pop($cid1,$rid);
	
	$us1="update popup_notifications set status='1' where n_sid='$rid' and n_rid='$cid1'";
	$ue1=mysql_query($us1) or die(mysql_error());

	
}




function buy_licence()
{
	$oid = $_POST['user_id'];
	$email = $_POST['user_email'];
	$to_name = $_POST['user_disp_name'];
	$num=$this->user->get_owner_licence($oid);
	$id=$this->user->insert_anether_olicence($oid,$email,$num);
	$lic="koala".$oid."_".$num;
	
	if($id)
	{
		
		$subject = "Koala-Licence Purchase Confirmation.";
				$to = $email; //"mahamoodphp@gmail.com";//
				$to_name = $_POST["user_disp_name"];
				$body = "
				<html>
				<head>
				<title>HTML email</title>
				</head>
				<body>
				<table>
				<tr>
				<th> Email: </th>
				<td>$email</td>
				</tr>
				<tr>
				<th> Licence Id: </th>
				<td> $lic </td>
				</tr> 
				</table>
				</body>
				</html>
				";
				
				$res = $this->mail_function($to,$to_name,$subject,$body);
				
				
	}
	
	//echo json_encode(array("result" =>"success","caregiver_chat" =>$num), 200); exit;
}


	function upgrade_licence(){

		$licence = $_POST['licence'];
		$id = $_POST['user_id'];
	
		$oid = $_POST['user_id'];
		$email = $_POST['user_email'];
		$to_name = $_POST['user_disp_name'];
		
		$lic=$licence;
	
	
		$num=$this->user->get_owner_by_licence($licence,$id);
			
		
		if($num>0)
		{
			$id=$this->user->updateownerlicencestatus1($licence);
		
			
				$subject = "Koala-Licence Upgrade Confirmation.";
				$to = $email; //"mahamoodphp@gmail.com";//
				$to_name = $_POST["user_disp_name"];
				$body = "
				<html>
				<head>
				<title>HTML email</title>
				</head>
				<body>
				<table>
				<tr>
				<th> Email: </th>
				<td>$email</td>
				</tr>
				<tr>
				<th> Licence Id: </th>
				<td> $lic </td>
				</tr> 
				</table>
				</body>
				</html>
				";
			
			$res = $this->mail_function($to,$to_name,$subject,$body);
		} else {
			
			echo json_encode(array("result" =>"success","message" =>"Entered Licence is already Extended or Invalid."), 200); exit;
		}
	 
	 

	}
	
	
	
	
	
	
	
	
	
	 function sendPushNotificationToGCM($deviceid,$msg1,$device_platform,$name,$from,$to,$loginusertye,$logintype,$var,$rname) {

		$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

			//echo $device_platform; exit;
		if($device_platform=="Android"){
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
			$message = $name.'Send The Message';
			
			$path = base_url().'assets/koalaDevpush1.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
			$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'sid'           =>$from,
				'rid'           =>$to,
				'sname'         =>$name,
				'recivertype'   =>$loginusertye,
				'logintype'     =>$logintype,
				'chat'          =>"text", 
				'call_type'     =>"text",
				'session_id'    =>$var,
				'rname'         =>$rname
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				 
		}
		
		echo $result;
		
    }
	
	
	
	function push()
	{
		
// Your API key
$api_key = "AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw";

// Client IDs from your application
$registration_ids = array($device);

$message = "hello, test!!";

// URL to POST to
$gcm_url = 'https://android.googleapis.com/gcm/send';

// data to be posted
$fields = array(
                'registration_ids'  => $registration_ids,
                'data'              => array( "message" => $message ),
                );

// headers for the request
$headers = array( 
                    'Authorization: key=' . $api_key,
                    'Content-Type: application/json'
                );

$curl_handle = curl_init();

// set CURL options
curl_setopt( $curl_handle, CURLOPT_URL, $gcm_url );

curl_setopt( $curl_handle, CURLOPT_POST, true );
curl_setopt( $curl_handle, CURLOPT_HTTPHEADER, $headers);
curl_setopt( $curl_handle, CURLOPT_RETURNTRANSFER, true );

curl_setopt( $curl_handle, CURLOPT_POSTFIELDS, json_encode( $fields ) );

// send
$response = curl_exec($curl_handle);

curl_close($curl_handle);

// let's check the response
$data = json_decode($response);

foreach ($data['results'] as $key => $value) {
    if ($value['registration_id']) {
        printf("%s has a new registration id: %s\r\n", $key, $value['registration_id']);
    }
    if ($value['error']) {
        printf("%s encountered error: %s\r\n", $key, $value['error']);
    }
    if ($value['message_id']) {
        printf("%s was successfully sent, message id: %s", $key, $value['message_id']);
    }
}

	}
	
	
	
	
	////////////////////
	
	function save_token_id(){
		
		$userid = $_POST['user_id'];
		$tockenid = $_POST['device_token'];
		$platform = $_POST['device_platform'];
		$user_type = $_POST['user_type'];
		$num = $this->user->check_token_id($userid,$tockenid,$platform,$user_type);
		
		if($num>0){
			$res = $this->user->update_token_id($userid,$tockenid,$platform,$user_type);
			echo json_encode(array("result" =>"success","message" =>"Token Updated Successfully."), 200); exit;
		} else {
			
			$res = $this->user->insert_token_id($userid,$tockenid,$platform,$user_type);
			
			echo json_encode(array("result" =>"success","message" =>"Token Inserted Successfully."), 200); exit;
		}
		
		
	}
	
	
	function destroy_token_id(){
		
		$userid = $_POST['user_id'];
		$tockenid = $_POST['device_token'];
		$user_type = $_POST['user_type'];
		 $this->user->delete_token_id($userid,$tockenid,$user_type);
		
		echo json_encode(array("result" =>"success","message" =>"Token Deleted Successfully."), 200); exit;
		
	}

/*  Send Video call push notification   */
	
function videocall()
{
	     
 		$reciver_id = $_POST['reciver_id'];
		$user_type= $_POST['user_type'];
		$token_id =	$_POST['token_id'];
		$session_id = $_POST['session_id'];
		$sender_name = $_POST['sender_name'];
		$sender_id = $_POST['sender_id'];
	
	//$note_type=$user_type;
	$ip=$_SERVER['REMOTE_ADDR'];
	$date=date("Y-m-d");
	$message=$sender_name."  Had Send The Video Call Request ";
	
	$ps="insert into popup_notifications (n_sid,n_rid,status,note_type,n_ip,n_date,token_id,session_id,note_message) values('$sender_id','$reciver_id','0','$user_type','$ip','$date','$token_id','$session_id','$message')";
	$pe=mysql_query($ps) or die(mysql_error());

	
		

		
		if($user_type=="video_cp")
		{
		$res="caregiver";
		}
		if($user_type=="video_pp")
		{
		$res="professional";
		}
		
		
		define('API_ACCESS_KEY', 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw');
		$device=" ";
		$devices = " ";
		$device_array = array();
		
     $ds="select * from device_tocken where userid='$reciver_id' and user_type='$res'";
		 $se=mysql_query($ds) or die(mysql_error());
		 $num = mysql_num_rows($se);
		 if($num>0){
			while($sr=mysql_fetch_array($se))
			{
				if($sr['tockenid']){
				
				//$device .= $sr['tockenid'].",";
				$device_array[] = $sr['tockenid'];
				
				}
			}	  
		}
		
		//if(!empty($device))  $devices = substr($device,0,-1); 
		
		 
		// echo "Device:".$devices; exit;
		 
		 //echo json_encode(array("result" =>"success","message" =>$devices), 200); exit;
		
		//$registrationIds = array($devices);
			$registrationIds = $device_array;
		
		//print_r($registrationIds); exit;
		
		$msg = array(
			  'message'       => "Testing Video Call Message",
			  'title'         => "Koala- Video Call",
			  'video_session' => $session_id,
			  'video_token' => $token_id,
			  'call_type' => "video",
			  'type'      => '2',
			   'vibrate'   => 1,
			   'sound'     => 1
				);
		
		

	
$fields = array('registration_ids' => $registrationIds,
                'data' => $msg);



		/*$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => array("message" => $msg)
		);*/

		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		echo $result;
	
	
	 
	 
	 
	




       
	   
    }
	
	
	
	
	
		
	function test_push(){
		
		// API access key from Google API's Console
define('API_ACCESS_KEY', 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw');
  
 
  
  $registrationIds = array("APA91bEMkFLwSZ0ZDeK2iSPFfLtV7OBHX3q3iC9eXIFPhYxpUkVgF92DWa64i4NJuyru_RbM81q0H_5cZQxb0SF3RdLpbLwPnavIBdfblWPAwd8TrfT2o0exGlyzyiMN9gxW52mNaBur");

  
  $msg = array(
     'message'       => "Testing Video Call Message"
     
    );
  
  





  $fields = array
  (
   'registration_ids'  => $registrationIds,
   'data'              => $msg
  );

  $headers = array
  (
   'Authorization: key=' . API_ACCESS_KEY,
   'Content-Type: application/json'
  );

  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
  curl_setopt($ch,CURLOPT_POST, true );
  curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch );
  curl_close( $ch );

  //echo $result;
  
  echo "nonononononononononno";
		
	}
	
	
	
	
	
	function delete_diets()
	{  //echo "hai";
    
		
		
		$eventid=3;
		$dietid=$_POST['diet_id'];
		$pid=$_POST["patient_id"];//$this->session->userdata('pid');
		$data = $this->user->getPendingmoredetails($pid,$eventid);
		$ds="update  diets set is_delete='1' where id='$id'";
		$de=mysql_query($ds) or die(mysql_error());
		
		//echo $de;
		//exit;
		
		if(count($data))
		{
			foreach($data as $pendingEvent)
			{
				$nid = $pendingEvent['note_id'];
				$subid=$pendingEvent['sub_event_id'];
				$gmt=$pendingEvent['gmt_time'];
				$milliseconds = round(microtime(true) * 1000);
    
				if($gmt>$milliseconds)
				{
					$ns="delete from notifications where note_id='$nid'";
					$ne=mysql_query($ns) or die(mysql_error());
				}
			}
		}
  
		echo json_encode(array("result" =>"success"), 200); exit;
		//redirect('/caregiver/show_diets/'.$pid);
	
		
	}
	
	
	
	
	
	
	
} //Class Closed
?>